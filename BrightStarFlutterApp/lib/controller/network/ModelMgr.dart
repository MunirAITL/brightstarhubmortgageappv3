import 'package:Bright_Star/model/json/auth/ForgotAPIModel.dart';
import 'package:Bright_Star/model/json/auth/LoginAPIModel.dart';
import 'package:Bright_Star/model/json/auth/RegAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:Bright_Star/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:Bright_Star/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/support/ResolutionAPIModel.dart';
import 'package:Bright_Star/model/json/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:Bright_Star/model/json/tab_mycases/UserNotePutAPIModel.dart';
import 'package:Bright_Star/model/json/tab_newcase/PostCaseAPIModel.dart';
import 'package:Bright_Star/model/json/tab_mycases/UserNotesAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TimeLineAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TimeLinePostAPIModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      //  misc
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
