//  https://github.com/flutterchina/dio
import 'dart:io';
import 'package:Bright_Star/config/Server.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import '../../Mixin.dart';
import 'ModelMgr.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'CookieMgr.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:http/http.dart' show get;

//typedef mapValue = Function(Map<String, dynamic>);

enum NetEnum { Available, NotAvailable, ServerDown }

class NetworkMgr with Mixin {
  Dio _dio;

  Future<T> postData<T, K>(
      {context, url, param, isPost = true, isLoading = true}) async {
    final NetEnum netEnum = await hasNetwork();
    if (netEnum == NetEnum.Available) {
      try {
        if (isLoading) {
          startLoading();
        }
        log("ws::postData==================" + url);
        for (var p in param.entries) {
          log(p.key + '=' + p.value.toString());
        }
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        log(cj.loadForRequest(Uri.parse(url)));
        var response;
        if (isPost)
          response = await _dio.post(url, data: param);
        else
          response = await _dio.put(url, data: param);
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } catch (e) {
        log(e.toString());
        if (isLoading) {
          stopLoading();
        }
      }
    } else if (netEnum == NetEnum.NotAvailable) {
      showToast(msg: "Sorry, internet is not available. try again later");
    } else if (netEnum == NetEnum.ServerDown) {
      showToast(msg: "Sorry, web server is down please try later. thanks");
    }
    return null;
  }

  Future<T> getData<T, K>({context, url, isLoading = true}) async {
    final NetEnum netEnum = await hasNetwork();
    if (netEnum == NetEnum.Available) {
      try {
        if (isLoading) {
          startLoading();
        }
        log("ws::getData==================" + url);
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.get(url);
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } catch (e) {
        if (isLoading) {
          stopLoading();
        }
      }
    } else if (netEnum == NetEnum.NotAvailable) {
      showToast(msg: "Sorry, internet is not available. try again later");
    } else if (netEnum == NetEnum.ServerDown) {
      showToast(msg: "Sorry, web server is down please try later. thanks");
    }
    return null;
  }

  //  files only
  Future<T> uploadFiles<T, K>(
      {context, url, List<File> files, isLoading = true}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        log("ws::uploadFiles:: files only  ==================" + url);
        var formData = FormData();
        for (var file in files) {
          final String mimeStr = lookupMimeType(file.path);
          var fileType = mimeStr.split('/');
          log(fileType[0]);
          //log('file type ${mimeStr}, ${fileType}');
          final String fileName = file.path.split('/').last;
          formData.files.addAll([
            MapEntry(
                "file",
                await MultipartFile.fromFile(file.path,
                    filename: fileName,
                    contentType: MediaType(fileType[0], mimeStr)))
          ]);
        }

        _dio = Dio();
        _dio.options.headers = {
          'Content-type': 'multipart/form-data',
        };
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: formData);
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showToast(msg: "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showToast(msg: "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      if (isLoading) {
        stopLoading();
      }
      showAlertErr(context: context, title: url, msg: e.toString());
    }
  }

  //  multipart
  Future<T> postFile<T, K>({context, url, param, isLoading = true}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        log("ws::postFile :: multipart  ==================" + url);
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: FormData.fromMap(param));
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showToast(msg: "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showToast(msg: "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      if (isLoading) {
        stopLoading();
      }
      showAlertErr(context: context, title: url, msg: e.toString());
    }
  }

  downloadFile({
    BuildContext context,
    String url,
    String pathName,
    isLoading = true,
    Function callback,
  }) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        var response = await get(url);
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == 200) {
          log("ws::downloadFile ==================" + url);
          final String fileName = url.split('/').last;
          var tempDir = await getTemporaryDirectory();
          String fullPath = tempDir.path + '/' + fileName;

          File file = File(fullPath);
          var raf = file.openSync(mode: FileMode.write);
          raf.writeFromSync(response.bodyBytes);
          await raf.close();
          callback(fullPath);
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showToast(msg: "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showToast(msg: "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      if (isLoading) {
        stopLoading();
      }
      log(e.toString());
      callback(null);
    }
  }

  dispose() {
    try {
      stopLoading();
      //_dio.close();
      //_dio = null;
    } catch (e) {}
  }

  //  0=ok
  //  1=internet not available
  //  2=server is down
  Future<NetEnum> hasNetwork() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return await isServerLive();
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return await isServerLive();
    }
    return NetEnum.NotAvailable;
  }

  Future<NetEnum> isServerLive() async {
    try {
      final result = await InternetAddress.lookup(Server.BASE_IP);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('server connected');
        return NetEnum.Available;
      }
    } on SocketException catch (_) {
      print('server not connected');
    }
    return NetEnum.ServerDown;
  }
}
