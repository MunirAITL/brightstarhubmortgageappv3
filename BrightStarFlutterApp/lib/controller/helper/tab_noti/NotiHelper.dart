import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiModel.dart';
import 'package:Bright_Star/config/dashboard/NotiCfg.dart';
import 'package:Bright_Star/view/dashboard/timeline/chat/TaskBiddingScreen.dart';
import 'package:Bright_Star/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class NotiHelper with Mixin {
  getUrl({pageStart, pageCount, caseStatus, UserModel userModel}) {
    var url = Server.NOTI_URL;
    url = url.replaceAll("#userId#", userModel.id.toString());
    return url;
  }

  Map<String, dynamic> getNotiMap({NotiModel model, UserModel userModel}) {
    Map<String, dynamic> notiMap = {};
    try {
      //print(NotiCfg.EVENT_LIST.toString());
      for (var map in NotiCfg.EVENT_LIST) {
        final int listCommunityId = map["communityId"];
        int notificationEventId = map['notificationEventId'];
        if (listCommunityId == int.parse(userModel.communityID)) {
          if (model.entityName == map["entityName"] &&
              (model.notificationEventId == notificationEventId ||
                  notificationEventId == 0)) {
            String txt = map["txt"];
            txt = txt.replaceAll(
                "#InitiatorDisplayName#", model.initiatorDisplayName);
            txt = txt.replaceAll("#EventName#", model.eventName);
            txt = txt.replaceAll('[] ', '');

            //  text processing
            notiMap['txt'] = txt ?? '';
            notiMap['publishDateTime'] = getTimeAgoTxt(model.publishDateTime);

            //  event processing
            //notiMap['communityId'] = map['communityId'];
            //notiMap['desc'] = map['desc'];
            //notiMap['notificationEventId'] = map['notificationEventId'];
            notiMap['route'] =
                (map['route2'] == null) ? map['route'] : map['route2'];

            break;
          }
        }
      }
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }

  setRoute({
    BuildContext context,
    UserModel userModel,
    NotiModel notiModel,
    Map<String, dynamic> notiMap,
    Function callback,
  }) async {
    try {
      Type route = notiMap['route'];
      if (identical(route, WebScreen)) {
        Get.to(
                () => WebScreen(
                      title: notiModel.message,
                      url: Server.BASE_URL_NOTI_WEB + notiModel.webUrl,
                    ),
                fullscreenDialog: true)
            .then((value) {
          //callback(route);
        });
      } else if (identical(route, TaskBiddingScreen)) {
        Get.to(() => TaskBiddingScreen(
              title: notiModel.eventName,
              taskId: notiModel.entityId,
            )).then((value) {
          //callback(route);
        });
      } else {
        //if (identical(route, MsgTab)) {
        callback();
      }
    } catch (e) {}
  }
}

/*extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}*/
