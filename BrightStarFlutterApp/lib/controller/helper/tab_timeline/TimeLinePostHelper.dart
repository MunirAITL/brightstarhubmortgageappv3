class TimeLinePostHelper {
  getParam({
    String message,
    String postTypeName,
    String additionalAttributeValue,
    List<dynamic> inlineTags,
    String checkin,
    int fromLat,
    int fromLng,
    String smallImageName,
    int receiverId,
    int ownerId,
    int senderId,
    int taskId,
    bool isPrivate,
  }) {
    return {
      "Message": message,
      "PostTypeName": postTypeName,
      "AdditionalAttributeValue": additionalAttributeValue,
      "InlineTags": inlineTags,
      "Checkin": checkin,
      "FromLat": fromLat,
      "FromLng": fromLng,
      "SmallImageName": smallImageName,
      "ReceiverId": receiverId,
      "OwnerId": ownerId,
      "SenderId": senderId,
      "TaskId": taskId,
      "IsPrivate": isPrivate,
    };
  }
}
