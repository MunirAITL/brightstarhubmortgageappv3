import 'dart:io';

class LoginRegOtpFBHelper {
  getParam({
    String mobileNumber,
    String otpCode,
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
      "Persist": true,
    };
  }
}
