import 'package:Bright_Star/config/Server.dart';

class SendOtpNotiHelper {
  getUrl({int otpId}) {
    var url = Server.SEND_OTP_NOTI_URL;
    url = url.replaceAll("#otpId#", otpId.toString());
    return url;
  }
}
