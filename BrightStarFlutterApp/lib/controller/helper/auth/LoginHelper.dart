class LoginHelper {
  getParam({
    String email,
    String pwd,
    bool persist = false,
    bool checkSignUpMobileNumber = false,
    String countryCode = "880",
    String status = "101",
    String oTPCode = "",
    String birthDay = "",
    String birthMonth = "",
    String birthYear = "",
    String userCompanyId = "0",
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": "",
      "BirthMonth": "",
      "BirthYear": "",
      "UserCompanyId": "0"
    };
  }
}
