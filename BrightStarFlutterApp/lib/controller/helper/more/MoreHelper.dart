import 'package:Bright_Star/view/dashboard/more/help/HelpScreen.dart';
import 'package:Bright_Star/view/dashboard/more/profile/ProfileScreen.dart';
import 'package:Bright_Star/view/dashboard/more/reviews/ReviewsScreen.dart';
import 'package:Bright_Star/view/dashboard/more/settings/SettingsScreen.dart';
import 'package:Bright_Star/view/dashboard/noti/NotiTab.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoreHelper {
  static const List<Map<String, dynamic>> listMore = [
    {"title": "Profile", "route": ProfileScreen},
    //{"title": "Reviews", "route": ReviewsScreen},
    {"title": "Notifications", "route": NotiTab},
    {"title": "Settings", "route": SettingsScreen},
    {"title": "Help", "route": HelpScreen},
    {"title": "Logout", "route": null},
  ];

  setRoute({
    Type route,
    BuildContext context,
    Function callback,
  }) async {
    try {
      if (identical(route, ProfileScreen)) {
        Get.to(() => ProfileScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, ReviewsScreen)) {
        Get.to(() => ReviewsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, SettingsScreen)) {
        Get.to(() => SettingsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, HelpScreen)) {
        Get.to(() => HelpScreen()).then((value) {
          callback(route);
        });
      }
    } catch (e) {}
  }
}
