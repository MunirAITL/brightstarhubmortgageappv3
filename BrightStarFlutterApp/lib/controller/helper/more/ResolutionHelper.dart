import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/mywidgets/dropdown/DropListModel.dart';
import 'package:jiffy/jiffy.dart';

class ResolutionHelper {
  //  dropdown
  DropListModel dd = DropListModel([
    OptionItem(id: "1", title: "Technical Problem with software"),
    OptionItem(id: "2", title: "I found a bug in the software"),
    OptionItem(id: "3", title: "I have a non-technical issue"),
    OptionItem(id: "4", title: "I have a Complain"),
    OptionItem(id: "5", title: "I have a question about My cases"),
  ]);

  OptionItem opt = OptionItem(id: null, title: "Select Support Ticket Type");

  //  *****************************

  getParam({
    UserModel userModel,
    String title,
    String desc,
    String fileUrl,
  }) {
    return {
      "Id": userModel.id,
      "Status": 101,
      "Title": title,
      "Description": desc,
      "Remarks": "",
      "InitiatorId": 115765,
      "ServiceDate": Jiffy().format('dd-MMM-yyyy'), //"05-Mar-2021",
      "ResolutionType": "Support Ticket",
      "ParentId": 0,
      "AssigneeId": 0,
      "UserCompanyId": userModel.userCompanyInfo.userID,
      "FileUrl": fileUrl,
    };
  }
}
