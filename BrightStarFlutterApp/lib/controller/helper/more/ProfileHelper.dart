import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/data/PrefMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:polygon_clipper/polygon_border.dart';

class ProfileHelper with Mixin {
  static const String ProfilePicFG_Key = "ProfilePicFG";
  static const String ProfilePicBG_Key = "ProfilePicBG";

  downloadProfileImages(
      {BuildContext context, UserModel userModel, isLoading = true}) async {
    if (userModel.profileImageURL != '' &&
        userModel.profileImageURL.startsWith("http")) {
      await NetworkMgr().downloadFile(
          context: context,
          url: userModel.profileImageURL,
          pathName: ProfileHelper.ProfilePicFG_Key,
          isLoading: isLoading,
          callback: (path) {
            if (path != null) {
              PrefMgr.shared.setPrefStr(ProfileHelper.ProfilePicFG_Key, path);
            }
          });
    }
    if (userModel.coverImageURL != '' &&
        userModel.coverImageURL.startsWith("http")) {
      await NetworkMgr().downloadFile(
          url: userModel.coverImageURL,
          pathName: ProfileHelper.ProfilePicBG_Key,
          callback: (path) {
            if (path != null) {
              PrefMgr.shared.setPrefStr(ProfileHelper.ProfilePicBG_Key, path);
            }
          });
    }
  }

  getUserOnlineStatus({BuildContext context, int statusIndex = 1}) {
    Color colr = Colors.greenAccent.shade400;
    String status = "Online"; //  statusIndex=1
    if (statusIndex == 0) {
      status = "Offline";
      colr = Colors.redAccent.shade400;
    } else if (statusIndex == 2) {
      status = "Away";
      colr = Colors.yellowAccent.shade400;
    }

    return Container(
      //width: getWP(context, 50),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: getWP(context, 5),
            height: getWP(context, 5),
            decoration: BoxDecoration(shape: BoxShape.circle, color: colr),
          ),
          SizedBox(width: 10),
          Txt(
              txt: status,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            Icons.star,
            color: (i < rate) ? colr : Colors.grey,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({int rate, int reviews}) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getStarsRow(rate, MyTheme.brownColor),
          SizedBox(width: 10),
          Txt(
              txt: reviews.toString() + " Reviews",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getCompletionText({int pa}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: pa.toString() + "% Completion Rate",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          IconButton(
            icon: Icon(
              Icons.info,
              color: Colors.grey, //MyTheme.themeData.accentColor,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  getAvatorStarView({int rate}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(18.0),
            margin: EdgeInsets.only(right: 16.0),
            decoration: ShapeDecoration(
                shape: PolygonBorder(
                    sides: 6,
                    borderRadius: 1,
                    border: BorderSide(color: Colors.grey, width: 0.5))),
            child: Image.asset(
              "assets/images/icons/user_icon.png",
              width: 40,
              height: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getStarsRow(rate, Colors.cyan),
              SizedBox(height: 10),
              Txt(
                  txt: getTimeAgoTxt("2021-01-27T16:16:33.47Z"),
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          )
        ],
      ),
    );
  }
}
