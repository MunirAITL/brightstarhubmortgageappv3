import 'package:Bright_Star/model/json/auth/UserModel.dart';

class PostNewCaseHelper {
  getParam({
    bool isOtherApplicantSwitch,
    bool isSPVSwitch,
    String compName,
    String regAddr,
    String regDate,
    String regNo,
    String title,
    String note,
    UserModel userModel,
  }) {
    try {
      final paramMortgageCaseInfoEntityModelList = [];

      final paramMortgageCaseInfoEntity = {
        "UserId": userModel.id.toString(),
        "CompanyId": userModel.userCompanyID,
        "TaskId": 0,
        "Status": 0,
        "CreationDate": "17-Oct-2020",
        "UpdatedDate": "17-Oct-2020",
        "VersionNumber": 0,
        "CaseType": "",
        "CustomerType": "",
        "IsSmoker": "No",
        "Remarks": "",
        "IsAnyOthers": (isOtherApplicantSwitch) ? "Yes" : "No",
        "IsCurrentProperty": "No",
        "CustomerName": userModel.name,
        "CustomerEmail": userModel.email,
        "CustomerMobileNumber": userModel.mobileNumber,
        "CustomerAddress": (userModel.address +
                ' ' +
                userModel.addressLine1 +
                ' ' +
                userModel.addressLine2 +
                ' ' +
                userModel.addressLine3)
            .trim(),
        "CoapplicantUserId": 0,
        "AreYouBuyingThePropertyInNameOfASPV": (isSPVSwitch) ? "Yes" : "No",
        "CompanyName": compName.trim(),
        "RegisteredAddress": regAddr.trim(),
        "DateRegistered": regDate,
        "CompanyRegistrationNumber": regNo.trim(),
        "DateRegistered1": "",
        "DateRegistered2": "",
        "DateRegistered3": "",
        "AdminFee": 0,
        "AdminFeeWhenPayable": "",
        "AdviceFee": 0,
        "AdviceFeeWhenPayable": "",
        "IsFeesRefundable": "",
        "FeesRefundable": ""
      };

      return {
        "Status": 903,
        "Title": title,
        "Description": note,
        "IsInPersonOrOnline": false,
        "DutDateType": 0,
        "DeliveryDate": "17-Oct-2020",
        "DeliveryTime": "",
        "WorkerNumber": 1,
        "Skill": "",
        "IsFixedPrice": true,
        "HourlyRate": 0,
        "FixedBudgetAmount": 0,
        "NetTotalAmount": 0,
        "JobCategory": "Regular",
        "PreferedLocation": "",
        "Requirements": "",
        "TotalHours": 0,
        "Latitude": 0,
        "Longitude": 0,
        "TaskReferenceNumber": "",
        "ReferenceTaskerId": 0,
        "MortgageCaseInfoEntityModelList": paramMortgageCaseInfoEntityModelList,
        "UserId": userModel.id.toString(),
        "EntityId": userModel.userCompanyInfo.entityID,
        "EntityName": userModel.userCompanyInfo.entityName,
        "CompanyId": userModel.userCompanyID,
        "MortgageCaseInfoEntity": paramMortgageCaseInfoEntity,
      };
    } catch (e) {}
  }
}
