import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/config/dashboard/NewCaseCfg.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';

class NewCaseHelper {
  getUrl({pageStart, pageCount, status, UserModel userModel}) {
    var url = Server.NEWCASE_URL;
    url = url.replaceAll("#userId#", userModel.id.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", status);
    return url;
  }

  getCreateCaseIconByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map["url"];
        }
      }
    } catch (e) {}
    return null;
  }
}
