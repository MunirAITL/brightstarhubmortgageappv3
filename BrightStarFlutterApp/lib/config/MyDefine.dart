class MyDefine {
  static const String MISSING_IMG =
      "https://mortgage-magic.co.uk/api/content/media/default_avatar.png";

  static const String CUR_SIGN = "€";
}
