import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double appbarTitleFontSize = 2.3;
  static final double txtSize = 2.2;
  static final double logoWidth = 40;
  static final String fontFamily = "Arial";
  static Color cyanColor = HexColor.fromHex("#BCEFE6");
  static Color brownColor = HexColor.fromHex('#320B42');
  static Color loginBGColor = HexColor.fromHex('#E1E1E5');
  static Color lPurpleColor = HexColor.fromHex("#f0c0c0");
  static Color appbarColor = HexColor.fromHex("#F1F1F1").withAlpha(200);
  static final Color appbarProgColor = Colors.orangeAccent;

  static final ThemeData refreshIndicatorTheme =
      ThemeData(canvasColor: brownColor, accentColor: Colors.white);

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#F5F5F7"),

    //iconTheme: IconThemeData(color: Colors.white),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    iconTheme: IconThemeData(color: Colors.white),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  //static final BoxDecoration boxDeco = BoxDecoration(
  //color: Colors.white,
  //);

  static final boxDeco = BoxDecoration(
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);
}
