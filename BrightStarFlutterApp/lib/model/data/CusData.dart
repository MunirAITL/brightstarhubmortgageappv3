class CusData {
  static final CusData _appData = new CusData._internal();
  factory CusData() {
    return _appData;
  }
  CusData._internal();
}

final cusData = CusData();
