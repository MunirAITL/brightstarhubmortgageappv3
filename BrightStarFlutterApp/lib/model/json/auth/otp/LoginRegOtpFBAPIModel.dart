import '../UserModel.dart';

class LoginRegOtpFBAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  _ResponseData responseData;

  LoginRegOtpFBAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  LoginRegOtpFBAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = (json['ResponseData'] != null)
        ? _ResponseData.fromJson(json['ResponseData'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['ResponseData'] = this.responseData;
    return data;
  }
}

class ErrorMessages {
  List<String> login;
  ErrorMessages({this.login});
  ErrorMessages.fromJson(Map<String, dynamic> json) {
    try {
      login = json['login'].cast<String>() ?? [];
    } catch (e) {
      login = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class _ResponseData {
  String returnUrl;
  UserModel user;
  //List<dynamic> user;
  _ResponseData({this.returnUrl, this.user});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(
        returnUrl: j['ReturnUrl'] ?? '',
        user: UserModel.fromJson(j['User']) ?? []);
  }

  Map<String, dynamic> toMap() => {
        'ReturnUrl': returnUrl,
        'User': user.toJson(),
      };
}
