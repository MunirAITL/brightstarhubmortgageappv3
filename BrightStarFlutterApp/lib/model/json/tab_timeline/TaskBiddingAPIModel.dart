import 'TaskBiddingModel.dart';

class TaskBiddingAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  TaskBiddingAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory TaskBiddingAPIModel.fromJson(Map<String, dynamic> j) {
    return TaskBiddingAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  List<dynamic> taskBiddings;
  _ResponseData({this.taskBiddings});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_TaskBiddings = [];
    try {
      list_TaskBiddings = (j['TaskBiddings'] != null)
          ? j['TaskBiddings'].map((i) => TaskBiddingModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(taskBiddings: list_TaskBiddings ?? []);
  }
  Map<String, dynamic> toMap() => {
        'TaskBiddings': taskBiddings,
      };
}
