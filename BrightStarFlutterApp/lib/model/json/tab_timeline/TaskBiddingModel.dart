class TaskBiddingModel {
  int userId;
  String status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String coverLetter;
  String description;
  String deliveryDate;
  String deliveryTime;
  double fixedbiddigAmount;
  double hourlyBiddingAmount;
  double totalHourPerWeek;
  double totalHour;
  String ownerName;
  String thumbnailPath;
  String referenceType;
  String referenceId;
  String remarks;
  String imageServerUrl;
  String ownerImageUrl;
  String ownerProfileUrl;
  double taskCompletionRate;
  double averageRating;
  int ratingCount;
  bool isTaskOwner;
  int taskOwnerId;
  bool isReview;
  bool isReviewByPoster;
  bool isReviewByShohoKari;
  String referenceBiddingUserId;
  String referenceBiddingUserType;
  String paymentStatus;
  double discountAmount;
  double netTotalAmount;
  int userPromotionId;
  bool isInPersonOrOnline;
  int totalComments;
  String taskPaymentPaymentMethod;
  String taskPaymentAccountStatus;
  int taskPaymentId;
  String taskTitle;
  String taskTitleUrl;
  int communityId;
  String communityName;
  int id;

  TaskBiddingModel({
    this.userId,
    this.status,
    this.creationDate,
    this.updatedDate,
    this.versionNumber,
    this.taskId,
    this.coverLetter,
    this.description,
    this.deliveryDate,
    this.deliveryTime,
    this.fixedbiddigAmount,
    this.hourlyBiddingAmount,
    this.totalHourPerWeek,
    this.totalHour,
    this.ownerName,
    this.thumbnailPath,
    this.referenceType,
    this.referenceId,
    this.remarks,
    this.imageServerUrl,
    this.ownerImageUrl,
    this.ownerProfileUrl,
    this.taskCompletionRate,
    this.averageRating,
    this.ratingCount,
    this.isTaskOwner,
    this.taskOwnerId,
    this.isReview,
    this.isReviewByPoster,
    this.isReviewByShohoKari,
    this.referenceBiddingUserId,
    this.referenceBiddingUserType,
    this.paymentStatus,
    this.discountAmount,
    this.netTotalAmount,
    this.userPromotionId,
    this.isInPersonOrOnline,
    this.totalComments,
    this.taskPaymentPaymentMethod,
    this.taskPaymentAccountStatus,
    this.taskPaymentId,
    this.taskTitle,
    this.taskTitleUrl,
    this.communityId,
    this.communityName,
    this.id,
  });

  factory TaskBiddingModel.fromJson(Map<String, dynamic> j) {
    return TaskBiddingModel(
      userId: j['UserId'] ?? 0,
      status: j['Status'] ?? '',
      creationDate: j['CreationDate'] ?? '',
      updatedDate: j['UpdatedDate'] ?? '',
      versionNumber: j['VersionNumber'] ?? 0,
      taskId: j['TaskId'] ?? 0,
      coverLetter: j['CoverLetter'] ?? '',
      description: j['Description'] ?? '',
      deliveryDate: j['DeliveryDate'] ?? '',
      deliveryTime: j['DeliveryTime'] ?? '',
      fixedbiddigAmount: j['FixedbiddigAmount'] ?? 0.0,
      hourlyBiddingAmount: j['HourlyBiddingAmount'] ?? 0.0,
      totalHourPerWeek: j['TotalHourPerWeek'] ?? 0.0,
      totalHour: j['TotalHour'] ?? 0.0,
      ownerName: j['OwnerName'] ?? '',
      thumbnailPath: j['ThumbnailPath'] ?? '',
      referenceType: j['ReferenceType'] ?? '',
      referenceId: j['ReferenceId'] ?? '',
      remarks: j['Remarks'] ?? '',
      imageServerUrl: j['ImageServerUrl'] ?? '',
      ownerImageUrl: j['OwnerImageUrl'] ?? '',
      ownerProfileUrl: j['OwnerProfileUrl'] ?? '',
      taskCompletionRate: j['TaskCompletionRate'] ?? 0.0,
      averageRating: j['AverageRating'] ?? 0.0,
      ratingCount: j['RatingCount'] ?? 0,
      isTaskOwner: j['IsTaskOwner'] ?? false,
      taskOwnerId: j['TaskOwnerId'] ?? 0,
      isReview: j['IsReview'] ?? false,
      isReviewByPoster: j['IsReviewByPoster'] ?? false,
      isReviewByShohoKari: j['IsReviewByShohoKari'] ?? false,
      referenceBiddingUserId: j['ReferenceBiddingUserId'] ?? '',
      referenceBiddingUserType: j['ReferenceBiddingUserType'] ?? '',
      paymentStatus: j['PaymentStatus'] ?? '',
      discountAmount: j['DiscountAmount'] ?? 0.0,
      netTotalAmount: j['NetTotalAmount'] ?? 0.0,
      userPromotionId: j['UserPromotionId'] ?? 0,
      isInPersonOrOnline: j['IsInPersonOrOnline'] ?? false,
      totalComments: j['TotalComments'] ?? 0,
      taskPaymentPaymentMethod: j['TaskPaymentPaymentMethod'] ?? '',
      taskPaymentAccountStatus: j['TaskPaymentAccountStatus'] ?? '',
      taskPaymentId: j['TaskPaymentId'] ?? 0,
      taskTitle: j['TaskTitle'] ?? '',
      taskTitleUrl: j['TaskTitleUrl'] ?? '',
      communityId: j['CommunityId'] ?? 0,
      communityName: j['CommunityName'] ?? '',
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'UserId': userId,
        'Status': status,
        'CreationDate': creationDate,
        'UpdatedDate': updatedDate,
        'VersionNumber': versionNumber,
        'TaskId': taskId,
        'CoverLetter': coverLetter,
        'Description': description,
        'DeliveryDate': deliveryDate,
        'DeliveryTime': deliveryTime,
        'FixedbiddigAmount': fixedbiddigAmount,
        'HourlyBiddingAmount': hourlyBiddingAmount,
        'TotalHourPerWeek': totalHourPerWeek,
        'TotalHour': totalHour,
        'OwnerName': ownerName,
        'ThumbnailPath': thumbnailPath,
        'ReferenceType': referenceType,
        'ReferenceId': referenceId,
        'Remarks': remarks,
        'ImageServerUrl': imageServerUrl,
        'OwnerImageUrl': ownerImageUrl,
        'OwnerProfileUrl': ownerProfileUrl,
        'TaskCompletionRate': taskCompletionRate,
        'AverageRating': averageRating,
        'RatingCount': ratingCount,
        'IsTaskOwner': isTaskOwner,
        'TaskOwnerId': taskOwnerId,
        'IsReview': isReview,
        'IsReviewByPoster': isReviewByPoster,
        'IsReviewByShohoKari': isReviewByShohoKari,
        'ReferenceBiddingUserId': referenceBiddingUserId,
        'ReferenceBiddingUserType': referenceBiddingUserType,
        'PaymentStatus': paymentStatus,
        'DiscountAmount': discountAmount,
        'NetTotalAmount': netTotalAmount,
        'UserPromotionId': userPromotionId,
        'IsInPersonOrOnline': isInPersonOrOnline,
        'TotalComments': totalComments,
        'TaskPaymentPaymentMethod': taskPaymentPaymentMethod,
        'TaskPaymentAccountStatus': taskPaymentAccountStatus,
        'TaskPaymentId': taskPaymentId,
        'TaskTitle': taskTitle,
        'TaskTitleUrl': taskTitleUrl,
        'CommunityId': communityId,
        'CommunityName': communityName,
        'Id': id,
      };
}
