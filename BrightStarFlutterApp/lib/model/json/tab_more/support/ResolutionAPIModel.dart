import 'ResolutionModel.dart';

class ResolutionAPIModel {
  bool success;
  dynamic errorMessages;
  _Messages messages;
  _ResponseData responseData;

  ResolutionAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory ResolutionAPIModel.fromJson(Map<String, dynamic> j) {
    return ResolutionAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'],
      messages: _Messages.fromJson(j['Messages']),
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _Messages {
  List<dynamic> resolution_post;
  _Messages({this.resolution_post});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      resolution_post: j['resolution_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'resolution_post': resolution_post,
      };
}

class _ResponseData {
  ResolutionModel resolution;
  _ResponseData({this.resolution});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(
        resolution: (j['Resolution'] != null)
            ? ResolutionModel.fromJson(j['Resolution'])
            : {});
  }
  Map<String, dynamic> toMap() => {
        'Resolution': resolution,
      };
}
