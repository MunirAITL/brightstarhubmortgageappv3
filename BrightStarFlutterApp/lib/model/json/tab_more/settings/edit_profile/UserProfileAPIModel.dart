import 'UserProfileModel.dart';

class UserProfileAPIModel {
  bool success;
  dynamic errorMessages;
  _Messages messages;
  _ResponseData responseData;

  UserProfileAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory UserProfileAPIModel.fromJson(Map<String, dynamic> j) {
    return UserProfileAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? [],
      messages: _Messages.fromJson(j['Messages']),
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _Messages {
  List<dynamic> post_user;
  _Messages({this.post_user});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      post_user: j['post_user'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'post_user': post_user,
      };
}

class _ResponseData {
  UserProfileModel user;
  _ResponseData({this.user});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(user: UserProfileModel.fromJson(j['User']) ?? []);
  }

  Map<String, dynamic> toMap() => {
        'User': user.toMap(),
      };
}
