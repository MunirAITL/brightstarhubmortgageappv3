class NotiSettingModel {
  int userId;
  int userCompanyId;
  String user;
  String category;
  bool isTransactionalEmail;
  bool isTransactionalNotification;
  bool isTransactionalSMS;
  bool isCaseCompletedEmail;
  bool isCaseCompletedSMS;
  bool isCaseCompletedNotification;
  bool isCaseRecomendationEmail;
  bool isCaseRecomendationNotification;
  bool isCaseUpdateEmail;
  bool isCaseUpdateSMS;
  bool isCaseUpdateNotification;
  bool isCaseReminderEmail;
  bool isCaseReminderSMS;
  bool isCaseReminderNotification;
  bool isUserUpdateEmail;
  bool isUserUpdateNotification;
  bool isHelpfulInformationEmail;
  bool isHelpfulInformationNotification;
  bool isUpdateAndNewsLetterEmail;
  bool isUpdateAndNewsLetterNotification;
  bool isHelpFullEmail;
  bool isHelpFullNotification;
  bool isLeadUpdateEmail;
  bool isLeadUpdateSMS;
  bool isLeadUpdateNotification;
  int id;

  NotiSettingModel(
      {this.userId,
      this.userCompanyId,
      this.user,
      this.category,
      this.isTransactionalEmail,
      this.isTransactionalNotification,
      this.isTransactionalSMS,
      this.isCaseCompletedEmail,
      this.isCaseCompletedSMS,
      this.isCaseCompletedNotification,
      this.isCaseRecomendationEmail,
      this.isCaseRecomendationNotification,
      this.isCaseUpdateEmail,
      this.isCaseUpdateSMS,
      this.isCaseUpdateNotification,
      this.isCaseReminderEmail,
      this.isCaseReminderSMS,
      this.isCaseReminderNotification,
      this.isUserUpdateEmail,
      this.isUserUpdateNotification,
      this.isHelpfulInformationEmail,
      this.isHelpfulInformationNotification,
      this.isUpdateAndNewsLetterEmail,
      this.isUpdateAndNewsLetterNotification,
      this.isHelpFullEmail,
      this.isHelpFullNotification,
      this.isLeadUpdateEmail,
      this.isLeadUpdateSMS,
      this.isLeadUpdateNotification,
      this.id});

  NotiSettingModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    userCompanyId = json['UserCompanyId'] ?? 0;
    user = json['User'] ?? '';
    category = json['Category'] ?? '';
    isTransactionalEmail = json['IsTransactionalEmail'] ?? false;
    isTransactionalNotification = json['IsTransactionalNotification'] ?? false;
    isTransactionalSMS = json['IsTransactionalSMS'] ?? false;
    isCaseCompletedEmail = json['IsCaseCompletedEmail'] ?? false;
    isCaseCompletedSMS = json['IsCaseCompletedSMS'] ?? false;
    isCaseCompletedNotification = json['IsCaseCompletedNotification'] ?? false;
    isCaseRecomendationEmail = json['IsCaseRecomendationEmail'] ?? false;
    isCaseRecomendationNotification =
        json['IsCaseRecomendationNotification'] ?? false;
    isCaseUpdateEmail = json['IsCaseUpdateEmail'] ?? false;
    isCaseUpdateSMS = json['IsCaseUpdateSMS'] ?? false;
    isCaseUpdateNotification = json['IsCaseUpdateNotification'] ?? false;
    isCaseReminderEmail = json['IsCaseReminderEmail'] ?? false;
    isCaseReminderSMS = json['IsCaseReminderSMS'] ?? false;
    isCaseReminderNotification = json['IsCaseReminderNotification'] ?? false;
    isUserUpdateEmail = json['IsUserUpdateEmail'] ?? false;
    isUserUpdateNotification = json['IsUserUpdateNotification'] ?? false;
    isHelpfulInformationEmail = json['IsHelpfulInformationEmail'] ?? false;
    isHelpfulInformationNotification =
        json['IsHelpfulInformationNotification'] ?? false;
    isUpdateAndNewsLetterEmail = json['IsUpdateAndNewsLetterEmail'] ?? false;
    isUpdateAndNewsLetterNotification =
        json['IsUpdateAndNewsLetterNotification'] ?? false;
    isHelpFullEmail = json['IsHelpFullEmail'] ?? false;
    isHelpFullNotification = json['IsHelpFullNotification'] ?? false;
    isLeadUpdateEmail = json['IsLeadUpdateEmail'] ?? false;
    isLeadUpdateSMS = json['IsLeadUpdateSMS'] ?? false;
    isLeadUpdateNotification = json['IsLeadUpdateNotification'] ?? false;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['User'] = this.user;
    data['Category'] = this.category;
    data['IsTransactionalEmail'] = this.isTransactionalEmail;
    data['IsTransactionalNotification'] = this.isTransactionalNotification;
    data['IsTransactionalSMS'] = this.isTransactionalSMS;
    data['IsCaseCompletedEmail'] = this.isCaseCompletedEmail;
    data['IsCaseCompletedSMS'] = this.isCaseCompletedSMS;
    data['IsCaseCompletedNotification'] = this.isCaseCompletedNotification;
    data['IsCaseRecomendationEmail'] = this.isCaseRecomendationEmail;
    data['IsCaseRecomendationNotification'] =
        this.isCaseRecomendationNotification;
    data['IsCaseUpdateEmail'] = this.isCaseUpdateEmail;
    data['IsCaseUpdateSMS'] = this.isCaseUpdateSMS;
    data['IsCaseUpdateNotification'] = this.isCaseUpdateNotification;
    data['IsCaseReminderEmail'] = this.isCaseReminderEmail;
    data['IsCaseReminderSMS'] = this.isCaseReminderSMS;
    data['IsCaseReminderNotification'] = this.isCaseReminderNotification;
    data['IsUserUpdateEmail'] = this.isUserUpdateEmail;
    data['IsUserUpdateNotification'] = this.isUserUpdateNotification;
    data['IsHelpfulInformationEmail'] = this.isHelpfulInformationEmail;
    data['IsHelpfulInformationNotification'] =
        this.isHelpfulInformationNotification;
    data['IsUpdateAndNewsLetterEmail'] = this.isUpdateAndNewsLetterEmail;
    data['IsUpdateAndNewsLetterNotification'] =
        this.isUpdateAndNewsLetterNotification;
    data['IsHelpFullEmail'] = this.isHelpFullEmail;
    data['IsHelpFullNotification'] = this.isHelpFullNotification;
    data['IsLeadUpdateEmail'] = this.isLeadUpdateEmail;
    data['IsLeadUpdateSMS'] = this.isLeadUpdateSMS;
    data['IsLeadUpdateNotification'] = this.isLeadUpdateNotification;
    data['Id'] = this.id;
    return data;
  }
}
