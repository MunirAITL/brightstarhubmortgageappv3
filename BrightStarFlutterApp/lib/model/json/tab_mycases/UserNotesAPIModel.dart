import '../tab_newcase/UserNotesModel.dart';

class UserNoteByEntityAPIModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  UserNoteByEntityAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory UserNoteByEntityAPIModel.fromJson(Map<String, dynamic> j) {
    return UserNoteByEntityAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  List<dynamic> userNotes;
  _ResponseData({this.userNotes});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_UserNotes = [];
    try {
      list_UserNotes = (j['UserNotes'] != null)
          ? j['UserNotes'].map((i) => UserNotesModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(userNotes: list_UserNotes ?? []);
  }
  Map<String, dynamic> toMap() => {
        'UserNotes': userNotes,
      };
}
