class MortgageCaseInfoEntityModelListModel {
  int userID;
  String user;
  int companyID, status;
  String creationDate, updatedDate;
  int versionNumber;
  String caseType;
  String customerType;
  String isSmoker;
  String isDoYouHaveAnyFinancialDependants;
  String remarks;
  String isAnyOthers;
  int taskID, coapplicantUserID;
  String customerName;
  String customerEmail;
  String customerMobileNumber;
  String customerAddress;
  String profileImageURL;
  String namePrefix;
  String areYouBuyingThePropertyInNameOfASPV;
  String companyName, registeredAddress;
  String dateRegistered;
  String companyRegistrationNumber;
  int applicationNumber;
  String customerEmail1, customerEmail2, customerEmail3;
  String isDoYouHaveAnyBTLPortfulio;
  String isClientAgreement;
  double adminFee;
  String adminFeeWhenPayable;
  double adviceFee;
  String adviceFeeWhenPayable;
  String isFeesRefundable, feesRefundable;
  String ipAdddress, ipLocation;
  String deviceType, reportLogo, officePhoneNumber;
  String reportFooter, adviceFeeType, clientAgreementStatus;
  String recommendationAgreementStatus;
  String recommendationAgreementSignature;
  String isThereAnySavingsOrInvestments;
  String isThereAnyExistingPolicy;
  String taskTitleURL;
  String customerAddress1,
      customerAddress2,
      customerAddress3,
      customerPostcode,
      customerTown;
  String customerLastName;
  int id;

  MortgageCaseInfoEntityModelListModel({
    this.userID,
    this.user,
    this.companyID,
    this.status,
    this.creationDate,
    this.updatedDate,
    this.versionNumber,
    this.caseType,
    this.customerType,
    this.isSmoker,
    this.isDoYouHaveAnyFinancialDependants,
    this.remarks,
    this.isAnyOthers,
    this.taskID,
    this.coapplicantUserID,
    this.customerName,
    this.customerEmail,
    this.customerMobileNumber,
    this.customerAddress,
    this.profileImageURL,
    this.namePrefix,
    this.areYouBuyingThePropertyInNameOfASPV,
    this.companyName,
    this.registeredAddress,
    this.dateRegistered,
    this.companyRegistrationNumber,
    this.applicationNumber,
    this.customerEmail1,
    this.customerEmail2,
    this.customerEmail3,
    this.isDoYouHaveAnyBTLPortfulio,
    this.isClientAgreement,
    this.adminFee,
    this.adminFeeWhenPayable,
    this.adviceFee,
    this.adviceFeeWhenPayable,
    this.isFeesRefundable,
    this.feesRefundable,
    this.ipAdddress,
    this.ipLocation,
    this.deviceType,
    this.reportLogo,
    this.officePhoneNumber,
    this.reportFooter,
    this.adviceFeeType,
    this.clientAgreementStatus,
    this.recommendationAgreementStatus,
    this.recommendationAgreementSignature,
    this.isThereAnySavingsOrInvestments,
    this.isThereAnyExistingPolicy,
    this.taskTitleURL,
    this.customerAddress1,
    this.customerAddress2,
    this.customerAddress3,
    this.customerPostcode,
    this.customerTown,
    this.customerLastName,
    this.id,
  });

  factory MortgageCaseInfoEntityModelListModel.fromJson(
      Map<String, dynamic> j) {
    return MortgageCaseInfoEntityModelListModel(
      userID: j['UserId'] ?? 0,
      user: j['User'] ?? '',
      companyID: j['CompanyId'] ?? 0,
      status: j['Status'] ?? 0,
      creationDate: j['CreationDate'] ?? '',
      updatedDate: j['UpdatedDate'] ?? '',
      versionNumber: j['VersionNumber'] ?? 0,
      caseType: j['CaseType'] ?? '',
      customerType: j['CustomerType'] ?? '',
      isSmoker: j['IsSmoker'] ?? '',
      isDoYouHaveAnyFinancialDependants:
          j['IsDoYouHaveAnyFinancialDependants'] ?? '',
      remarks: j['Remarks'] ?? '',
      isAnyOthers: j['IsAnyOthers'] ?? '',
      taskID: j['TaskId'] ?? 0,
      coapplicantUserID: j['CoapplicantUserId'] ?? 0,
      customerName: j['CustomerName'] ?? '',
      customerEmail: j['CustomerEmail'] ?? '',
      customerMobileNumber: j['CustomerMobileNumber'] ?? '',
      customerAddress: j['CustomerAddress'] ?? '',
      profileImageURL: j['ProfileImageUrl'] ?? '',
      namePrefix: j['NamePrefix'] ?? '',
      areYouBuyingThePropertyInNameOfASPV:
          j['AreYouBuyingThePropertyInNameOfASPV'] ?? '',
      companyName: j['CompanyName'] ?? '',
      registeredAddress: j['RegisteredAddress'] ?? '',
      dateRegistered: j['DateRegistered'] ?? '',
      companyRegistrationNumber: j['CompanyRegistrationNumber'] ?? '',
      applicationNumber: j['ApplicationNumber'] ?? 0,
      customerEmail1: j['CustomerEmail1'] ?? '',
      customerEmail2: j['CustomerEmail2'] ?? '',
      customerEmail3: j['CustomerEmail3'] ?? '',
      isDoYouHaveAnyBTLPortfulio: j['IsDoYouHaveAnyBTLPortfulio'] ?? '',
      isClientAgreement: j['IsClientAgreement'] ?? '',
      adminFee: j['AdminFee'] ?? 0.0,
      adminFeeWhenPayable: j['AdminFeeWhenPayable'] ?? '',
      adviceFee: j['AdviceFee'] ?? 0.0,
      adviceFeeWhenPayable: j['AdviceFeeWhenPayable'] ?? '',
      isFeesRefundable: j['IsFeesRefundable'] ?? '',
      feesRefundable: j['FeesRefundable'] ?? '',
      ipAdddress: j['IPAdddress'] ?? '',
      ipLocation: j['IPLocation'] ?? '',
      deviceType: j['DeviceType'] ?? '',
      reportLogo: j['ReportLogo'] ?? '',
      officePhoneNumber: j['OfficePhoneNumber'] ?? '',
      reportFooter: j['ReportFooter'] ?? '',
      adviceFeeType: j['AdviceFeeType'] ?? '',
      clientAgreementStatus: j['ClientAgreementStatus'] ?? '',
      recommendationAgreementStatus: j['RecommendationAgreementStatus'] ?? '',
      recommendationAgreementSignature:
          j['RecommendationAgreementSignature'] ?? '',
      isThereAnySavingsOrInvestments: j['IsThereAnySavingsOrInvestments'] ?? '',
      isThereAnyExistingPolicy: j['IsThereAnyExistingPolicy'] ?? '',
      taskTitleURL: j['TaskTitleUrl'] ?? '',
      customerAddress1: j['CustomerAddress1'] ?? '',
      customerAddress2: j['CustomerAddress2'] ?? '',
      customerAddress3: j['CustomerAddress3'] ?? '',
      customerPostcode: j['CustomerPostcode'] ?? '',
      customerTown: j['CustomerTown'] ?? '',
      customerLastName: j['CustomerLastName'] ?? '',
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'UserId': userID,
        'User': user,
        'CompanyId': companyID,
        'Status': status,
        'CreationDate': creationDate,
        'UpdatedDate': updatedDate,
        'VersionNumber': versionNumber,
        'CaseType': caseType,
        'CustomerType': customerType,
        'IsSmoker': isSmoker,
        'IsDoYouHaveAnyFinancialDependants': isDoYouHaveAnyFinancialDependants,
        'Remarks': remarks,
        'IsAnyOthers': isAnyOthers,
        'TaskId': taskID,
        'CoapplicantUserId': coapplicantUserID,
        'CustomerName': customerName,
        'CustomerEmail': customerEmail,
        'CustomerMobileNumber': customerMobileNumber,
        'CustomerAddress': customerAddress,
        'ProfileImageUrl': profileImageURL,
        'NamePrefix': namePrefix,
        'AreYouBuyingThePropertyInNameOfASPV':
            areYouBuyingThePropertyInNameOfASPV,
        'CompanyName': companyName,
        'RegisteredAddress': registeredAddress,
        'DateRegistered': dateRegistered,
        'CompanyRegistrationNumber': companyRegistrationNumber,
        'ApplicationNumber': applicationNumber,
        'CustomerEmail1': customerEmail1,
        'CustomerEmail2': customerEmail2,
        'CustomerEmail3': customerEmail3,
        'IsDoYouHaveAnyBTLPortfulio': isDoYouHaveAnyBTLPortfulio,
        'IsClientAgreement': isClientAgreement,
        'AdminFee': adminFee,
        'AdminFeeWhenPayable': adminFeeWhenPayable,
        'AdviceFee': adviceFee,
        'AdviceFeeWhenPayable': adviceFeeWhenPayable,
        'IsFeesRefundable': isFeesRefundable,
        'FeesRefundable': feesRefundable,
        'IPAdddress': ipAdddress,
        'IPLocation': ipLocation,
        'DeviceType': deviceType,
        'ReportLogo': reportLogo,
        'OfficePhoneNumber': officePhoneNumber,
        'ReportFooter': reportFooter,
        'AdviceFeeType': adviceFeeType,
        'ClientAgreementStatus': clientAgreementStatus,
        'RecommendationAgreementStatus': recommendationAgreementStatus,
        'RecommendationAgreementSignature': recommendationAgreementSignature,
        'IsThereAnySavingsOrInvestments': isThereAnySavingsOrInvestments,
        'IsThereAnyExistingPolicy': isThereAnyExistingPolicy,
        'TaskTitleUrl': taskTitleURL,
        'CustomerAddress1': customerAddress1,
        'CustomerAddress2': customerAddress2,
        'CustomerAddress3': customerAddress3,
        'CustomerPostcode': customerPostcode,
        'CustomerTown': customerTown,
        'CustomerLastName': customerLastName,
        'Id': id,
      };
}
