import 'MortgageCaseInfoEntityModelListModel.dart';

class LocationsModel {
  int userID;
  String user;
  dynamic status;
  String creationDate, updatedDate;
  int versionNumber;
  String title, description;
  bool isInPersonOrOnline;
  int dutDateType;
  String deliveryDate, deliveryTime;
  int workerNumber;
  String skill;
  bool isFixedPrice;
  double hourlyRate, fixedBudgetAmount, netTotalAmount, paidAmount, dueAmount;
  String jobCategory;
  int employeeID, totalBidsNumber;
  bool isArchive;
  String preferedLocation;
  double latitude, longitude;
  String ownerName;
  String thumbnailPath, remarks, taskReferenceNumber;
  String imageServerURL;
  String ownerImageURL;
  String requirements;
  double totalHours;
  String taskTitleURL;
  String ownerProfileURL;
  int totalAcceptedNumber, totalCompletedNumber, companyID;
  String companyName;
  int entityID;
  String entityName;
  double introducerFeeShareAmount,
      introducerPaymentedAmount,
      adviserAmount,
      adviserPaymentedAmount;
  int notificationUnreadTaskCount, notificationTaskCount;
  String addressOfPropertyToBeMortgaged;
  String overAllCaseGrade;
  int caseObservationAssessmentID;
  String namePrefix;
  String lastName;
  int caseStatus;
  List<dynamic> mortgageCaseInfoEntityModelListModel;
  int id;

  LocationsModel({
    this.userID,
    this.user,
    this.status,
    this.creationDate,
    this.updatedDate,
    this.versionNumber,
    this.title,
    this.description,
    this.isInPersonOrOnline,
    this.dutDateType,
    this.deliveryDate,
    this.deliveryTime,
    this.workerNumber,
    this.skill,
    this.isFixedPrice,
    this.hourlyRate,
    this.fixedBudgetAmount,
    this.netTotalAmount,
    this.paidAmount,
    this.dueAmount,
    this.jobCategory,
    this.employeeID,
    this.totalBidsNumber,
    this.isArchive,
    this.preferedLocation,
    this.latitude,
    this.longitude,
    this.ownerName,
    this.thumbnailPath,
    this.remarks,
    this.taskReferenceNumber,
    this.imageServerURL,
    this.ownerImageURL,
    this.requirements,
    this.totalHours,
    this.taskTitleURL,
    this.ownerProfileURL,
    this.totalAcceptedNumber,
    this.totalCompletedNumber,
    this.companyID,
    this.companyName,
    this.entityID,
    this.entityName,
    this.introducerFeeShareAmount,
    this.introducerPaymentedAmount,
    this.adviserAmount,
    this.adviserPaymentedAmount,
    this.notificationUnreadTaskCount,
    this.notificationTaskCount,
    this.addressOfPropertyToBeMortgaged,
    this.overAllCaseGrade,
    this.caseObservationAssessmentID,
    this.namePrefix,
    this.lastName,
    this.caseStatus,
    this.mortgageCaseInfoEntityModelListModel,
    this.id,
  });

  factory LocationsModel.fromJson(Map<String, dynamic> j) {
    var list_MortgageCaseInfoEntityModelList = [];
    try {
      list_MortgageCaseInfoEntityModelList =
          (j['MortgageCaseInfoEntityModelList'] != null)
              ? j['MortgageCaseInfoEntityModelList']
                  .map((i) => MortgageCaseInfoEntityModelListModel.fromJson(i))
                  .toList()
              : [];
    } catch (e) {
      print(e.toString());
    }
    return LocationsModel(
      userID: j['UserId'] ?? 0,
      user: j['User'] ?? '',
      status: j['Status'] ?? '',
      creationDate: j['CreationDate'] ?? '',
      updatedDate: j['UpdatedDate'] ?? '',
      versionNumber: j['VersionNumber'] ?? 0,
      title: j['Title'] ?? '',
      description: j['Description'] ?? '',
      isInPersonOrOnline: j['IsInPersonOrOnline'] ?? false,
      dutDateType: j['DutDateType'] ?? 0,
      deliveryDate: j['DeliveryDate'] ?? '',
      deliveryTime: j['DeliveryTime'] ?? '',
      workerNumber: j['WorkerNumber'] ?? 0,
      skill: j['Skill'] ?? '',
      isFixedPrice: j['IsFixedPrice'] ?? false,
      hourlyRate: j['HourlyRate'] ?? 0.0,
      fixedBudgetAmount: j['FixedBudgetAmount'] ?? 0.0,
      netTotalAmount: j['NetTotalAmount'] ?? 0.0,
      paidAmount: j['PaidAmount'] ?? 0.0,
      dueAmount: j['DueAmount'] ?? 0.0,
      jobCategory: j['JobCategory'] ?? '',
      employeeID: j['EmployeeId'] ?? 0,
      totalBidsNumber: j['TotalBidsNumber'] ?? 0,
      isArchive: j['IsArchive'] ?? false,
      preferedLocation: j['PreferedLocation'] ?? '',
      latitude: j['Latitude'] ?? 0.0,
      longitude: j['Longitude'] ?? 0.0,
      ownerName: j['OwnerName'] ?? '',
      thumbnailPath: j['ThumbnailPath'] ?? '',
      remarks: j['Remarks'] ?? '',
      taskReferenceNumber: j['TaskReferenceNumber'] ?? '',
      imageServerURL: j['ImageServerUrl'] ?? '',
      ownerImageURL: j['OwnerImageUrl'] ?? '',
      requirements: j['Requirements'] ?? '',
      totalHours: j['TotalHours'] ?? 0.0,
      taskTitleURL: j['TaskTitleUrl'] ?? '',
      ownerProfileURL: j['OwnerProfileUrl'] ?? '',
      totalAcceptedNumber: j['TotalAcceptedNumber'] ?? 0,
      totalCompletedNumber: j['TotalCompletedNumber'] ?? 0,
      companyID: j['CompanyId'] ?? 0,
      companyName: j['CompanyName'] ?? '',
      entityID: j['EntityId'] ?? 0,
      entityName: j['EntityName'] ?? '',
      introducerFeeShareAmount: j['IntroducerFeeShareAmount'] ?? 0.0,
      introducerPaymentedAmount: j['IntroducerPaymentedAmount'] ?? 0.0,
      adviserAmount: j['AdviserAmount'] ?? 0.0,
      adviserPaymentedAmount: j['AdviserPaymentedAmount'] ?? 0.0,
      notificationUnreadTaskCount: j['NotificationUnreadTaskCount'] ?? 0,
      notificationTaskCount: j['NotificationTaskCount'] ?? 0,
      addressOfPropertyToBeMortgaged: j['AddressOfPropertyToBeMortgaged'] ?? '',
      overAllCaseGrade: j['OverAllCaseGrade'] ?? '',
      caseObservationAssessmentID: j['CaseObservationAssessmentId'] ?? 0,
      namePrefix: j['NamePrefix'] ?? '',
      lastName: j['LastName'] ?? '',
      caseStatus: j['CaseStatus'] ?? 0,
      mortgageCaseInfoEntityModelListModel:
          list_MortgageCaseInfoEntityModelList,
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'UserId': userID,
        'User': user,
        'Status': status,
        'CreationDate': creationDate,
        'UpdatedDate': updatedDate,
        'VersionNumber': versionNumber,
        'Title': title,
        'Description': description,
        'IsInPersonOrOnline': isInPersonOrOnline,
        'DutDateType': dutDateType,
        'DeliveryDate': deliveryDate,
        'DeliveryTime': deliveryTime,
        'WorkerNumber': workerNumber,
        'Skill': skill,
        'IsFixedPrice': isFixedPrice,
        'HourlyRate': hourlyRate,
        'FixedBudgetAmount': fixedBudgetAmount,
        'NetTotalAmount': netTotalAmount,
        'PaidAmount': paidAmount,
        'DueAmount': dueAmount,
        'JobCategory': jobCategory,
        'EmployeeId': employeeID,
        'TotalBidsNumber': totalBidsNumber,
        'IsArchive': isArchive,
        'PreferedLocation': preferedLocation,
        'Latitude': latitude,
        'Longitude': longitude,
        'OwnerName': ownerName,
        'ThumbnailPath': thumbnailPath,
        'Remarks': remarks,
        'TaskReferenceNumber': taskReferenceNumber,
        'ImageServerUrl': imageServerURL,
        'OwnerImageUrl': ownerImageURL,
        'Requirements': requirements,
        'TotalHours': totalHours,
        'TaskTitleUrl': taskTitleURL,
        'OwnerProfileUrl': ownerProfileURL,
        'TotalAcceptedNumber': totalAcceptedNumber,
        'TotalCompletedNumber': totalCompletedNumber,
        'CompanyId': companyID,
        'CompanyName': companyName,
        'EntityId': entityID,
        'EntityName': entityName,
        'IntroducerFeeShareAmount': introducerFeeShareAmount,
        'IntroducerPaymentedAmount': introducerPaymentedAmount,
        'AdviserAmount': adviserAmount,
        'AdviserPaymentedAmount': adviserPaymentedAmount,
        'NotificationUnreadTaskCount': notificationUnreadTaskCount,
        'NotificationTaskCount': notificationTaskCount,
        'AddressOfPropertyToBeMortgaged': addressOfPropertyToBeMortgaged,
        'OverAllCaseGrade': overAllCaseGrade,
        'CaseObservationAssessmentId': caseObservationAssessmentID,
        'NamePrefix': namePrefix,
        'LastName': lastName,
        'CaseStatus': caseStatus,
        'Id': id,
      };
}
