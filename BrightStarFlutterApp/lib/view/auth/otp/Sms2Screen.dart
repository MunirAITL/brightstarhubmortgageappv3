import 'package:Bright_Star/config/AppDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/form_validate/UserProfileVal.dart';
import 'package:Bright_Star/controller/helper/auth/otp/LoginMobOtpPostHelper.dart';
import 'package:Bright_Star/controller/helper/auth/otp/SendOtpNotiHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:Bright_Star/view/auth/otp/Sms3Screen.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class Sms2Screen extends StatefulWidget {
  final String mobile;
  Sms2Screen({Key key, this.mobile = ''}) : super(key: key);
  @override
  State createState() => _Sms2ScreenState();
}

class _Sms2ScreenState extends State<Sms2Screen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _phoneController = TextEditingController();

  Color brownColor;
  Color btnTxtColor;
  bool isOtpCalled = false;
  bool isLoading = false;
  final int timeOut = 60;

  wsLoginMobileOtpPostAPI() async {
    try {
      await NetworkMgr()
          .postData<MobileUserOtpPostAPIModel, Null>(
        context: context,
        url: Server.LOGIN_MOBILE_OTP_POST_URL,
        param: LoginMobOtpPostHelper().getParam(
          mobileNumber: _phoneController.text.trim(),
        ),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                //final msg = model.messages.postUserotp[0].toString();
                //showToast(msg: msg, which: 1);
                if (mounted) {
                  wsSendOtpNotiAPI(model.responseData.userOTP.id);
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.messages.postUserotp[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  wsSendOtpNotiAPI(int otpId) async {
    try {
      await NetworkMgr()
          .getData<SendOtpNotiAPIModel, Null>(
        context: context,
        url: SendOtpNotiHelper().getUrl(otpId: otpId),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                //final msg = model.messages.postUserotp[0].toString();
                //showToast(msg: msg, which: 1);
                Get.to(
                  () => Sms3Screen(
                    mobileUserOTPModel: model.responseData.userOTP,
                  ),
                ).then((value) {
                  //callback(route);
                });
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.messages.postUserotp[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _phoneController.dispose();
    try {} catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      if (widget.mobile.length == 0) {
        brownColor = Colors.grey[400];
        btnTxtColor = Colors.black;
      } else {
        brownColor = MyTheme.brownColor;
        btnTxtColor = Colors.white;
      }

      _phoneController.text = widget.mobile;
      _phoneController.addListener(() {
        if (mounted) {
          if (_phoneController.text.trim().length <
              UserProfileVal.PHONE_LIMIT) {
            brownColor = Colors.grey[400];
            btnTxtColor = Colors.black;
          } else {
            brownColor = MyTheme.brownColor;
            btnTxtColor = Colors.white;
          }
          setState(() {});
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: MyTheme.themeData.accentColor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: Container(
          width: getW(context),
          //height: getH(context),
          child: drawSendBox(),
        ),
      ),
    );
  }

  drawSendBox() {
    return Form(
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 5)),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Enter your mobile number",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              txtLineSpace: 1.0,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              controller: _phoneController,
              keyboardType: TextInputType.phone,
              maxLength: 15,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize:
                    getTxtSize(context: context, txtSize: MyTheme.txtSize + .3),
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                hintStyle: new TextStyle(
                    color: Colors.grey,
                    fontSize: ResponsiveFlutter.of(context)
                        .fontSize(MyTheme.txtSize + .8)),
                hintText: AppDefine.COUNTRY_CODE + " xxxxxx",
                //prefixText: AppDefine.COUNTRY_CODE,
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                border: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              if (_phoneController.text.trim().length >=
                  UserProfileVal.PHONE_LIMIT) {
                wsLoginMobileOtpPostAPI();
              } else {
                showToast(msg: 'Please enter your valid phone number');
              }
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                width: getW(context),
                height: getHP(context, 7),
                //color: MyTheme.brownColor,
                decoration: new BoxDecoration(
                  color: brownColor,
                  borderRadius: new BorderRadius.circular(10),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Txt(
                      txt: "Next",
                      txtColor: btnTxtColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true,
                    ),
                    SizedBox(width: 20),
                    Icon(
                      Icons.arrow_forward,
                      color: (brownColor == MyTheme.brownColor)
                          ? Colors.white
                          : Colors.black,
                      size: 25,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
