import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/helper/auth/otp/LoginMobOtpPutHelper.dart';
import 'package:Bright_Star/controller/helper/auth/otp/LoginRegOtpFBHelper.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:Bright_Star/model/json/auth/otp/MobileUserOTPModel.dart';
import 'package:Bright_Star/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:Bright_Star/view/allacc/AllAccScreen.dart';
import 'package:Bright_Star/view/dashboard/DashboardScreen.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class Sms3Screen extends StatefulWidget {
  final MobileUserOTPModel mobileUserOTPModel;
  const Sms3Screen({
    Key key,
    @required this.mobileUserOTPModel,
  }) : super(key: key);
  @override
  State createState() => new _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Screen>
    with SingleTickerProviderStateMixin, Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;

  wsLoginMobileOtpPutAPI(String otpCode) async {
    try {
      await NetworkMgr()
          .postData<MobileUserOtpPutAPIModel, Null>(
        context: context,
        url: Server.LOGIN_MOBILE_OTP_PUT_URL,
        isPost: false,
        param: LoginMobOtpPutHelper().getParam(
          mobileNumber: widget.mobileUserOTPModel.mobileNumber,
          otpCode: otpCode,
        ),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                //final msg = model.messages.postUserotp[0].toString();
                //showToast(msg: msg, which: 1);
                if (mounted) {
                  wsLoginMobileFBPostAPI(model.responseData.userOTP, otpCode);
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.postUserotp[0];
                  showToast(msg: err, which: 0);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  wsLoginMobileFBPostAPI(
      MobileUserOTPModel mobileUserOTPModel, String otpCode) async {
    try {
      await NetworkMgr()
          .postData<LoginRegOtpFBAPIModel, Null>(
        context: context,
        url: Server.LOGIN_REG_OTP_FB_URL,
        param: LoginRegOtpFBHelper().getParam(
          mobileNumber: mobileUserOTPModel.mobileNumber,
          otpCode: otpCode,
        ),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                Get.off(() => AllAccScreen(
                      isLoggedIn: false,
                      userModel: model.responseData.user,
                    ));
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.login[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {}

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return GestureDetector(
      onTap: () {
        Get.back(result: widget.mobileUserOTPModel.mobileNumber);
      },
      child: Txt(
        txt: "I didn't get a code",
        txtColor: MyTheme.brownColor,
        txtSize: MyTheme.txtSize,
        txtAlign: TextAlign.center,
        isBold: false,
      ),
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 80,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: 80.0,
                  ),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        key: _scaffoldKey,
        backgroundColor: MyTheme.themeData.accentColor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Get.back(result: widget.mobileUserOTPModel.mobileNumber);
            },
          ),
        ),
        body: new Container(
            width: getW(context),
            //height: getH(context),
//        padding: new EdgeInsets.only(bottom: 16.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(height: getHP(context, 1)),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Txt(
                    txt: "Enter the code that was sent to",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.0,
                    isBold: false,
                  ),
                ),
                _getInputField,
                _getResendButton,
                _getOtpKeyboard
              ],
            )),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      child: Txt(
          txt: digit != null ? digit.toString() : "",
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.start,
          isBold: false),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .6,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;

          final otpCode = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode.length == SMS_AUTH_CODE_LEN) {
            //signInWithPhoneNumber(otpCode);
            wsLoginMobileOtpPutAPI(otpCode);
          }
        }
      });
    }
  }
}
