import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/IcoTxtIco.dart';
import 'package:Bright_Star/view/mywidgets/TCView.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'Sms2Screen.dart';

class Sms1Screen extends StatefulWidget {
  @override
  State createState() => _Sms1ScreenState();
}

class _Sms1ScreenState extends State<Sms1Screen> with Mixin {
  /*wsLoginMobileFBPostAPI() async {
    try {
      await NetworkMgr()
          .postData<LoginRegOtpFBAPIModel, Null>(
        context: context,
        url: Server.LOGIN_REG_OTP_FB_URL,
        param: LoginRegOtpFBHelper().getParam(
          mobileNumber: "923009231382",
          otpCode: "143336",
        ),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {} catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.login[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }*/

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        //resizeToAvoidBottomPadding: false,

        appBar: AppBar(
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(height: getHP(context, 5)),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Txt(
                txt: "Please give your mobile number to start using the app",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + 0.4,
                txtAlign: TextAlign.center,
                txtLineSpace: 1.2,
                isBold: false,
              ),
            ),
            drawCenterMobileCode(),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: TCView(
                screenName: 'Log in',
                txt1Color: Colors.black,
                txt2Color: Colors.blue,
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCenterMobileCode() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: getHP(context, 45),
        width: getWP(context, 80),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(20.0),
            topRight: const Radius.circular(20.0),
            bottomLeft: const Radius.circular(20.0),
            bottomRight: const Radius.circular(20.0),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                txt: "Please give your mobile number",
                txtColor: MyTheme.brownColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
            GestureDetector(
              onTap: () async {
                Get.to(
                  () => Sms2Screen(),
                ).then((value) {
                  //callback(route);
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: IcoTxtIco(
                  leftIcon: Icons.mobile_screen_share,
                  txt: "Log in with Mobile",
                  rightIcon: Icons.arrow_right,
                  iconColor: Colors.grey,
                  leftIconSize: 40,
                  rightIconSize: 50,
                ),
              ),
            ),
            SizedBox(height: getHP(context, 5)),
            Expanded(
              child: Container(
                width: getWP(context, 85),
                //height: getHP(context, 20),
                child: Image.asset(
                  'assets/images/screens/login/login_mobile_bg.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
