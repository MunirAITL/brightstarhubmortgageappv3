import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/form_validate/UserProfileVal.dart';
import 'package:Bright_Star/controller/helper/auth/LoginHelper.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/ForgotAPIModel.dart';
import 'package:Bright_Star/model/json/auth/LoginAPIModel.dart';
import 'package:Bright_Star/view/allacc/AllAccScreen.dart';
import 'package:Bright_Star/view/auth/RegScreen.dart';
import 'package:Bright_Star/view/dashboard/DashboardScreen.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/InputBox.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'ForgotDialog.dart';
import 'otp/Sms1Screen.dart';
import 'otp/Sms2Screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  State createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Mixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text = "anisur5001@yopmail.com";
        //"anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _pwd.text = "123456";
      }
    } catch (e) {}
  }

  wsLoginAPI() async {
    try {
      if (validate()) {
        await NetworkMgr()
            .postData<LoginAPIModel, Null>(
          context: context,
          url: Server.LOGIN_URL,
          param: LoginHelper()
              .getParam(email: _email.text.trim(), pwd: _pwd.text.trim()),
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  try {
                    await DBMgr.shared
                        .setUserProfile(user: model.responseData.user);
                    await ProfileHelper().downloadProfileImages(
                        context: context, userModel: model.responseData.user);
                  } catch (e) {
                    log(e.toString());
                  }
                  if (Server.isTest) {
                    Get.off(() => DashBoardScreen()).then((value) {
                      //callback(route);
                    });
                  } else {
                    if (model.responseData.user.isMobileNumberVerified) {
                      Get.off(() => DashBoardScreen()).then((value) {
                        //callback(route);
                      });
                    } else {
                      Get.to(() => Sms2Screen(
                            mobile: '',
                          )).then((value) {
                        //callback(route);
                      });
                    }
                  }

                  /*if (Server.isTest) {
                    Get.off(() => AllAccScreen(
                          isLoggedIn: false,
                          userModel: model.responseData.user,
                        ));
                  } else {
                    if (model.responseData.user.isMobileNumberVerified) {
                      Get.off(() => AllAccScreen(
                            isLoggedIn: false,
                            userModel: model.responseData.user,
                          ));
                    } else {
                      Get.to(() => Sms2Screen(
                            mobile: '',
                          )).then((value) {
                        //callback(route);
                      });
                    }
                  }*/
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.errorMessages.login[0].toString();
                    showToast(msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  wsForgotAPI(emailStr) async {
    try {
      await NetworkMgr().postData<ForgotAPIModel, Null>(
        context: context,
        url: Server.FORGOT_URL,
        param: {'email': emailStr},
      ).then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final msg = model.messages.forgotpassword[0].toString();
                showToast(msg: msg, which: 1);
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.forgotpassword[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  validate() {
    if (!UserProfileVal().isNotEmpty(_email, 'Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.loginBGColor,
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Center(
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: getWP(context, 14),
                      width: getWP(context, 14),
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new AssetImage("assets/images/logo/eplus.png"),
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 10),
                      child: Card(
                        child: Container(
                          color: Colors.white,
                          width: getW(context),
                          child: Column(
                            children: [
                              SizedBox(height: 40),
                              drawEmailBox(),
                              drawPwdBox(),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 20, right: 20),
                                child: BSBtn(
                                    txt: "Sign in",
                                    height: getHP(context, 7),
                                    callback: () {
                                      wsLoginAPI();
                                    }),
                              ),
                              //drawOtpBtn(),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: GestureDetector(
                              onTap: () {
                                showForgotDialog(
                                    context: context,
                                    email: _email,
                                    callback: (String emailStr) {
                                      //
                                      if (emailStr != null) {
                                        if (emailStr.length > 0) {
                                          wsForgotAPI(emailStr);
                                        } else {
                                          showToast(msg: "Missing email");
                                        }
                                      }
                                    });
                              },
                              child: Txt(
                                  txt: "Forgotten password",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: GestureDetector(
                              onTap: () async {
                                Get.to(() => RegScreen(),
                                        fullscreenDialog: true)
                                    .then((value) {
                                  //callback(route);
                                });
                              },
                              child: Txt(
                                  txt: "Create an account",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawEmailBox() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Txt(
                txt: 'Email address',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          InputBox(
            ctrl: _email,
            lableTxt: "",
            kbType: TextInputType.emailAddress,
            len: 50,
            isPwd: false,
          ),
        ],
      ),
    );
  }

  drawPwdBox() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Txt(
                txt: 'Password',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          InputBox(
            ctrl: _pwd,
            lableTxt: "",
            kbType: TextInputType.emailAddress,
            len: 20,
            isPwd: true,
          ),
        ],
      ),
    );
  }

  drawOtpBtn() {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 10),
          Txt(
            txt: "or",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: true,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
            child: BSBtn(
                txt: "Log in with Mobile",
                height: getHP(context, 7),
                callback: () {
                  Get.to(
                    () => Sms1Screen(),
                  ).then((value) {
                    //callback(route);
                  });
                }),
          ),
        ],
      ),
    );
  }
}
