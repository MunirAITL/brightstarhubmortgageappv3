import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/form_validate/UserProfileVal.dart';
import 'package:Bright_Star/controller/helper/auth/LoginHelper.dart';
import 'package:Bright_Star/controller/helper/auth/RegHelper.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/LoginAPIModel.dart';
import 'package:Bright_Star/model/json/auth/RegAPIModel.dart';
import 'package:Bright_Star/view/allacc/AllAccScreen.dart';
import 'package:Bright_Star/view/auth/otp/Sms2Screen.dart';
import 'package:Bright_Star/view/dashboard/DashboardScreen.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/DatePickerView.dart';
import 'package:Bright_Star/view/mywidgets/InputBox.dart';
import 'package:Bright_Star/view/mywidgets/SwitchView.dart';
import 'package:Bright_Star/view/mywidgets/TCView.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class RegScreen extends StatefulWidget {
  @override
  State createState() => _RegScreenState();
}

enum genderEnum { male, female }

class _RegScreenState extends State<RegScreen> with Mixin {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  final _compName = TextEditingController();

  bool _isSwitch = true;

  String dob = "";

  String dobDD = "";

  String dobMM = "";

  String dobYY = "";

  genderEnum _gender = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  final ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _compName.dispose();
    _scrollController.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {
      log(e.toString());
    }
  }

  wsRegAPI() async {
    try {
      if (validate2()) {
        setState(() {
          isLoading = true;
        });
        final param = RegHelper().getParam(
          email: _email.text.trim(),
          pwd: _pwd.text.trim(),
          fname: _fname.text.trim(),
          lname: _lname.text.trim(),
          phone: _mobile.text.trim(),
          dob: dob,
          dobDD: dobDD,
          dobMM: dobMM,
          dobYY: dobYY,
        );
        await NetworkMgr()
            .postData<RegAPIModel, Null>(
          context: context,
          url: Server.REG_URL,
          param: param,
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  //  recall login to get cookie
                  await NetworkMgr()
                      .postData<LoginAPIModel, Null>(
                    context: context,
                    url: Server.LOGIN_URL,
                    param: LoginHelper().getParam(
                        email: _email.text.trim(), pwd: _pwd.text.trim()),
                  )
                      .then((model2) async {
                    if (model2 != null && mounted) {
                      try {
                        setState(() {
                          isLoading = false;
                        });
                        if (model2.success) {
                          try {
                            await DBMgr.shared
                                .setUserProfile(user: model2.responseData.user);
                            await ProfileHelper().downloadProfileImages(
                                context: context,
                                userModel: model2.responseData.user);
                          } catch (e) {
                            log(e.toString());
                          }

                          try {
                            if (model
                                .responseData.user.isMobileNumberVerified) {
                              Get.off(() => DashBoardScreen()).then((value) {
                                //callback(route);
                              });

                              /*Get.off(() => AllAccScreen(
                                    isLoggedIn: false,
                                    userModel: model.responseData.user,
                                    otpId: model.responseData.otpId,
                                    otpMobileNumber:
                                        model.responseData.otpMobileNumber,
                                  ));*/
                            } else {
                              Get.to(
                                () => Sms2Screen(
                                  mobile: _mobile.text,
                                ),
                              ).then((value) {
                                //callback(route);
                              });
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                        } else {
                          try {
                            if (mounted) {
                              final err =
                                  model2.errorMessages.login[0].toString();
                              showToast(msg: err);
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                        }
                      } catch (e) {
                        log(e.toString());
                      }
                    }
                  });
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  setState(() {
                    isLoading = false;
                  });
                  if (mounted) {
                    final err = model.errorMessages.register[0].toString();
                    showToast(msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    } else if (!UserProfileVal().isEmailOK(_email)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    } else if (_isSwitch && !UserProfileVal().isComNameOK(_compName)) {
      return false;
    }
    return true;
  }

  validate2() {
    if (!UserProfileVal().isDOBOK(dob)) {
      return false;
    }
    return true;
  }

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon((isStep2) ? Icons.arrow_back : Icons.close,
                          color: Colors.black),
                      onPressed: () async {
                        (!isStep2) ? Get.back() : refreshStep1();
                      }),

                  title: Txt(
                      txt: "Join Bright Star",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: (!isStep2) ? drawRegView1() : drawRegView2(),
          ),
        ),
      ),
    );
  }

  drawRegView1() {
    Future.delayed(Duration.zero, () async {
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    InputBox(
                      ctrl: _fname,
                      lableTxt: "First name",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _lname,
                      lableTxt: "Last name",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _email,
                      lableTxt: "Email",
                      kbType: TextInputType.emailAddress,
                      len: 50,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _mobile,
                      lableTxt: "Phone Number",
                      kbType: TextInputType.phone,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _pwd,
                      lableTxt: "Password",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                  ],
                ),
              ),
              /*SizedBox(height: 20),
              Txt(
                txt: "or",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: ListTile(
                  tileColor: Colors.blue.shade800,
                  leading: Image(
                    image: AssetImage(
                      "assets/images/icons/fb_icon.png",
                    ),
                    //color: null,
                    width: 20,
                    height: 20,
                  ),
                  minLeadingWidth: 0,
                  title: MaterialButton(
                    child: Container(
                      width: double.infinity,
                      child: Align(
                        alignment: Alignment(-1, 0),
                        child: Text(
                          "Continue with Facebook",
                        ),
                      ),
                    ),
                    onPressed: () async {
                      //
                    },
                  ),
                ),
              ),*/

              SizedBox(height: 20),
              drawCompSwitch(),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: BSBtn(
                    txt: "Continue",
                    height: getHP(context, 7),
                    callback: () {
                      if (validate1()) {
                        isStep2 = true;
                        setState(() {});
                      }
                    }),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
                child: TCView(
                    screenName: 'Continue', txt2Color: MyTheme.brownColor),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    Future.delayed(Duration.zero, () async {
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });

    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 20),
      child: Container(
        width: getW(context),
        //height: getH(context),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Txt(
                      txt: "Provide a date of birth",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Txt(
                      txt:
                          "To signup, you must be 18 or older. Other people won't see your birthday.",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - 0.4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: DatePickerView(
                    cap: null,
                    dt: (dob == '') ? 'Select a date' : dob,
                    initialDate: dateDOBlast,
                    firstDate: dateDOBfirst,
                    lastDate: dateDOBlast,
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat('dd-MMM-yyyy')
                                .format(value)
                                .toString();
                            final dobArr = dob.toString().split('-');
                            this.dobDD = dobArr[0];
                            this.dobMM = dobArr[1];
                            this.dobYY = dobArr[2];
                          } catch (e) {
                            log(e.toString());
                          }
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 20),
                drawGender(),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: BSBtn(
                      txt: "Get Started",
                      height: getHP(context, 7),
                      callback: () {
                        wsRegAPI();
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Container(
        //color: Colors.yellow,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Gender",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: MyTheme.brownColor,
                disabledColor: MyTheme.brownColor,
                selectedRowColor: MyTheme.brownColor,
                indicatorColor: MyTheme.brownColor,
                toggleableActiveColor: MyTheme.brownColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.male;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.male,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Male",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.female;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.female,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Female",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCompSwitch() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: Txt(
                      txt:
                          "Do you have a mortgage company? Otherwise we will recommend to you.",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                      txtLineSpace: 1.5,
                    ),
                  ),
                  //SizedBox(width: 10),
                  SwitchView(
                    value: _isSwitch,
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InputBox(
                      ctrl: _compName,
                      lableTxt: "Company Name",
                      kbType: TextInputType.text,
                      len: 100,
                      isPwd: false,
                      leftPad: 0,
                      rightPad: 0,
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
