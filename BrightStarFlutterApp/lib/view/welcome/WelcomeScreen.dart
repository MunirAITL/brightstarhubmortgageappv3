import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/network/CookieMgr.dart';
import 'package:Bright_Star/controller/services/PushNotificationService.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/view/auth/LoginScreen.dart';
import 'package:Bright_Star/view/dashboard/DashboardScreen.dart';
import 'package:Bright_Star/view/dashboard/more/settings/CaseAlertScreen.dart';
import 'package:Bright_Star/view/mywidgets/ImgFade.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/splash/SplashScreen.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isDoneCookieCheck = false;
  double widget1Opacity = 0.0;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
      //SystemChrome.setSystemUIOverlayStyle(
      //  SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));

      Future.delayed(Duration(milliseconds: 300), () {
        widget1Opacity = 1;
      });

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      final pushNotificationService =
          PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(
          callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          Get.off(
            () => DashBoardScreen(),
          ).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        } else {
          setState(() {
            isDoneCookieCheck = true;
          });
        }
      } catch (e) {}
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        Get.off(
          () => CaseAlertScreen(
            message: message,
          ),
        ).then((value) {
          Get.to(
            () => DashBoardScreen(),
          ).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        });
      }
    } catch (e) {
      //log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final x = getW(context);
    log(x.toString());
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (!isDoneCookieCheck)
              ? SplashScreen()
              : Container(
                  height: getH(context),
                  child: Stack(children: <Widget>[
                    new Container(
                      height: getHP(context, 70),
                      /*decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new AssetImage(
                              "assets/images/screens/splash/splash_bs.png"),
                          fit: BoxFit.cover,
                        ),
                      ),*/
                      child: ImgFade(
                          url: "assets/images/screens/splash/splash_bs.png"),
                    ),
                    Positioned(
                      top: 0,
                      child: Stack(children: <Widget>[
                        Container(
                          color: Colors.white,
                          width: getW(context),
                          height: getHP(context, 10),
                        ),
                        Positioned(
                          left: getWP(context, 5),
                          top: getHP(context, 1),
                          child: Image.asset(
                            "assets/images/logo/logo.png",
                            width: getWP(context, MyTheme.logoWidth),
                          ),
                        ),
                      ]),
                    ),
                    Positioned(
                      top: getHP(context, 80),
                      child: Container(
                        width: getW(context),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30, right: 30),
                          child: MaterialButton(
                            color: MyTheme.cyanColor,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 15, bottom: 15),
                              child: new Txt(
                                txt: "Get Started",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: true,
                              ),
                            ),
                            onPressed: () async {
                              Get.to(
                                () => LoginScreen(),
                              ).then((value) {
                                //callback(route);
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
        ),
      ),
    );
  }
}
