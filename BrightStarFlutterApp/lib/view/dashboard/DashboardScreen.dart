import 'dart:convert';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/config/dashboard/NewCaseCfg.dart';
import 'package:Bright_Star/config/dashboard/UserNotesCfg.dart';
import 'package:Bright_Star/controller/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:Bright_Star/controller/helper/tab_newcase/UserNotesHelper.dart';
import 'package:Bright_Star/controller/network/CookieMgr.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:Bright_Star/model/json/tab_mycases/UserNotePutAPIModel.dart';
import 'package:Bright_Star/model/json/tab_newcase/UserNotesModel.dart';
import 'package:Bright_Star/model/json/tab_mycases/UserNotesAPIModel.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/dashboard/new_case/CaseNoteDoneScreen.dart';
import 'package:Bright_Star/view/dashboard/new_case/PostNewCaseScreen.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/drawer/AppDrawer.dart';
import 'package:Bright_Star/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:Bright_Star/view/mywidgets/dropdown/DropListModel.dart';
import 'package:Bright_Star/view/welcome/WelcomeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  State createState() => DashBoardScreenState();
}

class DashBoardScreenState extends State<DashBoardScreen>
    with Mixin, StateListener, TickerProviderStateMixin {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  final GlobalKey _ddKey = GlobalKey();

  List<dynamic> listUserNotesModel = [];
  UserModel userModel;

  bool isShowSlider = true;
  bool isLoading = false;

  int pageStart = 0;

  int totalMsg = 4;
  int totalNoti = 2;

  StateProvider _stateProvider;

  //  dropdown title
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt = OptionItem(id: null, title: "I am looking for...");
  Size caseDDSize;

  getSizeAndPosition() {
    RenderBox _cardBox = _ddKey.currentContext.findRenderObject();
    caseDDSize = _cardBox.size;
    log(caseDDSize.height.toString());
    setState(() {});
  }

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_newcase_reload_case_api) {
      _getRefreshData();
    } else if (state == ObserverState.STATE_CHANGED_logout) {
      CookieMgr().delCookiee();
      Get.offAll(
        () => WelcomeScreen(),
      ).then((value) {
        //callback(route);
      });
    }
  }

  wsFcmDeviceInfo() async {
    try {
      final param = await FcmDeviceInfoHelper().getParam(
        userId: userModel.id,
      );
      log(param);
      await NetworkMgr()
          .postData<FcmDeviceInfoAPIModel, Null>(
        context: context,
        url: Server.FCM_DEVICE_INFO_URL,
        param: param,
        isLoading: false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {}
          } catch (e) {
            log(e.toString());
          }
        } else {
          log("not in");
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsUserNotesAPI() async {
    try {
      setState(() {
        isLoading = true;
      });

      final url = UserNotesHelper().getUrl(
        userModel: userModel,
        status: UserNotesCfg.PENDING.toString(),
      );
      log(url);
      await NetworkMgr()
          .getData<UserNoteByEntityAPIModel, Null>(
        context: context,
        url: url,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                listUserNotesModel = model.responseData.userNotes;
                if (listUserNotesModel.length > 0) {
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  setState(() {
                    isLoading = false;
                  });
                }
              } catch (e) {
                setState(() {
                  isLoading = false;
                });
                log(e.toString());
              }
            } else {
              setState(() {
                isLoading = false;
              });
            }
          } catch (e) {
            setState(() {
              isLoading = false;
            });
            log(e.toString());
          }
        }
      });
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      log(e.toString());
    }
  }

  wsCaseNoteDoneClicked(UserNotesModel userNoteModel) async {
    try {
      final param = UserNotesHelper().getParam(userNoteModel: userNoteModel);
      log(json.encode(param));
      await NetworkMgr()
          .postData<UserNotePutAPIModel, Null>(
        context: context,
        url: Server.USERNOTE_PUT_URL,
        isPost: false,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                for (UserNotesModel userNoteModel2 in listUserNotesModel) {
                  if (userNoteModel2.id == model.responseData.userNote.id) {
                    listUserNotesModel.remove(userNoteModel2);
                    break;
                  }
                }
                setState(() {});
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.messages.post_user[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> _getRefreshData() async {
    //pageNo = 0;
    listUserNotesModel.clear();
    wsUserNotesAPI();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    listUserNotesModel = null;
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    } catch (e) {}
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
    } catch (e) {}
    try {
      int i = 0;
      List<OptionItem> list = [];
      for (var map in NewCaseCfg.listCreateNewCase) {
        list.add(OptionItem(id: (i++).toString(), title: map['title']));
      }
      caseDD = DropListModel(list);
    } catch (e) {}
    try {
      userModel = await DBMgr.shared.getUserProfile();
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      Future.delayed(Duration(seconds: 3 * NewCaseCfg.listSliderImages.length),
          () {
        if (mounted) {
          setState(() {
            isShowSlider = false;
          });
        }
      });
    } catch (e) {}
    try {
      await _getRefreshData();
      wsFcmDeviceInfo();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        key: _drawerKey,
        //drawerScrimColor: Colors.grey.withOpacity(0),
        endDrawer: AppDrawer(
          userModel: userModel,
          totalMsg: totalMsg,
          totalNoti: totalNoti,
          isRightClose: true,
          isNewCaseTab: true,
          isMyCasesTab: false,
          isTimelineTab: false,
          isNotiTab: false,
        ),
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 160,
                  elevation: 0,
                  toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  title: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Container(
                      width: getWP(context, MyTheme.logoWidth),
                      child: Image.asset(
                        "assets/images/logo/logo.png",
                        //fit: BoxFit.fitWidth,
                        //width: getWP(context, 80),
                      ),
                    ),
                  ),
                  actions: [
                    Builder(
                      builder: (context) => Padding(
                        padding: const EdgeInsets.only(right: 30),
                        child: GestureDetector(
                          onTap: () {
                            Scaffold.of(context).openEndDrawer();
                          },
                          child: Image.asset(
                            "assets/images/icons/menu_icon.png",
                            width: 25,
                          ),
                        ),
                      ),
                    ),
                  ],

                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    final name = (userModel != null) ? userModel.firstName : '';
    return ListView(
      shrinkWrap: true,
      primary: true,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Container(
            color: Colors.white,
            child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    Txt(
                      txt: "Hello " + name + ",",
                      txtColor: MyTheme.brownColor,
                      txtSize: MyTheme.txtSize + 1.3,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                    SizedBox(height: 10),
                    Txt(
                      txt: "welcome back",
                      txtColor: MyTheme.brownColor,
                      txtSize: MyTheme.txtSize + 1,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                    SizedBox(height: 20),
                    Txt(
                      txt:
                          "chdLM WHj QmKlvst ASF SDAFA SFAS DFASD  KC6yK 1T:AP A91bHbp vRicHC ubE2ALvUk JXOCy k7HF fBVU fGS N5qR aRKJ 3ejS msfwP I0F_A Rn SmE FA4q GVsI 0Kzi nMrMoyiY5 TR48 8vCseRPxl xIC1PvP3 HSkpcefzTFwFm016zw o",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize / 1.6,
                      txtLineSpace: 1.8,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                  ],
                )),
          ),
        ),
        //(isShowSlider) ? drawGridImages() : SizedBox(),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Container(
            color: MyTheme.cyanColor,
            child: Column(
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Txt(
                    txt: "Want to submit an enquiry?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Container(
                    width: getW(context),
                    child: Stack(
                      //alignment: Alignment.center,
                      //mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Container(
                            key: _ddKey,
                            width: getWP(context, 50),
                            //height: getHP(context, 10),
                            //color: Colors.black,
                            child: DropDownPicker(
                              cap: null,
                              bgColor: Colors.white,
                              txtColor: (caseOpt.id == null)
                                  ? HexColor.fromHex("#85868C")
                                  : Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              ddTitleSize: .4,
                              ddRadius: 5,
                              itemSelected: caseOpt,
                              dropListModel: caseDD,
                              onOptionSelected: (optionItem) {
                                caseOpt = optionItem;
                                setState(() {});
                              },
                            ),
                          ),
                        ),
                        (caseDDSize == null)
                            ? SizedBox()
                            : Positioned(
                                right: 20,
                                top: 5,
                                child: Container(
                                  width: getWP(context, 25),
                                  //alignment: Alignment.center,
                                  //color: Colors.black,
                                  child: BSBtn(
                                      txt: "Go",
                                      radius: 5,
                                      leadingSpace: 20,
                                      height: caseDDSize.height - 10,
                                      callback: () {
                                        if (caseOpt.id != null) {
                                          Get.to(
                                            () => PostNewCaseScreen(
                                              indexCase: int.parse(caseOpt.id),
                                            ),
                                          ).then((value) {
                                            //callback(route);
                                          });
                                        } else {
                                          showToast(
                                              msg:
                                                  "Please choose case from the list");
                                        }
                                      }),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),

                //SizedBox(width: 10),
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Container(
            color: MyTheme.cyanColor,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Txt(
                    txt: "Request a call back",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Container(
                    width: getWP(context, 25),
                    child: BSBtn(
                        txt: "Go",
                        radius: 5,
                        //leadingSpace: 20,
                        height: getHP(context, 5),
                        callback: () {
                          //
                        }),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),

        (listUserNotesModel.length > 0) ? drawActionReqView() : SizedBox(),

        (listUserNotesModel.length > 0)
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: (listUserNotesModel.length > 4) ? 30 : 0,
                        left: 30,
                        right: 30),
                    child: Txt(
                      txt: "Cases",
                      txtColor: MyTheme.brownColor,
                      txtSize: MyTheme.txtSize + 0.3,
                      txtAlign: TextAlign.start,
                      isBold: true,
                    ),
                  ),
                  drawRecentCases(),
                ],
              )
            : SizedBox(),

        SizedBox(height: getHP(context, 5)),
      ],
    );
  }

  drawRecentCases() {
    try {
      return Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
        child: Container(
          color: Colors.white,
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Container(
                  color: MyTheme.brownColor,
                  height: getHP(context, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Txt(
                              txt: "TYPE",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - 1,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ),
                      Flexible(
                        child: Txt(
                            txt: "CASE No",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize - 1,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Txt(
                              txt: "APPLICATION STARTED",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - 1,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: listUserNotesModel.length,
                    itemBuilder: (context, index) {
                      final UserNotesModel userNotesModel =
                          listUserNotesModel[index];

                      var now =
                          Jiffy(userNotesModel.creationDate).format("dd/MM/yy");

                      return GestureDetector(
                        onTap: () async {
                          Get.to(() => CaseNoteDoneScreen(
                              userNotesModel: userNotesModel)).then((isOk) {
                            if (isOk) {
                              //  api call
                              //wsCaseNoteDoneClicked(userNotesModel);
                              log("ff");
                            }
                          });
                          /*try {
                            await Get.dialog(CaseNoteDoneDialog(
                                userNotesModel: userNotesModel,
                                callback: (isOk) {
                                  if (isOk) {
                                    //  api call
                                    wsCaseNoteDoneClicked(userNotesModel);
                                  }
                                })).then((value) {
                              setState(() {});
                            });
                          } catch (e) {}*/
                        },
                        child: Container(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Column(
                              children: [
                                Row(
                                  //crossAxisAlignment: CrossAxisAlignment.start,
                                  //mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                          //width: getWP(context, 25),
                                          //color: Colors.yellow,
                                          child: Txt(
                                        txt: userNotesModel.title,
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                        isOverflow: true,
                                      )),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Txt(
                                          txt: userNotesModel.id.toString(),
                                          txtColor: MyTheme.brownColor,
                                          txtSize: MyTheme.txtSize - 0.4,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                    //SizedBox(width: 10),
                                    Expanded(
                                      flex: 3,
                                      child: Txt(
                                          txt: now,
                                          txtColor: MyTheme.brownColor,
                                          txtSize: MyTheme.txtSize - 0.4,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                    Flexible(
                                      child: Icon(
                                        Icons.arrow_forward,
                                        color: Colors.black54,
                                      ),
                                      /*onPressed: () async {
                                        Get.to(
                                          () => TaskBiddingScreen(
                                                                  title: caseModel.title,
                                                                  taskId: caseModel.id),
                                        ).then((value) {
                                          //callback(route);
                                        });
                                         
                                        },*/
                                      //),
                                    ),
                                    //SizedBox(width: 5),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 10, top: 20),
                                  child:
                                      Container(color: Colors.grey, height: .5),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              //SizedBox(height: 20),
            ],
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
      return Container();
    }
  }

  drawActionReqView() {
    //print('absolute coordinates on screen: ${containerKey.globalPaintBounds}');
    try {
      //RenderBox _box = posKey.currentContext.findRenderObject();
      //Offset position = _box.localToGlobal(Offset.zero);

      return Container(
        //color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 40, left: 30),
              child: Txt(
                  txt: "Action required",
                  txtColor: MyTheme.brownColor,
                  txtSize: MyTheme.txtSize + 0.3,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
                child: Container(
                  color: Colors.transparent,
                  height: getHP(context, 20),
                  child: ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: listUserNotesModel.length,
                    itemBuilder: (context, index) {
                      final UserNotesModel userNotesModel =
                          listUserNotesModel[index];
                      return Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/icons/info_icon.png",
                              width: 35,
                              height: 35,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Txt(
                                  txt: userNotesModel.title,
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      );
    } catch (e) {
      log(e.toString());
      return Container();
    }
  }
}
