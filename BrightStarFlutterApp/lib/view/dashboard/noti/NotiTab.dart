import 'package:Bright_Star/config/AppConfig.dart';
import 'package:Bright_Star/config/MyDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/helper/tab_noti/NotiHelper.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiAPIModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiModel.dart';
import 'package:Bright_Star/view/dashboard/timeline/TimeLineTab.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/BoldTxt.dart';
import 'package:Bright_Star/view/mywidgets/MyNetworkImage.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/drawer/AppDrawer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:get/get.dart';

class NotiTab extends StatefulWidget {
  @override
  State createState() => _NotiTabState();
}

class _NotiTabState extends State<NotiTab> with Mixin, StateListener {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  StateProvider _stateProvider;

  UserModel userModel;
  List<NotiModel> listNotiModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int totalMsg = 4;
  int totalNoti = 2;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_dashboard_reload) {
      Get.back();
    }
  }

  wsOnPageLoad() async {
    setState(() {
      isLoading = true;
    });
    try {
      final url = NotiHelper().getUrl(
          pageStart: pageStart, pageCount: pageCount, userModel: userModel);
      log(url);

      log(url);
      await NetworkMgr()
          .getData<NotiAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> notifications =
                    model.responseData.notifications;

                if (notifications != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (notifications.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (NotiModel noti in notifications) {
                      listNotiModel.add(noti);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listNotiModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showToast(msg: "Notifications not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listNotiModel = null;
    userModel = null;
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        key: _drawerKey,
        endDrawer: AppDrawer(
          userModel: userModel,
          totalMsg: totalMsg,
          totalNoti: totalNoti,
          isRightClose: false,
          isNewCaseTab: false,
          isMyCasesTab: false,
          isTimelineTab: false,
          isNotiTab: true,
        ),
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: "Notifications",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    try {
      return Container(
        child: (listNotiModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      wsOnPageLoad();
                    }
                  }
                  return true;
                },
                child: Theme(
                  data: MyTheme.refreshIndicatorTheme,
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      //primary: false,
                      itemCount: listNotiModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        return drawNotiItem(index);
                      },
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: getWP(context, 40),
                            height: getHP(context, 20),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(height: 20),
                          Txt(
                            txt:
                                "Looks like you haven't created any notification?",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.center,
                            isBold: false,
                            txtLineSpace: 1.2,
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                            onTap: () {
                              wsOnPageLoad();
                            },
                            child: Txt(
                                txt: "Refresh",
                                txtColor: MyTheme.themeData.accentColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];
      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap =
          NotiHelper().getNotiMap(model: notiModel, userModel: userModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      return GestureDetector(
        onTap: () async {
          if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                userModel: userModel,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () async {
                  //  go to timeline as per condition
                  Get.to(
                    () => TimeLineTab(),
                  ).then((value) {
                    //callback(route);
                  });
                });
          }
        },
        child: Container(
          //height: getHP(context, 25),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            child: ListTile(
              leading: CircleAvatar(
                radius: 30.0,
                backgroundColor: Colors.transparent,
                backgroundImage: new CachedNetworkImageProvider(
                  MyNetworkImage.checkUrl((notiModel != null)
                      ? notiModel.initiatorImageUrl
                      : MyDefine.MISSING_IMG),
                ),
              ),
              minLeadingWidth: 0,
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  BoldTxt(text1: txt ?? '', text2: eventName),
                  SizedBox(height: 10),
                  Txt(
                    txt: notiMap['publishDateTime'].toString(),
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                    isOverflow: true,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
