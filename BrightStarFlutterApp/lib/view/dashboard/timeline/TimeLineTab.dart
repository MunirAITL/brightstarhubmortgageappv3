import 'package:Bright_Star/config/AppConfig.dart';
import 'package:Bright_Star/config/MyDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/dashboard/NewCaseCfg.dart';
import 'package:Bright_Star/controller/helper/tab_newcase/NewCaseHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:Bright_Star/model/json/tab_newcase/LocationsModel.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/dashboard/timeline/chat/TaskBiddingScreen.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/MyNetworkImage.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:badges/badges.dart';
import 'package:get/get.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<TimeLineTab> with Mixin, StateListener {
  StateProvider _stateProvider;

  UserModel userModel;
  List<LocationsModel> listTaskInfoSearchModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int caseStatus = NewCaseCfg.ALL;

  int totalMsg = 4;
  int totalNoti = 2;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_dashboard_reload) {
      Get.back();
    }
  }

  wsOnPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });

      final url = NewCaseHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
        userModel: userModel,
        status: caseStatus.toString(),
      );
      log(url);
      await NetworkMgr()
          .getData<TaskInfoSearchAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> locations = model.responseData.locations;
                if (locations != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (locations.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (LocationsModel location in locations) {
                      listTaskInfoSearchModel.add(location);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTaskInfoSearchModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  isLoading = false;
                  showToast(msg: "Cases not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        } else {
          log("not in");
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskInfoSearchModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTaskInfoSearchModel = null;
    userModel = null;
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: "Private Messages",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  Widget drawLayout() {
    return Container(
      child: (listTaskInfoSearchModel.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    wsOnPageLoad();
                  }
                }
                return true;
              },
              child: Theme(
                data: MyTheme.refreshIndicatorTheme,
                child: RefreshIndicator(
                  onRefresh: _getRefreshData,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: listTaskInfoSearchModel.length,
                      itemBuilder: (context, index) {
                        return drawRecentCaseItem(index);
                      },
                    ),
                  ),
                ),
              ),
            )
          : (!isLoading)
              ? Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: getWP(context, 40),
                          height: getHP(context, 20),
                          child: Image.asset(
                            'assets/images/screens/home/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(height: 20),
                        Txt(
                          txt: "Looks like you haven't created any timeline?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: false,
                          txtLineSpace: 1.2,
                        ),
                        SizedBox(height: 20),
                        GestureDetector(
                          onTap: () {
                            wsOnPageLoad();
                          },
                          child: Txt(
                              txt: "Refresh",
                              txtColor: MyTheme.themeData.accentColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawRecentCaseItem(index) {
    final topBadgePos = getHP(context, 2);
    final endBadgePos = getHP(context, .05);
    try {
      LocationsModel locationsModel = listTaskInfoSearchModel[index];
      if (locationsModel == null) return SizedBox();
      //final icon =
      //NewCaseHelper().getCreateCaseIconByTitle(userNotesModel.title);
      //if (icon == null) return SizedBox();

      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => TaskBiddingScreen(
                  title: locationsModel.title,
                  taskId: 74119 /*locationsModel.id*/),
            ).then((value) {
              //callback(route);
            });
          } catch (e) {
            log(e.toString());
          }
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Container(
            //height: getHP(context, 20),
            //color: Colors.blue,
            child: Card(
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: getWP(context, 25),
                    //color: Colors.yellow,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 10),
                        /*CircleAvatar(
                          backgroundImage: AssetImage(icon),
                          radius: 30,
                          backgroundColor: Colors.transparent,
                        ),
                        SizedBox(height: 2),*/
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Txt(
                              txt: locationsModel.title,
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - 0.4,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      //color: Colors.yellow,
                      child: (Txt(
                          txt: locationsModel
                              .ownerName, //userNotesModel.adviserName,
                          txtColor: MyTheme.brownColor,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: true)),
                    ),
                  ),
                  //Spacer(),
                  /*Flexible(
                    child: CircleAvatar(
                      radius: 30.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: new CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl((userNotesModel != null)
                            ? userNotesModel.profileImageUrl
                            : MyDefine.MISSING_IMG),
                      ),
                    ),
                  ),*/
                  //SizedBox(width: 10),
                  Flexible(
                    child: CircleAvatar(
                      radius: 20.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: new CachedNetworkImageProvider(
                          MyNetworkImage.checkUrl((locationsModel != null)
                              ? locationsModel.ownerImageURL
                              : MyDefine.MISSING_IMG)),
                    ),
                  ),
                  Flexible(
                    child: Badge(
                      showBadge:
                          (locationsModel.notificationUnreadTaskCount > 0)
                              ? true
                              : false,
                      badgeContent: Txt(
                          txt: locationsModel.notificationUnreadTaskCount
                              .toString(),
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: false),
                      position: BadgePosition.topEnd(
                          top: -topBadgePos, end: -endBadgePos),
                      child: Container(
                        width: getWP(context, 6),
                        height: getWP(context, 6),
                        child: Image.asset(
                          "assets/images/icons/case_msg_arrow_icon.png",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                ],
              ),
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
