import 'dart:async';
import 'package:Bright_Star/config/AppConfig.dart';
import 'package:Bright_Star/config/PubNubCfg.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/classes/Common.dart';
import 'package:Bright_Star/controller/helper/tab_timeline/TimeLineMessagesHelper.dart';
import 'package:Bright_Star/controller/helper/tab_timeline/TimeLinePostHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/tab_timeline/TimeLineAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TimeLinePostModel.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/MyNetworkImage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'utils/ChatBubble.dart';
// Import all PubNub objects into your namespace
import 'package:pubnub/core.dart';
// Or import PubNub into a named namespace
import 'package:pubnub/pubnub.dart' as pn;
import 'package:jiffy/jiffy.dart';

class TimeLinePostScreen extends StatefulWidget {
  final String title;
  final int taskId;
  final int receiverId;

  const TimeLinePostScreen({
    Key key,
    @required this.title,
    @required this.taskId,
    @required this.receiverId,
  }) : super(key: key);
  @override
  State createState() => _TimeLinePostScreenState();
}

class _TimeLinePostScreenState extends State<TimeLinePostScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController textController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  UserModel userModel;
  List<TimeLinePostModel> listTimeLineModel = [];

  Timer timerGetTimeLine;
  Timer timerScrollToBottom;
  static const int callTimelineSec = 35;
  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;

  String msg_tmp = '';

  wsPostTimelineAPI() async {
    try {
      isLoading = true;
      final param = TimeLinePostHelper().getParam(
        message: msg_tmp,
        postTypeName: "status",
        additionalAttributeValue: null,
        inlineTags: [],
        checkin: "",
        fromLat: 0,
        fromLng: 0,
        smallImageName: "",
        receiverId: widget.receiverId,
        ownerId: 0,
        senderId: userModel.id,
        taskId: widget.taskId,
        isPrivate: true,
      );
      log(param.toString());

      await NetworkMgr()
          .postData<TimeLinePostAPIModel, Null>(
        context: context,
        url: Server.TIMELINE_POST_URL,
        isLoading: false,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              await wsOnPageLoad(1);
            } else {}
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
      isLoading = false;
    }
  }

  wsOnPageLoad(int startPage) async {
    try {
      setState(() {
        isLoading = true;
      });
      var url = TimeLineMessagesHelper().getUrl(
        pageStart: startPage,
        pageCount: pageCount,
        taskId: widget.taskId,
        isPrivate: true,
        customerId: 0,
        senderId: userModel.id.toString(),
        receiverId: widget.receiverId,
        timeLineId: 0,
      );
      log(url);
      await NetworkMgr()
          .getData<TimeLineAPIModel, Null>(
        context: context,
        url: url,
        isLoading: false,
      )
          .then((model) async {
        if (model != null && mounted) {
          msg_tmp = '';
          try {
            if (model.success) {
              try {
                final List<dynamic> timeLines =
                    model.responseData.timelinePosts;
                if (timeLines != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility

                  try {
                    setState(() {
                      isLoading = false;
                    });
                    if (listTimeLineModel.length > 0) {
                      try {
                        TimeLinePostModel tlModel1 = listTimeLineModel[0];
                        TimeLinePostModel tlModel2 =
                            model.responseData.timelinePosts[0];
                        if (tlModel1.id == tlModel2.id) {
                          return;
                        }
                      } catch (e) {
                        log(e.toString());
                      }
                    }

                    //await _removeFromMeMsg();
                    if (startPage == 1) {
                      listTimeLineModel.clear();
                      //await _removeFromMeMsg();
                    }
                    for (TimeLinePostModel timeLine in timeLines) {
                      //if (!listTimeLineModel.contains(timeLine))
                      listTimeLineModel.add(timeLine);
                    }
                    setState(() {
                      if (timeLines.length != pageCount) {
                        isPageDone = true;
                      }
                    });
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTimeLineModel.toString());
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      //initPubNub();
    } catch (e) {}
  }

  /*_removeFromMeMsg() async {
    try {
      for (TimeLinePostModel tlModel in listTimeLineModel) {
        try {
          if (tlModel.isFromMe) {
            listTimeLineModel.remove(tlModel);
          }
        } catch (e) {}
      }
    } catch (e) {}
  }*/

  //  ******************* Pub Nub Start Here....

  initPubNub() async {
    userModel = await DBMgr.shared.getUserProfile();
    final pubnub = pn.PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(await Common.getUDID(context))));

    final param = TimeLinePostHelper().getParam(
      message: "testinggggg pubnub",
      postTypeName: "status",
      additionalAttributeValue: null,
      inlineTags: [],
      checkin: "",
      fromLat: 0,
      fromLng: 0,
      smallImageName: "",
      receiverId: widget.receiverId,
      ownerId: 0,
      senderId: userModel.id,
      taskId: widget.taskId,
      isPrivate: true,
    );

    final myChannel = pubnub.channel(userModel.id.toString());
    myChannel.publish({
      'data': param,
      'user': userModel.id,
    });

    var subscription = pubnub.subscribe(channels: {userModel.id.toString()});

    subscription.messages.listen((envelope) {
      print('${envelope.uuid} sent a message: ${envelope.payload}');
    });

    var history = myChannel.history(chunkSize: 50);
    log(history.toString());

    appInit();
    /*myChannel.subscribe().messages.listen((envelope) {
      print(envelope.payload);
    });*/
  }

  //  ******************* Pub Nub End Here....
  @override
  void dispose() {
    listTimeLineModel = null;
    userModel = null;
    textController.dispose();
    _scrollController.dispose();
    _scrollController = null;
    try {
      timerScrollToBottom = null;
      timerGetTimeLine?.cancel();
      timerGetTimeLine = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
      await wsOnPageLoad(1);
      pageStart++;
      timerGetTimeLine =
          Timer.periodic(Duration(seconds: callTimelineSec), (Timer t) {
        //  call to ws for getting any new msg of the sender
        if (!isLoading && msg_tmp.length == 0) {
          wsOnPageLoad(1);
        }
      });
    } catch (e) {}
  }

  Widget buildSingleMessage(index) {
    try {
      TimeLinePostModel timelineModel = listTimeLineModel[index];
      bool fromMe = (timelineModel.senderId == userModel.id) ? true : false;

      String lastMsgDate;
      DateTime date1;
      try {
        //if (index  <= listTimeLineModel.length) {
        TimeLinePostModel timelineModel2 = listTimeLineModel[(index + 1)];
        //var jiffy1 = Jiffy(timelineModel2.dateCreated);
        //var jiffy2 = Jiffy(timelineModel2.dateCreated)..date;

        date1 = Jiffy(timelineModel.dateCreated).dateTime;
        DateTime date2 = Jiffy(timelineModel2.dateCreated).dateTime;
        //final days = (jiffy2.diff(jiffy1.date));
        if (date1.day > date2.day) {
          // log(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
        }
        //}
      } catch (e) {
        log(e.toString());
      }

      Alignment alignment = fromMe ? Alignment.topRight : Alignment.topLeft;
      Alignment chatArrowAlignment =
          fromMe ? Alignment.topRight : Alignment.topLeft;

      Color chatBgColor = fromMe ? MyTheme.brownColor : MyTheme.lPurpleColor;
      EdgeInsets edgeInsets = fromMe
          ? EdgeInsets.fromLTRB(5, 5, 15, 5)
          : EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = fromMe
          ? EdgeInsets.fromLTRB(80, 5, 10, 5)
          : EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //(!isFromMeSent && fromMe && timelineModel.fromMeDate != null)

          (fromMe && lastMsgDate != null)
              ? Padding(
                  padding: const EdgeInsets.all(20),
                  child: Txt(
                      txt: lastMsgDate,
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .6,
                      txtAlign: TextAlign.start,
                      isBold: false),
                )
              : SizedBox(),
          Row(
            children: [
              (!fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
              Expanded(
                child: Container(
                  //color: Colors.white,
                  margin: margins,
                  child: Align(
                    alignment: alignment,
                    child: Column(
                      children: <Widget>[
                        CustomPaint(
                          painter: ChatBubble(
                            color: chatBgColor,
                            alignment: chatArrowAlignment,
                          ),
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: edgeInsets,
                                  child: Txt(
                                      txt: timelineModel.message
                                          .toString()
                                          .trim(),
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        Txt(
                            txt: getTimeAgoTxt(timelineModel.dateCreated),
                            txtColor: Colors.grey,
                            txtSize: MyTheme.txtSize - .8,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
              ),
              (fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
            ],
          ),
        ],
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return CircleAvatar(
      radius: 30.0,
      backgroundColor: Colors.transparent,
      backgroundImage:
          new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
    );
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            if (!isLoading && !isPageDone && msg_tmp.length == 0) {
              wsOnPageLoad(pageStart++);
            }
          }
          return true;
        },
        child: ListView.builder(
          controller: _scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            try {
              return buildSingleMessage(index);
            } catch (e) {
              return Container();
            }
          },
        ),
      ),
    );
  }

  _bottomChatArea() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          _chatTextArea(),
          IconButton(
            icon: Icon(
              Icons.send,
              color: MyTheme.brownColor,
            ),
            onPressed: () async {
              //Check if the textfield has text or not
              onSendClicked();
            },
          ),
        ],
      ),
    );
  }

  onSendClicked() async {
    try {
      if (textController.text.isNotEmpty) {
        //Add the message to the list
        String formattedDate = DateFormat('dd-MMM-yyyy').format(DateTime.now());
        if (mounted) {
          //Scrolldown the list to show the latest message
          msg_tmp = textController.text.trim();
          final TimeLinePostModel timeLinePostModel = TimeLinePostModel();
          timeLinePostModel.senderId = userModel.id;
          timeLinePostModel.ownerId = userModel.id;
          timeLinePostModel.ownerImageUrl = userModel.profileImageURL;
          timeLinePostModel.message = msg_tmp;
          timeLinePostModel.dateCreatedUtc = formattedDate;
          timeLinePostModel.isFromMe = true;
          listTimeLineModel.insert(0, timeLinePostModel);
          textController.text = '';
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
          //_chatListScrollToBottom();
          //setState(() {});
          //Future.delayed(Duration(seconds: 5), () async {
          pageStart = 1;
          await wsPostTimelineAPI();
          //});
        }
      }
    } catch (e) {}
  }

  _chatTextArea() {
    return Expanded(
      child: TextField(
        controller: textController,
        textInputAction: TextInputAction.send,
        onSubmitted: (value) {
          onSendClicked();
        },
        maxLength: 255,
        autocorrect: false,
        style: TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context),
        ),
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey),
          counter: Offstage(),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 5,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(
              color: Colors.black,
              width: 0.0,
            ),
          ),
          filled: true,
          hintText: 'Type a message...',
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          title: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Txt(
                txt: "Chat",
                txtColor: Colors.black,
                txtSize: MyTheme.appbarTitleFontSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          centerTitle: false,
          leading: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () async {
                Get.back();
              },
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          buildMessageList(),
          //Divider(height: 1.0),
          _bottomChatArea(),
        ],
      ),
    );
  }
}
