import 'package:Bright_Star/config/AppConfig.dart';
import 'package:Bright_Star/config/MyDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/helper/tab_timeline/TaskBiddingHelper.dart';
import 'package:Bright_Star/model/json/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:Bright_Star/model/json/tab_timeline/TaskBiddingModel.dart';
import 'package:Bright_Star/view/dashboard/timeline/chat/TimeLinePostScreen.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/MyNetworkImage.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:badges/badges.dart';
import 'package:get/get.dart';

class TaskBiddingScreen extends StatefulWidget {
  final String title;
  final int taskId;

  const TaskBiddingScreen({
    Key key,
    @required this.title,
    @required this.taskId,
  }) : super(key: key);
  @override
  State createState() => _TaskBiddingScreenState();
}

class _TaskBiddingScreenState extends State<TaskBiddingScreen> with Mixin {
  List<TaskBiddingModel> listTaskBiddingModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  wsOnPageLoad() async {
    try {
      var url = TaskBiddingHelper().getUrl(taskId: widget.taskId.toString());
      log(url);
      await NetworkMgr()
          .getData<TaskBiddingAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> taskBiddings =
                    model.responseData.taskBiddings;

                if (taskBiddings != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (taskBiddings.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (TaskBiddingModel taskBidding in taskBiddings) {
                      listTaskBiddingModel.add(taskBidding);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTaskBiddingModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showToast(msg: "Tiskbidding not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskBiddingModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTaskBiddingModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: widget.title,
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return (listTaskBiddingModel.length > 0)
        ?  Theme(
            data: MyTheme.refreshIndicatorTheme,
            child: RefreshIndicator(
              onRefresh: _getRefreshData,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: ListView.builder(
                  itemCount: listTaskBiddingModel.length,
                  itemBuilder: (context, index) {
                    return drawMessageList(index);
                  },
                ),
              ),
            ),
        )
        : (!isLoading)
            ? Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: getWP(context, 40),
                        height: getHP(context, 20),
                        child: Image.asset(
                          'assets/images/screens/home/my_cases/case_nf.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      SizedBox(height: 20),
                      Txt(
                        txt: "Looks like you haven't got any taskbidding?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.center,
                        isBold: false,
                        txtLineSpace: 1.2,
                      ),
                      SizedBox(height: 20),
                      GestureDetector(
                        onTap: () {
                          wsOnPageLoad();
                        },
                        child: Txt(
                            txt: "Refresh",
                            txtColor: MyTheme.themeData.accentColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
              )
            : SizedBox();
  }

  drawMessageList(int index) {
    try {
      final TaskBiddingModel taskBiddingModel = listTaskBiddingModel[index];
      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => TimeLinePostScreen(
                title: widget.title,
                taskId: widget.taskId,
                receiverId: taskBiddingModel.userId,
              ),
            ).then((value) {
              //callback(route);
            });
          } catch (e) {}
        },
        child: Card(
          color: Colors.white,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 20),
            child: Row(
              //contentPadding:
              //EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              //dense: true,
              //mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  child: CircleAvatar(
                    radius: 30.0,
                    backgroundColor: Colors.transparent,
                    backgroundImage: new CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl((taskBiddingModel != null)
                            ? taskBiddingModel.ownerImageUrl
                            : MyDefine.MISSING_IMG)),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  flex: 5,
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: taskBiddingModel.ownerName,
                            txtColor: MyTheme.brownColor,
                            txtSize: MyTheme.txtSize - 0.4,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 10),
                        Txt(
                            txt: taskBiddingModel.communityName,
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - 0.6,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  child: Badge(
                    showBadge:
                        (taskBiddingModel.totalComments > 0) ? true : false,
                    badgeContent: Txt(
                        txt: taskBiddingModel.totalComments.toString(),
                        txtColor: null,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.center,
                        isBold: false),

                    //position: BadgePosition.topEnd(
                    //top: -topBadgePos, end: -endBadgePos),
                    child: Icon(
                      Icons.message,
                      color: MyTheme.brownColor,
                      size: 30,
                    ),
                  ),
                ),
                //SizedBox(width: 10),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
