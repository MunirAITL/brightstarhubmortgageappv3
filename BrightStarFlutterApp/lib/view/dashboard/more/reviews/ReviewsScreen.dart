import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class ReviewsScreen extends StatefulWidget {
  @override
  State createState() => _ReviewsScreenState();
}

class _ReviewsScreenState extends State<ReviewsScreen> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Reviews",
              txtColor: Colors.white,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 50),
                  height: getHP(context, 30),
                  child: Image.asset(
                    'assets/images/screens/home/my_cases/case_nf.png',
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(height: 20),
                Txt(
                  txt: "There are no reviews to show!",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  txtLineSpace: 1.2,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
