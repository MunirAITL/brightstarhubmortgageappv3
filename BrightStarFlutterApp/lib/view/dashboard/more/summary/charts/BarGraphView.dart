// Copyright 2018 the Charts project authors. Please see the AUTHORS file
// for details.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Bar chart example
// EXCLUDE_FROM_GALLERY_DOCS_START
// EXCLUDE_FROM_GALLERY_DOCS_END
import 'package:Bright_Star/view/dashboard/more/summary/charts/utils/CustomLegendBuilder.dart';
import 'package:Bright_Star/view/dashboard/more/summary/charts/utils/IconRenderer.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';

class BarGraphView extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  BarGraphView(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory BarGraphView.withSampleData() {
    return new BarGraphView(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(seriesList,
        animate: animate,
        //defaultRenderer: ,
        /* domainAxis: new charts.OrdinalAxisSpec(
            renderSpec: new charts.SmallTickRendererSpec(
                labelStyle: new charts.TextStyleSpec(
                  fontSize: 16,
                  color: charts.ColorUtil.fromDartColor(mat.Colors.black),
                  fontFamily: 'Open_Sans',
                  fontWeight: 'w700',
                  lineHeight: 2,
                ),
                lineStyle: new charts.LineStyleSpec(
                  color: charts.ColorUtil.fromDartColor(mat.Colors.black),
                  // thickness: 2,
                ))),
        primaryMeasureAxis: new charts.NumericAxisSpec(
            renderSpec: new charts.GridlineRendererSpec(
                labelStyle: new charts.TextStyleSpec(
                  fontSize: 16,
                  color: charts.ColorUtil.fromDartColor(mat.Colors.black),
                  fontFamily: 'Open_Sans',
                  // fontWeight: Material,
                  // lineHeight: 2,
                ),
                lineStyle: new charts.LineStyleSpec(
                  // color: charts.MaterialPalette.black
                  color: charts.ColorUtil.fromDartColor(mat.Colors.black),
                ))),
        // domainAxis: new charts.OrdinalAxisSpec(renderSpec: new charts.NoneRenderSpec()),
        // barRendererDecorator: new charts.BarLabelDecorator<String>(),
        defaultRenderer: new charts.BarRendererConfig(
          groupingType: charts.BarGroupingType.stacked,
          // weightPattern: [1, 3, 1],
          //stackHorizontalSeparator: 50,
          // cornerStrategy: const charts.ConstCornerStrategy(30)
          barRendererDecorator: new charts.BarLabelDecorator(
            // labelAnchor: charts.BarLabelAnchor.middle,
            labelPosition: charts.BarLabelPosition.outside,
            insideLabelStyleSpec: new charts.TextStyleSpec(
              fontSize: 14,
              color: charts.ColorUtil.fromDartColor(Colors.white),
              // lineHeight: 0.14,
              fontWeight: '700',
            ),
            outsideLabelStyleSpec: new charts.TextStyleSpec(
              fontSize: 14,
              color: charts.ColorUtil.fromDartColor(mat.Colors.black),
              lineHeight: 0.14,
              // fontWeight: '700',
            ),
          ),
        ),
        barGroupingType: charts.BarGroupingType.stacked,
        customSeriesRenderers: [
          new charts.BarTargetLineRendererConfig<String>(
            // ID used to link series to this renderer.
            customRendererId: 'customTargetLine',
          ),
        ],*/

        defaultRenderer: new charts.BarRendererConfig(
          symbolRenderer: new IconRenderer(Icons.circle),
        ),
        behaviors: [
          charts.SeriesLegend(
            position: charts.BehaviorPosition.top,
            entryTextStyle: charts.TextStyleSpec(
                color: charts.ColorUtil.fromDartColor(Colors.black)),
            horizontalFirst: false,
            desiredMaxRows: 2,
            cellPadding: EdgeInsets.only(right: 4.0, bottom: 4.0),
          ),
          //charts.SelectNearest(
          //eventTrigger: charts.SelectionTrigger.tapAndDrag),
          charts.LinePointHighlighter(
            symbolRenderer: CustomCircleSymbolRenderer(),
            drawFollowLinesAcrossChart: true,
            defaultRadiusPx: 6,
            //selectionModelType: charts.SelectionModelType.action,
            //showVerticalFollowLine:
            //charts.LinePointHighlighterFollowLineType.,
            selectionModelType: charts.SelectionModelType.info,
            //showHorizontalFollowLine:
            //charts.LinePointHighlighterFollowLineType.all,
          ),
        ],
        selectionModels: [
          charts.SelectionModelConfig(
              type: charts.SelectionModelType.info,
              changedListener: (SelectionModel model) {
                if (model.hasDatumSelection) {
                  final value2 = model.selectedSeries[0]
                      .measureFn(model.selectedDatum[0].index);
                  final value3 = model.selectedSeries[0]
                      .domainFn(model.selectedDatum[0].index);
                  CustomCircleSymbolRenderer.value1 = "August 2020";
                  CustomCircleSymbolRenderer.value2 =
                      "\$" + value2.toString() + "k";
                  CustomCircleSymbolRenderer.value3 =
                      "\$" + value3.toString() + "k"; // paints the tapped value
                }
              })
        ]);
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<Sales, String>> _createSampleData() {
    final data = [
      new Sales('2014', 5),
      new Sales('2015', 25),
      new Sales('2016', 40),
      new Sales('2017', 20),
    ];

    return [
      new charts.Series<Sales, String>(
        id: 'Completed',
        //areaColorFn: mat.Colors.black,
        //areaColorFn: charts.Color.black,
        colorFn: (_, __) => charts.Color.fromHex(code: "#938BEA"),
        domainFn: (Sales sales, _) => sales.year,
        measureFn: (Sales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample ordinal data type.
class Sales {
  final String year;
  final int sales;
  Sales(this.year, this.sales);
}
