import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/config/MyDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/more/summary/SummaryHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/view/dashboard/more/summary/charts/BarGraphView.dart';
import 'package:Bright_Star/view/mywidgets/SliderNav.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SummaryScreen extends StatefulWidget {
  @override
  State createState() => _SummaryScreenState();
}

class _SummaryScreenState extends State<SummaryScreen> with Mixin {
  final PageController _pagePaymentController =
      PageController(initialPage: 0, keepPage: false, viewportFraction: 0.9);

  final PageController _pageCaseController =
      PageController(initialPage: 0, keepPage: false, viewportFraction: 0.9);

  final PageController _pageLeadController =
      PageController(initialPage: 0, keepPage: false, viewportFraction: 0.9);

  int indexPayment = 0;
  int indexCase = 0;
  int indexLead = 0;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _pagePaymentController.dispose();
      _pageCaseController.dispose();
      _pageLeadController.dispose();
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.lPurpleColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  backgroundColor: MyTheme.lPurpleColor.withOpacity(0.5),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: "Summary",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          drawPaymentSummaryUI(),
          drawCaseSummaryUI(),
          drawLeadSummaryUI(),
          SizedBox(height: getHP(context, 5)),
        ],
      ),
    );
  }

  drawPaymentSummaryUI() {
    final len = SummaryHelper.listPayment.length;

    return Container(
      width: double.infinity,
      height: getHP(context, 30),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 0),
            child: SliderNav(
                title: "Payment Summary",
                len: len,
                index: indexPayment,
                callback: (isNext) {
                  if (isNext) {
                    indexPayment--;
                    if (indexPayment < 0) {
                      indexPayment = 0;
                      return;
                    }
                    _pagePaymentController.animateToPage(
                      indexPayment,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn,
                    );
                  } else {
                    indexPayment++;
                    if (indexPayment == SummaryHelper.listPayment.length) {
                      indexPayment--;
                      return;
                    }
                    setState(() {
                      _pagePaymentController.animateToPage(
                        indexPayment,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn,
                      );
                    });
                  }
                }),
          ),
          Expanded(
            child: PageView.builder(
              itemCount: len,
              controller: _pagePaymentController,
              onPageChanged: (int index) => setState(() {
                indexPayment = index;
              }),
              itemBuilder: (_, i) {
                final map = SummaryHelper.listPayment[i];
                return Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: map['title'],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          Spacer(),
                          Expanded(
                            child: Txt(
                                txt:
                                    MyDefine.CUR_SIGN + map['total'].toString(),
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  drawCaseSummaryUI() {
    final len = SummaryHelper.listCase.length;

    return Container(
      width: double.infinity,
      height: getHP(context, 70),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 0),
            child: SliderNav(
                title: "Case Summary",
                len: len,
                index: indexCase,
                callback: (isNext) {
                  if (isNext) {
                    indexCase--;
                    if (indexCase < 0) {
                      indexCase = 0;
                      return;
                    }
                    _pageCaseController.animateToPage(
                      indexCase,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn,
                    );
                  } else {
                    indexCase++;
                    if (indexCase == SummaryHelper.listCase.length) {
                      indexCase--;
                      return;
                    }
                    setState(() {
                      _pageCaseController.animateToPage(
                        indexCase,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn,
                      );
                    });
                  }
                }),
          ),
          Expanded(
            child: PageView.builder(
              physics: new NeverScrollableScrollPhysics(),
              itemCount: len,
              controller: _pageCaseController,
              onPageChanged: (int index) => setState(() {
                indexCase = index;
              }),
              itemBuilder: (_, i) {
                final map = SummaryHelper.listCase[i];
                return Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Txt(
                              txt: map['title'],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 40),
                          Expanded(
                            child:
                                Container(child: BarGraphView.withSampleData()),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  drawLeadSummaryUI() {
    final len = SummaryHelper.listLead.length;
    return Container(
      width: double.infinity,
      height: getHP(context, 70),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 0),
            child: SliderNav(
                title: "Lead Summary",
                len: len,
                index: indexLead,
                callback: (isNext) {
                  if (isNext) {
                    indexLead--;
                    if (indexLead < 0) {
                      indexLead = 0;
                      return;
                    }
                    _pageLeadController.animateToPage(
                      indexLead,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn,
                    );
                  } else {
                    indexLead++;
                    if (indexLead == SummaryHelper.listLead.length) {
                      indexLead--;
                      return;
                    }
                    setState(() {
                      _pageLeadController.animateToPage(
                        indexLead,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn,
                      );
                    });
                  }
                }),
          ),
          Expanded(
            child: PageView.builder(
              physics: new NeverScrollableScrollPhysics(),
              itemCount: len,
              controller: _pageLeadController,
              onPageChanged: (int index) => setState(() {
                indexLead = index;
              }),
              itemBuilder: (_, i) {
                final map = SummaryHelper.listLead[i];
                return Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: map['title'],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
