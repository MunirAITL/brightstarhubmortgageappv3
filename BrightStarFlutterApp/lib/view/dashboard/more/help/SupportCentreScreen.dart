import 'package:Bright_Star/config/AppDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/dashboard/more/help/ResolutionScreen.dart';
import 'package:Bright_Star/view/mywidgets/Btn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: "Support",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () async {
                Get.back();
              }),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.brownColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {},
        ),
        body: drawSupportButtons(context),
      ),
    );
  }

  drawSupportButtons(context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Email: " + AppDefine.SUPPORT_EMAIL,
              txtColor: Colors.white,
              bgColor: MyTheme.brownColor,
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Call: " + AppDefine.SUPPORT_CALL,
              txtColor: Colors.white,
              bgColor: MyTheme.brownColor,
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Send Message",
              txtColor: Colors.white,
              bgColor: MyTheme.brownColor,
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () async {
                Get.to(
                  () => ResolutionScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
