import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/helper/more/ResolutionHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:Bright_Star/model/json/media_upload/MediaUploadFilesModel.dart';
import 'package:Bright_Star/model/json/tab_more/support/ResolutionAPIModel.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:file_picker/file_picker.dart';
import 'package:dotted_border/dotted_border.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends State<ResolutionScreen> with Mixin {
  final TextEditingController _desc = TextEditingController();

  static const int MAX_FILE_UPLOAD = 5;

  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];

  UserModel userModel;

  ResolutionHelper resolutionHelper;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _desc.dispose();
    userModel = null;
    resolutionHelper = null;
    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  wsMediaUploadFileAPI(File file) async {
    try {
      if (listMediaUploadFilesModel.length > MAX_FILE_UPLOAD) {
        showToast(
            msg: "Maximum file upload limit is " +
                MAX_FILE_UPLOAD.toString() +
                " files");
        return;
      }

      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: Server.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              listMediaUploadFilesModel.add(model.responseData.images[0]);
              setState(() {});
            } else {
              final err = model.errorMessages.upload_pictures[0].toString();
              showToast(msg: err);
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsResolutionAPI() async {
    try {
      if (resolutionHelper.opt.id == null) {
        showToast(msg: "Please choose ticket type from the list");
        return;
      } else if (_desc.text.trim().length == 0) {
        showToast(msg: "Please enter description");
        return;
      }
      List<String> listFileUrl = [];
      for (MediaUploadFilesModel model in listMediaUploadFilesModel) {
        listFileUrl.add(model.url);
      }
      final param = resolutionHelper.getParam(
        userModel: userModel,
        title: resolutionHelper.opt.title,
        desc: _desc.text.trim(),
        fileUrl: listFileUrl.join(','),
      );
      log(json.encode(param));
      await NetworkMgr()
          .postData<ResolutionAPIModel, Null>(
        context: context,
        url: Server.RESOLUTION_URL,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              _desc.text = "";
              listMediaUploadFilesModel.clear();
              final msg = model.messages.resolution_post[0].toString();
              showToast(msg: msg, which: 1);
              setState(() {});
            } else {}
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  browseFiles() async {
    try {
      try {
        FilePickerResult result = await FilePicker.platform.pickFiles(
          allowMultiple: false,
          type: FileType.any,
          //type: FileType.custom,
          /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
        );

        if (result != null) {
          wsMediaUploadFileAPI(File(result.files.single.path));
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {
      log(e.toString());
    }
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
      resolutionHelper = ResolutionHelper();
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 160,
                  elevation: 0,
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  title: Txt(
                      txt: "Contact us",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return (resolutionHelper == null)
        ? SizedBox()
        : Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 30, right: 30, bottom: 30),
            child: Container(
              color: Colors.white,
              child: ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: DropDownPicker(
                      cap: "Support Ticket Type",
                      itemSelected: resolutionHelper.opt,
                      dropListModel: resolutionHelper.dd,
                      onOptionSelected: (optionItem) {
                        resolutionHelper.opt = optionItem;
                        setState(() {});
                      },
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Txt(
                        txt: "How can we help you?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.only(left: 20.0, right: 20, top: 10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: TextField(
                      controller: _desc,
                      minLines: 5,
                      maxLines: 10,
                      //expands: true,
                      autocorrect: false,
                      maxLength: 500,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getTxtSize(context: context),
                      ),
                      decoration: InputDecoration(
                        hintText: 'Description',
                        hintStyle: TextStyle(color: Colors.grey),
                        //labelText: 'Your message',
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 15, top: 15, right: 15),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Txt(
                        txt: "Attachments - " +
                            listMediaUploadFilesModel.length.toString() +
                            ' files added',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  for (MediaUploadFilesModel model in listMediaUploadFilesModel)
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Card(
                        child: ListTile(
                          leading: IconButton(
                              icon: Icon(
                                Icons.remove_circle,
                                color: Colors.red,
                              ),
                              onPressed: () {
                                listMediaUploadFilesModel.remove(model);
                                setState(() {});
                              }),
                          title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Txt(
                                txt: model.name ?? '',
                                txtColor: Colors.black87,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ),
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: DottedBorder(
                      borderType: BorderType.RRect,
                      radius: Radius.circular(10),
                      padding: EdgeInsets.all(6),
                      color: Colors.grey,
                      strokeWidth: 1.5,
                      child: GestureDetector(
                        onTap: () async {
                          await browseFiles();
                        },
                        child: Container(
                          height: getHP(context, 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.attach_file,
                                color: Colors.black54,
                                size: 30,
                              ),
                              Txt(
                                  txt: "Add upto 5 files",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: BSBtn(
                        txt: "Submit",
                        height: getHP(context, 7),
                        callback: () {
                          wsResolutionAPI();
                        }),
                  ),
                ],
              ),
            ),
          );
  }
}
