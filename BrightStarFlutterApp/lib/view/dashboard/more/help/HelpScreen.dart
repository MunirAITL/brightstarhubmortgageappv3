import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/view/dashboard/more/help/SupportCentreScreen.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  List<String> listItems = [
    "Support centre",
    "Terms and conditions",
    "Privacy",
    //"Application tutorial"
  ];

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: "Help",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: listItems.length,
            itemBuilder: (context, index) {
              final item = listItems[index];
              return GestureDetector(
                onTap: () async {
                  switch (index) {
                    case 0: //  Support Center
                      Get.to(
                        () => SupportCentreScreen(),
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    case 1: //  TC
                      Get.to(
                        () => WebScreen(
                          title: "Terms & Conditions",
                          url: Server.TC_URL,
                        ),
                        fullscreenDialog: true,
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    case 2: //  Privacy
                      Get.to(
                        () => WebScreen(
                          title: "Privacy",
                          url: Server.PRIVACY_URL,
                        ),
                        fullscreenDialog: true,
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    case 3: //  Application Tuturial
                      Get.back(result: true);
                      break;
                    default:
                  }
                },
                child: Container(
                  child: ListTile(
                    title: Txt(
                        txt: item,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: Icon(
                      Icons.arrow_right,
                      color: Colors.black,
                      size: 30,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
