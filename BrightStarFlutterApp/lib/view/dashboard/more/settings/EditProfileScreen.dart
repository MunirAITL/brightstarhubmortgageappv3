import 'dart:convert';
import 'dart:io';
import 'package:Bright_Star/config/MyConfig.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/form_validate/UserProfileVal.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/controller/helper/more/settings/EditProfileHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/model/data/PrefMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Btn.dart';
import 'package:Bright_Star/view/mywidgets/CamPicker.dart';
import 'package:Bright_Star/view/mywidgets/DatePickerView.dart';
import 'package:Bright_Star/view/mywidgets/InputBox.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/TxtBox.dart';
import 'package:Bright_Star/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../Mixin.dart';
import 'DeactivateProfileDialog.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State createState() => _EditProfileState();
}

enum SmookerEnum { yes, no }

class _EditProfileState extends State<EditProfileScreen> with Mixin {
  StateProvider _stateProvider = StateProvider();

  File _pathFG;

  EditProfileHelper editProfileHelper;

  //  personal info
  final _pwd = TextEditingController();
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _dob = TextEditingController();
  final _mobile = TextEditingController();
  final _tel = TextEditingController();
  //final _ref = TextEditingController();

  //  visa info
  final _nin = TextEditingController();
  final _passport = TextEditingController();

  //  deactivate reason
  final TextEditingController _deactivateReason = TextEditingController();

  //  Email
  String email;
  int profileImageId;

  //  DOB
  String dob = "";

  //  Visa
  String visaExp = "";

  //  Passport
  String passportExp = "";

  SmookerEnum _radio = SmookerEnum.no;

  wsMediaUploadFileAPI(File file) async {
    try {
      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: Server.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              profileImageId = model.responseData.images[0].id;
              setState(() {});
            } else {
              final err = model.errorMessages.upload_pictures[0].toString();
              showToast(msg: err);
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsUpdateProfileAPI() async {
    try {
      if (validate()) {
        final UserModel userModel = await DBMgr.shared.getUserProfile();
        final param = EditProfileHelper().getParam(
          firstName: _fname.text.trim(),
          lastName: _lname.text.trim(),
          name: userModel.name,
          email: email,
          userName: userModel.userName,
          profileImageId: profileImageId,
          coverImageId: '',
          referenceId: userModel.referenceID.toString(),
          referenceType: "", //_ref.text.trim(),
          remarks: userModel.remarks,
          cohort: userModel.cohort,
          communityId: userModel.communityID.toString(),
          isFirstLogin: false,
          mobileNumber: _mobile.text.trim(),
          dateofBirth: dob,
          middleName: _mname.text.trim(),
          namePrefix: _fname.text.trim(),
          areYouASmoker: (_radio == SmookerEnum.no) ? 'No' : 'Yes',
          countryCode: '',
          addressLine1: userModel.addressLine1,
          addressLine2: userModel.addressLine2,
          addressLine3: userModel.addressLine3,
          town: userModel.town,
          county: userModel.county,
          postcode: userModel.postcode,
          telNumber: _tel.text.trim(),
          nationalInsuranceNumber: _nin.text.trim(),
          nationality: editProfileHelper.optNationalities.title,
          countryofBirth: editProfileHelper.optCountriesBirth.title,
          countryofResidency: editProfileHelper.optCountriesResidential.title,
          passportNumber: _passport.text.trim(),
          maritalStatus: editProfileHelper.optMaritalStatus.title,
          occupantType: userModel.occupantType,
          livingDate: userModel.livingDate,
          password: _pwd.text.trim(),
          userCompanyId: userModel.userCompanyID,
          visaExpiryDate: visaExp,
          passportExpiryDate: passportExp,
          visaName: userModel.visaName,
          otherVisaName: userModel.otherVisaName,
          id: userModel.id,
        );

        log(json.encode(param));
        await NetworkMgr()
            .postData<UserProfileAPIModel, Null>(
          context: context,
          url: Server.EDIT_PROFILE_URL,
          isPost: false,
          param: param,
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final msg = model.messages.post_user[0].toString();
                  showToast(msg: msg, which: 1);
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.messages.post_user[0].toString();
                    showToast(msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  wsDeactivateProfileAPI(String reason) async {
    try {
      final UserModel userModel = await DBMgr.shared.getUserProfile();

      await NetworkMgr().postData<DeactivateProfileAPIModel, Null>(
        context: context,
        url: Server.DEACTIVATE_PROFILE_URL,
        param: {'id': userModel.id, 'CancelReason': reason},
      ).then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final msg = model.messages.delete[0].toString();
                showToast(msg: msg, which: 1);
                Future.delayed(Duration(seconds: MyConfig.AlertDismisSec), () {
                  //  signout
                  _stateProvider.notify(ObserverState.STATE_CHANGED_logout);
                });
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.messages.delete[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _pathFG = null;
    editProfileHelper = null;
    _stateProvider = null;

    //  personal info
    _pwd.dispose();
    _fname.dispose();
    _mname.dispose();
    _lname.dispose();
    _dob.dispose();
    _mobile.dispose();
    _tel.dispose();
    //_ref.dispose();

    //  visa info
    _passport.dispose();
    _nin.dispose();

    //  deactivate
    _deactivateReason.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      final UserModel userModel = await DBMgr.shared.getUserProfile();

      try {
        final fgPath =
            await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicFG_Key);
        if (fgPath != null) {
          _pathFG = File(fgPath);
        }
      } catch (e) {}

      //  personal info
      email = userModel.email;
      profileImageId = userModel.profileImageID;
      _fname.text = userModel.firstName;
      _mname.text = userModel.middleName;
      _lname.text = userModel.lastName;
      _dob.text = userModel.dateofBirth;
      _mobile.text = userModel.mobileNumber;
      _tel.text = userModel.telNumber;
      //_ref.text = userModel.referenceType;

      //  visa info
      _nin.text = userModel.nationalInsuranceNumber;
      _passport.text = userModel.passportNumber;
      passportExp = DateFormat('dd-MM-yyyy')
          .format(DateTime.parse(userModel.passportExpiryDate));
      visaExp = DateFormat('dd-MM-yyyy')
          .format(DateTime.parse(userModel.visaExpiryDate));

      //  populate countries drop down all 3
      editProfileHelper = EditProfileHelper();
      await editProfileHelper.getCountriesBirth(
          context: context, cap: userModel.countryofBirth);
      editProfileHelper.getCountriesResidential(
          context: context, cap: userModel.countryofResidency);
      editProfileHelper.getCountriesNationaity(
          context: context, cap: userModel.nationality);
      //  set data into drop down
      //editProfileHelper.optTitle.title = userModel.
      //editProfileHelper.optGender.title = userModel.
      if (userModel.maritalStatus != '')
        editProfileHelper.optMaritalStatus.title = userModel.maritalStatus;
      dob = userModel.dateofBirth;
      if (userModel.countryofBirth != '')
        editProfileHelper.optCountriesBirth.title = userModel.countryofBirth;
      if (userModel.countryofResidency != '')
        editProfileHelper.optCountriesResidential.title =
            userModel.countryofResidency;
      if (userModel.nationality != '')
        editProfileHelper.optNationalities.title = userModel.nationality;

      Future.delayed(Duration.zero, () {
        setState(() {});
      });
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_mobile)) {
      return false;
    } else if (_passport.text.trim().length > 0 && passportExp == '') {
      showToast(msg: "Invalid passport expiry date");
      return false;
    } else if (visaExp == '') {
      showToast(msg: "Invalid visa expiry date");
      return false;
    } else if (editProfileHelper.optTitle.id == null) {
      showToast(msg: "Please choose title from the list");
      return false;
    } else if (editProfileHelper.optGender.id == null) {
      showToast(msg: "Please choose gender from the list");
      return false;
    } else if (editProfileHelper.optMaritalStatus.id == null) {
      showToast(msg: "Please choose marital status from the list");
      return false;
    } else if (editProfileHelper.optCountriesBirth.id == null) {
      showToast(msg: "Please choose country of birth from the list");
      return false;
    } else if (editProfileHelper.optCountriesResidential.id == null) {
      showToast(msg: "Please choose country of residence from the list");
      return false;
    } else if (editProfileHelper.optNationalities.id == null) {
      showToast(msg: "Please choose nationality from the list");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                    //expandedHeight: 160,
                    elevation: 0,
                    //toolbarHeight: getHP(context, 10),
                    backgroundColor: MyTheme.appbarColor,
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: false,
                    title: Txt(
                        txt: "Edit profile",
                        txtColor: Colors.black,
                        txtSize: MyTheme.appbarTitleFontSize,
                        txtAlign: TextAlign.start,
                        isBold: false)),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (detail) {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Container(
          child: (editProfileHelper == null)
              ? SizedBox()
              : ListView(
                  shrinkWrap: true,
                  children: [
                    SizedBox(height: 10),
                    drawCamPicker(),
                    SizedBox(height: 10),
                    drawPersonalInfoView(),
                    SizedBox(height: 20),
                    drawVisaInfoView(),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 10, top: 20),
                      child: BSBtn(
                          txt: "Save Profile",
                          height: getHP(context, 7),
                          callback: () {
                            wsUpdateProfileAPI();
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Btn(
                        txt: "Deactivate MyAccount",
                        txtColor: Colors.white,
                        bgColor: Colors.redAccent,
                        width: getWP(context, 50),
                        height: getHP(context, 7),
                        radius: 10,
                        callback: () {
                          /*MyAlert(
                                  context,
                                  "Are you sure to deactivate your profile please?",
                                  null, (bool yes) {
                                if (yes) {
                                  wsDeactivateProfileAPI();
                                }
                              });*/
                          showDeactivateProfileDialog(
                              context: context,
                              email: _deactivateReason,
                              callback: (String reason) {
                                //
                                if (reason != null) {
                                  wsDeactivateProfileAPI(reason);
                                }
                              });
                        },
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
        ),
      ),
    );
  }

  drawCamPicker() {
    return Container(
      alignment: Alignment.center,
      child: Stack(
        children: [
          Container(
            width: getWP(context, 25),
            height: getWP(context, 25),
            child: Container(
              height: getW(context) * 0.3,
              width: getW(context) * 0.3,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: (_pathFG != null)
                      ? FileImage(_pathFG)
                      : AssetImage('assets/images/icons/user_icon.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: Container(
              width: 25,
              height: 25,
              child: MaterialButton(
                onPressed: () {
                  CamPicker().showCamModal(
                    context: context,
                    prefkey: ProfileHelper.ProfilePicFG_Key,
                    isRear: false,
                    callback: (File path) {
                      if (path != null) {
                        _pathFG = path;
                        wsMediaUploadFileAPI(path);
                      }
                    },
                  );
                },
                color: Colors.grey,
                child: Icon(
                  Icons.camera_alt_outlined,
                  size: 15,
                ),
                padding: EdgeInsets.all(0),
                shape: CircleBorder(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawPersonalInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      width: getW(context),
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: Container(
              width: getW(context),
              color: MyTheme.brownColor,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Txt(
                    txt: "Personal information",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownPicker(
              cap: "Choose Title",
              itemSelected: editProfileHelper.optTitle,
              dropListModel: editProfileHelper.ddTitle,
              onOptionSelected: (optionItem) {
                editProfileHelper.optTitle = optionItem;
                setState(() {});
              },
            ),
          ),
          SizedBox(height: 20),
          InputBox(
            ctrl: _fname,
            lableTxt: "First Name",
            kbType: TextInputType.name,
            len: 20,
            isPwd: false,
          ),
          InputBox(
            ctrl: _mname,
            lableTxt: "Middle Name",
            kbType: TextInputType.name,
            len: 20,
            isPwd: false,
          ),
          InputBox(
            ctrl: _lname,
            lableTxt: "Last Name",
            kbType: TextInputType.name,
            len: 20,
            isPwd: false,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: DropDownPicker(
              cap: "Choose Gender",
              itemSelected: editProfileHelper.optGender,
              dropListModel: editProfileHelper.ddGender,
              onOptionSelected: (optionItem) {
                editProfileHelper.optGender = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownPicker(
              cap: "Choose Marital Status",
              itemSelected: editProfileHelper.optMaritalStatus,
              dropListModel: editProfileHelper.ddMaritalStatus,
              onOptionSelected: (optionItem) {
                editProfileHelper.optMaritalStatus = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DatePickerView(
              cap: 'Select date of birth',
              dt: (dob == '') ? 'Select date of birth' : dob,
              initialDate: dateDOBlast,
              firstDate: dateDOBfirst,
              lastDate: dateDOBlast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      dob = DateFormat('dd-MM-yyyy').format(value).toString();
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownPicker(
              cap: "Country of birth",
              itemSelected: editProfileHelper.optCountriesBirth,
              dropListModel: editProfileHelper.ddMaritalCountriesBirth,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesBirth = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: " Email",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
                TxtBox(txt: email),
              ],
            ),
          ),
          InputBox(
            ctrl: _mobile,
            lableTxt: "Mobile Number",
            kbType: TextInputType.phone,
            len: 20,
            isPwd: false,
          ),
          InputBox(
            ctrl: _tel,
            lableTxt: "Telephone Number",
            kbType: TextInputType.phone,
            len: 20,
            isPwd: false,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: DropDownPicker(
              cap: "Country of Residence",
              itemSelected: editProfileHelper.optCountriesResidential,
              dropListModel: editProfileHelper.ddMaritalCountriesResidential,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesResidential = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownPicker(
              cap: "Nationality",
              itemSelected: editProfileHelper.optNationalities,
              dropListModel: editProfileHelper.ddMaritalNationalities,
              onOptionSelected: (optionItem) {
                editProfileHelper.optNationalities = optionItem;
                setState(() {});
              },
            ),
          ),
          /*SizedBox(height: 20),
          InputBox(
            ctrl: _ref,
            lableTxt: "Reference Source",
            kbType: TextInputType.text,
            len: 50,
            isPwd: false,
          ),*/
          drawSmooker(),
        ],
      ),
    );
  }

  drawVisaInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return Container(
      width: getW(context),
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: Container(
              width: getW(context),
              color: MyTheme.brownColor,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Txt(
                    txt: "Visa information",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
          ),
          SizedBox(height: 20),
          InputBox(
            ctrl: _nin,
            lableTxt: "National Insurance Number",
            kbType: TextInputType.text,
            len: 50,
            isPwd: false,
          ),
          InputBox(
            ctrl: _passport,
            lableTxt: "Passport Number",
            kbType: TextInputType.text,
            len: 50,
            isPwd: false,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: DatePickerView(
              cap: 'Passport Expiry Date',
              dt: (passportExp == '') ? 'Passport Expiry Date' : passportExp,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      passportExp =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownPicker(
              cap: "Visa Status",
              itemSelected: editProfileHelper.optVisaStatus,
              dropListModel: editProfileHelper.ddVisaStatus,
              onOptionSelected: (optionItem) {
                editProfileHelper.optVisaStatus = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: DatePickerView(
              cap: 'Visa Expiry Date',
              dt: (visaExp == '') ? 'Visa Expiry Date' : visaExp,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      visaExp =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  drawSmooker() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Are you a smooker?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: MyTheme.brownColor,
                disabledColor: MyTheme.brownColor,
                selectedRowColor: MyTheme.brownColor,
                indicatorColor: MyTheme.brownColor,
                toggleableActiveColor: MyTheme.brownColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radio = SmookerEnum.no;
                        });
                      }
                    },
                    child: Radio(
                      value: SmookerEnum.no,
                      groupValue: _radio,
                      onChanged: (SmookerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radio = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "No",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radio = SmookerEnum.yes;
                        });
                      }
                    },
                    child: Radio(
                      value: SmookerEnum.yes,
                      groupValue: _radio,
                      onChanged: (SmookerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radio = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Yes",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
