import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/more/settings/FcmTestNotiHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:Bright_Star/view/mywidgets/BtnOutline.dart';
import 'package:Bright_Star/view/mywidgets/Misc.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class TestNotiScreen extends StatefulWidget {
  @override
  State createState() => _TestNotiScreenState();
}

class _TestNotiScreenState extends State<TestNotiScreen> with Mixin {
  wsTestNotiAPI() async {
    try {
      final UserModel userModel = await DBMgr.shared.getUserProfile();
      final url = FcmTestNotiHelper().getUrl(
        userId: userModel.id,
      );
      log(url);
      await NetworkMgr()
          .getData<FcmTestNotiAPIModel, Null>(
        context: context,
        url: url,
        isLoading: true,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final msg = model.responseData.notification.description;
                showAlert(msg: msg);
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                final err = model.messages.pushMessage[0].toString();
                showToast(msg: err, which: 0);
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        } else {
          log("not in");
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: "Notification settings",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: drawTestUI(),
      ),
    );
  }

  drawTestUI() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Txt(
                        txt: "Is it working?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: BtnOutline(
                      txt: "Test it",
                      txtColor: MyTheme.brownColor,
                      borderColor: MyTheme.brownColor,
                      callback: () {
                        wsTestNotiAPI();
                      }),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt:
                      "Make sure you're actually getting those all important push notifications.",
                  txtColor: Colors.grey,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            drawLine(context: context, w: getW(context)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt:
                      "Your notifications can be updated at any time via the options below",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            drawLine(context: context, w: getW(context)),
          ],
        ),
      ),
    );
  }
}
