import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class CaseAlertScreen extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertScreen({Key key, @required this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: "Case alerts",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: drawCaseUI(context),
      ),
    );
  }

  drawCaseUI(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: MyTheme.brownColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: MyTheme.brownColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: BSBtn(
                  txt: "Close",
                  height: getHP(context, 7),
                  callback: () {
                    Get.back();
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
