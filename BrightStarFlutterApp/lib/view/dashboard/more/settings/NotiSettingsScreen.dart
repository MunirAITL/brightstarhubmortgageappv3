import 'dart:convert';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/config/dashboard/more/settings/NotiSettingsCfg.dart';
import 'package:Bright_Star/controller/helper/more/settings/NotiSettingsHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:Bright_Star/model/json/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';

class NotiSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotiSettingsScreenState();
}

class _NotiSettingsScreenState extends State<NotiSettingsScreen> with Mixin {
  UserModel userModel;
  NotiSettingsCfg notiSettingsCfg;

  bool isLoading = false;

  wsPostNotificationSettingsAPI() async {
    try {
      setState(() {
        isLoading = true;
      });
      final param = NotiSettingsHelper().getParam(
        userModel: userModel,
        notiSettingsCfg: notiSettingsCfg,
      );
      log(json.encode(param));
      await NetworkMgr()
          .postData<NotiSettingsPostAPIModel, Null>(
        context: context,
        url: Server.NOTI_SETTINGS_POST_URL,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            setState(() {
              isLoading = false;
            });
            if (model.success) {
              try {
                final msg =
                    model.messages.postNotificationSetting[0].toString();
                showToast(msg: msg, which: 1);
                //await NotiSettingsHelper().setSettings(notiSettingsCfg.listOpt);
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err =
                      model.messages.postNotificationSetting[0].toString();
                  showToast(msg: err, which: 1);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  wsGetNotificationSettingsAPI() async {
    try {
      setState(() {
        isLoading = true;
      });
      await NetworkMgr()
          .getData<NotiSettingsAPIModel, Null>(
        context: context,
        url: NotiSettingsHelper().getUrl(userModel: userModel),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final caseUpdateMap = notiSettingsCfg.listOpt[0];
                final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                final caseCompletedMap = notiSettingsCfg.listOpt[2];
                final caseReminderMap = notiSettingsCfg.listOpt[3];
                final userUpdateMap = notiSettingsCfg.listOpt[4];
                final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                final updateNewsLetterMap = notiSettingsCfg.listOpt[6];
                //  caseUpdate
                caseUpdateMap['isEmail'] = model
                    .responseData.userNotificationSetting.isCaseUpdateEmail;
                caseUpdateMap['isSms'] =
                    model.responseData.userNotificationSetting.isCaseUpdateSMS;
                caseUpdateMap['isPush'] = model.responseData
                    .userNotificationSetting.isCaseUpdateNotification;
                //  caseRecomendation
                caseRecomendationMap['isEmail'] = model.responseData
                    .userNotificationSetting.isCaseRecomendationEmail;
                caseRecomendationMap['isPush'] = model.responseData
                    .userNotificationSetting.isCaseRecomendationNotification;
                //  caseCompleted
                caseCompletedMap['isEmail'] = model
                    .responseData.userNotificationSetting.isCaseCompletedEmail;
                caseCompletedMap['isSms'] = model
                    .responseData.userNotificationSetting.isCaseCompletedSMS;
                caseCompletedMap['isPush'] = model.responseData
                    .userNotificationSetting.isCaseCompletedNotification;
                //  caseReminder
                caseReminderMap['isEmail'] = model
                    .responseData.userNotificationSetting.isCaseReminderEmail;
                caseReminderMap['isSms'] = model
                    .responseData.userNotificationSetting.isCaseReminderSMS;
                caseReminderMap['isPush'] = model.responseData
                    .userNotificationSetting.isCaseReminderNotification;
                //  userUpdateMap
                userUpdateMap['isEmail'] = model
                    .responseData.userNotificationSetting.isUserUpdateEmail;
                userUpdateMap['isPush'] = model.responseData
                    .userNotificationSetting.isUserUpdateNotification;
                //  helpfulInfoMap
                helpfulInfoMap['isEmail'] = model.responseData
                    .userNotificationSetting.isHelpfulInformationEmail;
                helpfulInfoMap['isPush'] = model.responseData
                    .userNotificationSetting.isHelpfulInformationNotification;
                //  updateNewsLetterMap
                updateNewsLetterMap['isEmail'] = model.responseData
                    .userNotificationSetting.isUpdateAndNewsLetterEmail;
                updateNewsLetterMap['isPush'] = model.responseData
                    .userNotificationSetting.isUpdateAndNewsLetterNotification;

                setState(() {
                  isLoading = false;
                });
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                setState(() {
                  isLoading = true;
                });
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showToast(msg: "Timeline not found");
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    notiSettingsCfg = null;
    userModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
      notiSettingsCfg = NotiSettingsCfg();
      await wsGetNotificationSettingsAPI();
    } catch (e) {}
    try {
      /*await NotiSettingsHelper().getSettings().then((listOpt) {
        if (listOpt != null) {
          notiSettingsCfg.listOpt = listOpt;
          setState(() {});
        } else {}
      });*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: "Notification settings",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),

                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(0),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
      child: Container(
        child: (notiSettingsCfg == null)
            ? SizedBox()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 20, bottom: 10),
                      child: Txt(
                          txt:
                              "Your notifications can be updated at any time via the options below or the Bright Star App.",
                          txtColor: MyTheme.brownColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          txtLineSpace: 1.2,
                          isBold: true),
                    ),
                    for (var listOpt in notiSettingsCfg.listOpt)
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          width: double.infinity,
                          //color: Colors.black,
                          child: Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 10, left: 10),
                                  child: Txt(
                                      txt: listOpt['title'],
                                      txtColor: Colors.black,
                                      txtSize: 2,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                ),
                                SizedBox(height: 10),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 10, left: 10),
                                  child: Txt(
                                      txt: listOpt['subTitle'],
                                      txtColor: Colors.black,
                                      txtSize: 2,
                                      txtLineSpace: 1.2,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                                (listOpt['email'] as bool)
                                    ? Theme(
                                        data: ThemeData(
                                          accentColor: MyTheme.brownColor,
                                          unselectedWidgetColor: Colors.black,
                                        ),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isEmail'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isEmail'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Email',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                (listOpt['sms'] as bool)
                                    ? Theme(
                                        data: ThemeData(
                                          accentColor: MyTheme.brownColor,
                                          unselectedWidgetColor: Colors.black,
                                        ),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isSms'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isSms'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Sms',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                (listOpt['push'] as bool)
                                    ? Theme(
                                        data: ThemeData(
                                          accentColor: MyTheme.brownColor,
                                          unselectedWidgetColor: Colors.black,
                                        ),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isPush'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isPush'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Push',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15, right: 15, top: 10, bottom: 20),
                      child: BSBtn(
                          txt: "Save",
                          height: getHP(context, 7),
                          callback: () {
                            wsPostNotificationSettingsAPI();
                          }),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
