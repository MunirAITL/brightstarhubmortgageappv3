import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/more/MoreHelper.dart';
import 'package:Bright_Star/view/dashboard/more/settings/TestNotiScreen.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'EditProfileScreen.dart';
import 'NotiSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  List<String> listItems = [
    "Edit account",
    "Notification settings",
    "Test Notification"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: 0,
          //toolbarHeight: getHP(context, 10),
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: "Settings",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: listItems.length,
            itemBuilder: (context, index) {
              final item = listItems[index];
              return GestureDetector(
                onTap: () async {
                  switch (index) {
                    case 0:
                      Get.to(
                        () => EditProfileScreen(),
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    case 1:
                      Get.to(
                        () => NotiSettingsScreen(),
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    case 2:
                      Get.to(
                        () => TestNotiScreen(),
                      ).then((value) {
                        //callback(route);
                      });
                      break;
                    default:
                  }
                },
                child: Container(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Txt(
                        txt: item,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: (index != MoreHelper.listMore.length - 1)
                        ? Icon(
                            Icons.arrow_right,
                            color: Colors.black,
                            size: 30,
                          )
                        : SizedBox(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
