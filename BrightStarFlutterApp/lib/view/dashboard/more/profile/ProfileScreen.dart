import 'dart:io';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/model/data/PrefMgr.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/dashboard/more/settings/EditProfileScreen.dart';
import 'package:Bright_Star/view/mywidgets/CamPicker.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:share/share.dart';
import 'package:Bright_Star/view/mywidgets/BtnOutline.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with Mixin {
  UserModel userModel;

  File _pathBG;
  File _pathFG;

  var address;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    userModel = null;
    _pathBG = null;
    _pathFG = null;
    address = null;
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();

      try {
        final bgPath =
            await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicBG_Key);
        if (bgPath != null) {
          _pathBG = File(bgPath);
        }
      } catch (e) {}
      try {
        final fgPath =
            await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicFG_Key);
        if (fgPath != null) {
          _pathFG = File(fgPath);
        }
      } catch (e) {}
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    if (userModel != null) {
      address = userModel.address;
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        //resizeToAvoidBottomPadding: true,
        body: (userModel == null)
            ? SizedBox()
            : NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      elevation: 0,
                      //toolbarHeight: getHP(context, 10),
                      backgroundColor: MyTheme.appbarColor,
                      iconTheme: IconThemeData(
                          color: Colors.black //change your color here
                          ),
                      title: Txt(
                          txt: "Profile",
                          txtColor: Colors.black,
                          txtSize: MyTheme.appbarTitleFontSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      actions: [
                        GestureDetector(
                          onTap: () async {
                            Get.to(
                              () => EditProfileScreen(),
                            ).then((value) {
                              //callback(route);
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Icon(Icons.edit),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            final msg = 'Hola Bright Star';
                            await Share.share(msg);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Icon(Icons.share),
                          ),
                        ),
                      ],
                      expandedHeight: getHP(context, 70),
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                        //collapseMode: CollapseMode.pin,
                        centerTitle: true,
                        background: Container(
                          decoration: (_pathBG != null)
                              ? BoxDecoration(
                                  image: DecorationImage(
                                    image: FileImage(_pathBG),
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : BoxDecoration(color: MyTheme.appbarColor),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: getHP(context, 12)),
                              Align(
                                alignment: Alignment.topRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 20),
                                  child: Container(
                                    width: 25,
                                    height: 25,
                                    child: MaterialButton(
                                      onPressed: () {
                                        CamPicker().showCamModal(
                                          context: context,
                                          prefkey:
                                              ProfileHelper.ProfilePicBG_Key,
                                          isRear: true,
                                          callback: (File path) {
                                            if (path != null) {
                                              setState(() {
                                                _pathBG = path;
                                              });
                                            }
                                          },
                                        );
                                      },
                                      color: Colors.grey,
                                      child: Icon(
                                        Icons.camera_alt_outlined,
                                        size: 15,
                                      ),
                                      padding: EdgeInsets.all(0),
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: getHP(context, 3)),
                              Stack(
                                children: [
                                  Container(
                                    width: getWP(context, 25),
                                    height: getWP(context, 25),
                                    child: Container(
                                      height: getW(context) * 0.3,
                                      width: getW(context) * 0.3,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: (_pathFG != null)
                                              ? FileImage(_pathFG)
                                              : AssetImage(
                                                  'assets/images/icons/user_icon.png'),
                                          fit: BoxFit.fill,
                                        ),
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      child: MaterialButton(
                                        onPressed: () {
                                          CamPicker().showCamModal(
                                            context: context,
                                            prefkey:
                                                ProfileHelper.ProfilePicFG_Key,
                                            isRear: false,
                                            callback: (File path) {
                                              if (path != null) {
                                                setState(() {
                                                  _pathFG = path;
                                                });
                                              }
                                            },
                                          );
                                        },
                                        color: Colors.grey,
                                        child: Icon(
                                          Icons.camera_alt_outlined,
                                          size: 15,
                                        ),
                                        padding: EdgeInsets.all(0),
                                        shape: CircleBorder(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: getHP(context, 5)),
                              Txt(
                                  txt: userModel.name,
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                              SizedBox(height: getHP(context, 2)),
                              ProfileHelper().getUserOnlineStatus(
                                  context: context, statusIndex: 1),
                              SizedBox(height: getHP(context, 3)),
                              Container(
                                width: getWP(context, 70),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/icons/pin_icon.png",
                                      width: 20,
                                      height: 20,
                                      color: Colors.black,
                                    ),
                                    (address.length > 0)
                                        ? Expanded(
                                            child: Txt(
                                                txt: address,
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          )
                                        : SizedBox()
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      //background:
                    ),
                  ];
                },
                body: ListView(
                  shrinkWrap: true,
                  primary: true,
                  children: [
                    SizedBox(height: getHP(context, 10)),
                    ProfileHelper().getStarRatingView(rate: 4, reviews: 5),
                    ProfileHelper().getCompletionText(pa: 50),
                    ProfileHelper().getAvatorStarView(rate: 3),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Txt(
                          txt: "Residential Mortgage ",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: true),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            left: getWP(context, 10),
                            right: getWP(context, 10),
                            top: 10),
                        child: BtnOutline(
                            txt: "Learn more",
                            txtColor: MyTheme.brownColor,
                            borderColor: Colors.black,
                            callback: () {
                              openUrl(context, Server.ABOUTUS_URL);
                            })),
                  ],
                ),
              ),
      ),
    );
  }
}
