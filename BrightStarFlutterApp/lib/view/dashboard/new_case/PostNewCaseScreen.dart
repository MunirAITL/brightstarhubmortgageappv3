import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/config/dashboard/NewCaseCfg.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/controller/helper/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:Bright_Star/controller/helper/tab_newcase/PostNewCaseHelper.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/tab_newcase/UserNotesModel.dart';
import 'package:Bright_Star/model/json/tab_newcase/PostCaseAPIModel.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/dashboard/my_cases/MyCaseTab.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:Bright_Star/view/mywidgets/TxtBox.dart';
import 'package:Bright_Star/view/mywidgets/com/OtherApplicantSwitchView.dart';
import 'package:Bright_Star/view/mywidgets/com/SPVSwitchView.dart';
import 'package:Bright_Star/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class PostNewCaseScreen extends StatefulWidget {
  final int indexCase;
  const PostNewCaseScreen({
    Key key,
    @required this.indexCase,
  }) : super(key: key);
  @override
  State createState() => _PostNewCaseScreenState();
}

class _PostNewCaseScreenState extends State<PostNewCaseScreen>
    with Mixin, StateListener {
  final TextEditingController _note = TextEditingController();

  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regAddr = TextEditingController();
  final _regNo = TextEditingController();
  String regDate = "";

  var title = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  int otherApplicantRadioIndex = 1;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state) {
    if (state == ObserverState.STATE_CHANGED_otherapplicant) {
      isOtherApplicantSwitch = !isOtherApplicantSwitch;
      log("observer called 1");
    } else if (state == ObserverState.STATE_CHANGED_spvswitchview) {
      isSPVSwitch = !isSPVSwitch;
      log("observer called 2");
    }
  }

  wsOnPostCase() async {
    try {
      if (_note.text.trim().length == 0) {
        showToast(msg: "Please enter case note");
        return;
      }

      final UserModel userModel = await DBMgr.shared.getUserProfile();
      final param = PostNewCaseHelper().getParam(
        isOtherApplicantSwitch: isOtherApplicantSwitch,
        isSPVSwitch: isSPVSwitch,
        compName: _compName.text.trim(),
        regAddr: _regAddr.text.trim(),
        regDate: regDate.trim(),
        regNo: _regNo.text.trim(),
        title: title.trim(),
        note: _note.text.trim(),
        userModel: userModel,
      );
      log(param);
      await NetworkMgr()
          .postData<PostCaseAPIModel, Null>(
        context: context,
        url: Server.POSTCASE_URL,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            final UserNotesModel caseModel = model.responseData.task;
            Get.to(
              () => WebScreen(
                title: "New Case - " + title,
                url: CaseDetailsWebHelper().getLink(
                  title: title,
                  taskId: caseModel.id,
                ),
              ),
              fullscreenDialog: true,
            ).then((value) {
              try {
                _stateProvider.notify(
                    ObserverState.STATE_CHANGED_newcase_reload_case_api);
                _stateProvider.notify(
                    ObserverState.STATE_CHANGED_mycases_reload_case_api);

                Get.off(
                  () => MyCaseTab(),
                ).then((value) {
                  //callback(route);
                });
              } catch (e) {
                log(e.toString());
              }
            });

            Get.to(
              () => WebScreen(
                title: "New Case - " + title,
                url: CaseDetailsWebHelper().getLink(
                  title: title,
                  taskId: caseModel.id,
                ),
              ),
              fullscreenDialog: true,
            ).then((value) {
              try {
                _stateProvider.notify(
                    ObserverState.STATE_CHANGED_newcase_reload_case_api);
                _stateProvider.notify(
                    ObserverState.STATE_CHANGED_mycases_reload_case_api);
                Get.off(
                  () => MyCaseTab(),
                ).then((value) {
                  //callback(route);
                });
              } catch (e) {
                log(e.toString());
              }
            });
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    _note.dispose();
    _compName.dispose();
    _regAddr.dispose();
    _regNo.dispose();
    listOApplicantInputFieldsCtr = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
    } catch (e) {}
    try {
      final map = NewCaseCfg.listCreateNewCase[widget.indexCase];
      //final icon = map["url"];
      title = map["title"];
      if (map["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (map["isSPV"]) {
        isSPVSwitchShow = true;
      }
    } catch (e) {}
    try {
      _compName.addListener(() {
        log(_compName.text);
      });
      _regAddr.addListener(() {
        log(_regAddr.text);
      });
      _regNo.addListener(() {
        log(_regNo.text);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: Txt(
                      txt: "New Case",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getHP(context, 2)),
            drawCaseLayout(),
            (isOtherApplicantSwitchShow)
                ? OtherApplicantSwitchView(
                    listOApplicantInputFieldsCtr: listOApplicantInputFieldsCtr,
                    callback: (value) {
                      otherApplicantRadioIndex = value;
                      log(value);
                    },
                  )
                : SizedBox(),
            (isSPVSwitchShow)
                ? SPVSwitchView(
                    compName: _compName,
                    regAddr: _regAddr,
                    regNo: _regNo,
                    callback: (value) {
                      regDate = value;
                    },
                  )
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
              child: BSBtn(
                  txt: "Continue",
                  height: getHP(context, 7),
                  callback: () {
                    wsOnPostCase();
                  }),
            ),
            SizedBox(height: getHP(context, 10)),
          ],
        ),
      ),
    );
  }

  drawCaseLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30),
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "Case Type",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              TxtBox(txt: title, height: 5),
              SizedBox(height: 20),
              Txt(
                  txt: "Case Note",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Container(
                //padding: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _note,
                    minLines: 5,
                    maxLines: 10,
                    autocorrect: false,
                    keyboardType: TextInputType.multiline,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getTxtSize(context: context),
                    ),
                    decoration: new InputDecoration(
					border: InputBorder.none,
					focusedBorder: InputBorder.none,
					enabledBorder: InputBorder.none,
					errorBorder: InputBorder.none,
					disabledBorder: InputBorder.none,
                      hintText: " Case Note",
                      hintStyle: new TextStyle(
                        color: Colors.grey,
                        fontSize: getTxtSize(context: context),
                      ),
                      contentPadding: const EdgeInsets.symmetric(vertical: 0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
