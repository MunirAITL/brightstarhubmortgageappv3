import 'package:Bright_Star/Mixin.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class Txt extends StatelessWidget with Mixin {
  final txt;
  Color txtColor;
  double txtSize;
  double txtLineSpace;
  TextAlign txtAlign;
  final isBold;
  final isOverflow;

  Txt({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.txtSize,
    @required this.txtAlign,
    @required this.isBold,
    this.txtLineSpace = 1,
    this.isOverflow = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return //FittedBox(
        //fit: BoxFit.fitWidth,
        //child:
        Text(
      txt,
      textAlign: txtAlign,
      overflow: (isOverflow) ? TextOverflow.ellipsis : TextOverflow.visible,
      style: TextStyle(
        fontFamily: 'Fieldwork',
        height: txtLineSpace,
        fontSize: getTxtSize(context: context, txtSize: txtSize),
        color: txtColor,
        fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
      ),
      //),
    );
  }
}

extension CustomStyles on TextTheme {
  TextStyle get error {
    return TextStyle(
      fontSize: 20.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    );
  }
}
