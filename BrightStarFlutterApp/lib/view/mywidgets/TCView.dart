import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;
  Color txt1Color;
  Color txt2Color;

  TCView({
    @required this.screenName,
    this.txt1Color = Colors.black,
    this.txt2Color = Colors.blue,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: "By clicking on '" +
                      screenName +
                      "' you confirm that you accept the ",
                  style: TextStyle(
                    color: txt1Color,
                    fontSize: getTxtSize(context: context),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Bright Star Terms and Conditions',
                        style: TextStyle(
                            color: txt2Color,
                            fontSize: getTxtSize(context: context),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: Server.TC_URL,
                              ),
                              fullscreenDialog: true,
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
