import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/config/Server.dart';
import 'package:Bright_Star/controller/network/CookieMgr.dart';
import 'package:Bright_Star/view/mywidgets/AppbarBotProgbar.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';
import 'package:path/path.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;

  const WebScreen({
    Key key,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  final GlobalKey webViewKey = GlobalKey();
  InAppWebViewController webView;

  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  double progress = 0;
  String cookieStr;
  final CookieManager cookieManager = CookieManager.instance();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;
    //webView = null;
    flutterWebviewPlugin.close();
    super.dispose();
  }

  appInit() async {
    try {
      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Server.BASE_URL,
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );

        flutterWebviewPlugin.onUrlChanged.listen((String url) {
          log("onUrlChanged::" + url);
        });
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: widget.title,
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () async {
              Get.back();
            },
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: drawWebView(),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: drawBottomNavBar(),
                    ),
                    Align(
                        alignment: Alignment.topCenter,
                        child: _buildProgressBar()),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawWebView() {
    return InAppWebView(
      key: webViewKey,
      initialUrl: widget.url,
      //initialHeaders: {'Cookie': cookieStr},
      initialOptions: InAppWebViewGroupOptions(
        crossPlatform: InAppWebViewOptions(
          //minimumFontSize: 14,
          debuggingEnabled: true,
          javaScriptEnabled: true,
          javaScriptCanOpenWindowsAutomatically: true,
          useShouldOverrideUrlLoading: true,
          mediaPlaybackRequiresUserGesture: false,
          useShouldInterceptAjaxRequest: true,
          useShouldInterceptFetchRequest: true,
          useOnDownloadStart: true,
          //useOnDownloadStart: true,
          useOnLoadResource: true,
          verticalScrollBarEnabled: true,
          //horizontalScrollBarEnabled: true,
          //preferredContentMode: UserPreferredContentMode.MOBILE,
        ),
        ios: IOSInAppWebViewOptions(
          allowsInlineMediaPlayback: true,
        ),
        android: AndroidInAppWebViewOptions(
          //minimumLogicalFontSize: 1,
          saveFormData: true,
          hardwareAcceleration: true,
          domStorageEnabled: true,
          databaseEnabled: true,
          clearSessionCache: true,
          thirdPartyCookiesEnabled: true,
          allowUniversalAccessFromFileURLs: true,
          allowFileAccess: true,
          allowContentAccess: true,
          useWideViewPort: false,
        ),
      ),
      onWebViewCreated: (InAppWebViewController controller) {
        webView = controller;
      },
      androidOnPermissionRequest: (controller, origin, resources) async {
        return PermissionRequestResponse(
            resources: resources,
            action: PermissionRequestResponseAction.GRANT);
      },
      shouldOverrideUrlLoading: (controller, navigationAction) async {
        //var uri = navigationAction.url;

        /*if (!["http", "https", "file", "chrome", "data", "javascript", "about"]
            .contains(uri)) {
          if (await canLaunch(widget.url)) {
            // Launch the App
            await launch(
              widget.url,
            );
            // and cancel the request
            return ShouldOverrideUrlLoadingAction.CANCEL;
          }
        }*/

        return ShouldOverrideUrlLoadingAction.ALLOW;
      },
      onLoadStart: (InAppWebViewController controller, String url) {},
      onLoadStop: (InAppWebViewController controller, String url) async {},
      onProgressChanged: (InAppWebViewController controller, int progress) {
        setState(() {
          this.progress = progress / 100;
        });
      },
    );
  }

  Widget _buildProgressBar() {
    if (progress != 1.0) {
      //return CircularProgressIndicator();
      // You can use LinearProgressIndicator also
      return AppbarBotProgBar(
        backgroundColor: MyTheme.appbarProgColor,
      );
    }
    return Container();
  }

  drawBottomNavBar() {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
            onTap: () {
              if (flutterWebviewPlugin != null) {
                flutterWebviewPlugin.goBack();
              }
            },
            child: Icon(Icons.arrow_back, color: MyTheme.brownColor, size: 30)),
        GestureDetector(
            onTap: () {
              if (flutterWebviewPlugin != null) {
                flutterWebviewPlugin.goForward();
              }
            },
            child: Icon(
              Icons.arrow_forward,
              color: MyTheme.brownColor,
              size: 30,
            )),

        /*MaterialButton(
          color: Colors.white,
          child: Icon(
            Icons.refresh,
            color: Colors.black,
          ),
          onPressed: () {
            if (flutterWebviewPlugin != null) {
              flutterWebviewPlugin.reload();
            }
          },
        ),*/
      ],
    );
  }
}

class _WebScreenState2 extends State<WebScreen> with Mixin {
  //final GlobalKey webViewKey = GlobalKey();
  //InAppWebViewController webView;

  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  double progress = 0;
  bool isLoading = false;
  String cookieStr;
  final CookieManager cookieManager = CookieManager.instance();

  bool _shouldShowBottomBar = true;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;
    //webView = null;
    flutterWebviewPlugin.close();
    super.dispose();
  }

  appInit() async {
    try {
      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Server.BASE_URL,
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );

        flutterWebviewPlugin.onUrlChanged.listen((String url) {
          log("onUrlChanged::" + url);
        });

        flutterWebviewPlugin.onStateChanged.listen((viewState) {
          if (viewState.type == WebViewState.startLoad) {
            setState(() {
              isLoading = true;
            });
          } else if (viewState.type == WebViewState.finishLoad) {
            setState(() {
              isLoading = false;
            });
          }
        });

        flutterWebviewPlugin.onScrollYChanged.listen((double y) {
          if (mounted) {
            /*setState(() {
              _shouldShowBottomBar = y < 50;
            });*/
          }
        });

        setState(() {});
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WebviewScaffold(
        scrollBar: true,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          title: Txt(
              txt: widget.title,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () async {
              Get.back();
            },
          ),
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(0),
              child: (isLoading)
                  ? AppbarBotProgBar(
                      backgroundColor: MyTheme.appbarProgColor,
                    )
                  : SizedBox()),
        ),
        bottomNavigationBar: _shouldShowBottomBar
            ? BottomAppBar(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: const Icon(Icons.arrow_back_ios),
                        onPressed: () {
                          flutterWebviewPlugin.goBack();
                        },
                      ),
                      IconButton(
                        icon: const Icon(Icons.arrow_forward_ios),
                        onPressed: () {
                          flutterWebviewPlugin.goForward();
                        },
                      ),
                      /*IconButton(
                      icon: const Icon(Icons.autorenew),
                      onPressed: () {
                        flutterWebviewPlugin.reload();
                      },
                    ),*/
                    ],
                  ),
                ),
              )
            : null,
        allowFileURLs: true,
        resizeToAvoidBottomInset: true,
        url: widget.url,
        withLocalStorage: true,
        enableAppScheme: true,
        hidden: true,
        withZoom: true,
        appCacheEnabled: false,
        debuggingEnabled: true,
        clearCookies: false,
        mediaPlaybackRequiresUserGesture: false,
        ignoreSSLErrors: true,
        useWideViewPort: false,
        javascriptChannels: [
          JavascriptChannel(
              name: 'Print',
              onMessageReceived: (JavascriptMessage message) {
                print(message.message);
              }),
        ].toSet(),
        withJavascript: true,
        initialChild: Container(
          color: Colors.white,
          child: Center(
            child: Txt(
                txt: "Loading...",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
      ),
    );
  }
}
