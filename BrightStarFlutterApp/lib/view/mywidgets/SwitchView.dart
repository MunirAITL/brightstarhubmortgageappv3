import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';

class SwitchView extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  const SwitchView({
    Key key,
    @required this.value,
    @required this.onChanged,
  }) : super(key: key);

  @override
  State createState() => _SwitchViewState();
}

class _SwitchViewState extends State<SwitchView> with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWP(context, 35),
      //height: getHP(context, 6),
      child: Row(
        children: [
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (widget.value) ? MyTheme.brownColor : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "Yes",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - 0.4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(true);
                },
              ),
            ),
          ),
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (!widget.value) ? MyTheme.brownColor : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topRight: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "No",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - 0.4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(false);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
