import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/InputBox.dart';
import 'package:Bright_Star/view/mywidgets/SwitchView.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';

class OtherApplicantSwitchView extends StatefulWidget {
  final Function(int) callback;
  List<TextEditingController> listOApplicantInputFieldsCtr;
  OtherApplicantSwitchView({
    Key key,
    @required this.listOApplicantInputFieldsCtr,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _OtherApplicantSwitchViewState();
}

enum RadioEnum { one, two, three }

class _OtherApplicantSwitchViewState extends State<OtherApplicantSwitchView>
    with Mixin {
  bool isSwitch = false;

  RadioEnum _character = RadioEnum.one;

  List<String> listInputFieldsTitle = [
    "Please enter Second Applicant's email address:",
    "Please enter Third Applicant's email address:",
    "Please enter Fourth Applicant's email address:"
  ];
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Txt(
                        txt: "Is there other applicant?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                        txtLineSpace: 1.5,
                      ),
                    ),
                    //SizedBox(width: 10),
                    SwitchView(
                      value: isSwitch,
                      onChanged: (value) {
                        isSwitch = value;
                        if (mounted) {
                          setState(() {
                            //_stateProvider.notify(ObserverState.STATE_CHANGED);
                          });
                        }
                        //callback((isSwitch) ? _companyName.text.trim() : '');
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          (isSwitch) ? drawApplicantView() : SizedBox(),
        ],
      ),
    );
  }

  drawApplicantView() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Txt(
                  txt:
                      "How many people do you want named on the case application?*",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false,
                  txtLineSpace: 1.2,
                ),
                SizedBox(height: 10),
                Txt(
                  txt:
                      "It's ok if the person you're applying with doesn't have an income-they can still be named on your case application. Choose an option from the below One, Two or Three",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false,
                  txtLineSpace: 1.2,
                ),
                drawApplicantRadioView(),
                drawInputFieldsView(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawApplicantRadioView() {
    return Container(
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Container(
            //color: Colors.transparent,
            child: Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: MyTheme.brownColor,
                disabledColor: MyTheme.brownColor,
                selectedRowColor: MyTheme.brownColor,
                indicatorColor: MyTheme.brownColor,
                toggleableActiveColor: MyTheme.brownColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.one;
                          widget.callback(1);
                        });
                      }
                    },
                    child: Radio(
                      value: RadioEnum.one,
                      groupValue: _character,
                      onChanged: (RadioEnum value) {
                        if (mounted) {
                          setState(() {
                            _character = value;
                            widget.callback(1);
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "One",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),

                  //SizedBox(width: 10),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.two;
                          widget.callback(2);
                        });
                      }
                    },
                    child: Radio(
                      value: RadioEnum.two,
                      groupValue: _character,
                      onChanged: (RadioEnum value) {
                        if (mounted) {
                          setState(() {
                            _character = value;
                            widget.callback(2);
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Two",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),

                  //SizedBox(width: 10),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.three;
                          widget.callback(3);
                        });
                      }
                    },
                    child: Radio(
                      value: RadioEnum.three,
                      groupValue: _character,
                      onChanged: (RadioEnum value) {
                        if (mounted) {
                          setState(() {
                            _character = value;
                            widget.callback(3);
                          });
                        }
                      },
                    ),
                  ),
                  Flexible(
                    child: Txt(
                      txt: "Three",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawInputFieldsView() {
    try {
      int totalInputFields = 0;
      switch (_character.index) {
        case 0:
          totalInputFields = 1;
          break;
        case 1:
          totalInputFields = 2;
          break;
        case 2:
          totalInputFields = 3;
          break;
        default:
      }
      return (totalInputFields == 0)
          ? SizedBox()
          : Container(
              child: Column(
                children: [
                  //SizedBox(height: 10),
                  for (int i = 0; i < totalInputFields; i++)
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Txt(
                              txt: listInputFieldsTitle[i],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                        InputBox(
                          ctrl: widget.listOApplicantInputFieldsCtr[i],
                          lableTxt:
                              "applicant" + (i + 2).toString() + "@example.com",
                          kbType: TextInputType.emailAddress,
                          len: 100,
                          isPwd: false,
                          leftPad: 10,
                          rightPad: 10,
                        ),
                      ],
                    ),
                ],
              ),
            );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
