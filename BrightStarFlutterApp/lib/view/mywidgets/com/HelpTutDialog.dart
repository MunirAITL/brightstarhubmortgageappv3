import 'dart:ui';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/more/HelpTutHelper.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';

class HelpTutDialog extends StatefulWidget {
  final Function(int) callback;
  const HelpTutDialog({Key key, @required this.callback}) : super(key: key);
  @override
  State createState() => _HelpTutDialogState();
}

class _HelpTutDialogState extends State<HelpTutDialog> with Mixin {
  int index = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    HelpTutHelper().listTips = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: MyTheme.brownColor,
      child: Container(
        width: getW(context),
        height: getHP(context, 30),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            new Align(
              alignment: FractionalOffset.topRight,
              child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    //
                    Navigator.of(context).pop();
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Txt(
                txt: HelpTutHelper().listTips[index],
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize - 0.2,
                txtAlign: TextAlign.center,
                isBold: true,
                txtLineSpace: 1.2,
              ),
            ),
            Positioned(
              bottom: 5,
              left: 10,
              right: 10,
              child: Container(
                color: MyTheme.themeData.accentColor,
                height: getHP(context, 7),
                width: double.infinity,
                child: Row(
                  children: [
                    (index == 0)
                        ? SizedBox()
                        : Transform.translate(
                            offset: Offset(0, -10),
                            child: IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_left,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index--;
                                  widget.callback(index);
                                });
                              },
                            ),
                          ),
                    Expanded(
                      child: Txt(
                        txt: HelpTutHelper().listTab[index].toString(),
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.center,
                        isBold: true,
                      ),
                    ),
                    (index == HelpTutHelper().listTips.length - 1)
                        ? SizedBox()
                        : Transform.translate(
                            offset: Offset(0, -10),
                            child: IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_right,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index++;
                                  widget.callback(index);
                                });
                              },
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
