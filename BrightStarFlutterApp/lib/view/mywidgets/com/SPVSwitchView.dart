import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/view/mywidgets/InputBox.dart';
import 'package:Bright_Star/view/mywidgets/SwitchView.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:intl/intl.dart';
import '../IcoTxtIco.dart';

class SPVSwitchView extends StatefulWidget {
  final Function(String) callback;
  final TextEditingController compName;
  final TextEditingController regAddr;
  final TextEditingController regNo;
  SPVSwitchView({
    Key key,
    @required this.compName,
    @required this.regAddr,
    @required this.regNo,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _SPVSwitchViewState();
}

class _SPVSwitchViewState extends State<SPVSwitchView> with Mixin {
  bool isSwitch = false;
  String dt = "";
  String dd = "";
  String mm = "";
  String yy = "";
  //final Function(String) callback;

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Txt(
                        txt:
                            "Are you buying the property in the name of a SPV (Limited Company)?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                        txtLineSpace: 1.5,
                      ),
                    ),
                    //SizedBox(width: 10),
                    SwitchView(
                      value: isSwitch,
                      onChanged: (value) {
                        isSwitch = value;
                        if (mounted) {
                          setState(() {
                            _stateProvider.notify(
                                ObserverState.STATE_CHANGED_spvswitchview);
                          });
                        }
                        //callback((isSwitch) ? _companyName.text.trim() : '');
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          (isSwitch) ? drawSPVInputFields() : SizedBox(),
        ],
      ),
    );
  }

  drawSPVInputFields() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                //SizedBox(height: 10),
                InputBox(
                  ctrl: widget.compName,
                  lableTxt: "Name of Company",
                  kbType: TextInputType.text,
                  len: 100,
                  isPwd: false,
                  leftPad: 0,
                  rightPad: 0,
                ),
                InputBox(
                  ctrl: widget.regAddr,
                  lableTxt: "Registered Address",
                  kbType: TextInputType.text,
                  len: 200,
                  isPwd: false,
                  leftPad: 0,
                  rightPad: 0,
                ),
                InputBox(
                  ctrl: widget.regAddr,
                  lableTxt: "Company Registeration Number",
                  kbType: TextInputType.text,
                  len: 200,
                  isPwd: false,
                  leftPad: 0,
                  rightPad: 0,
                ),
                drawRegDate(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawRegDate() {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Date registered",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          SizedBox(height: 5),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme:
                          ColorScheme.light(primary: MyTheme.brownColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  if (mounted) {
                    setState(() {
                      try {
                        dt = DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = date.toString().split('-');
                        this.dd = dobArr[0];
                        this.mm = dobArr[1];
                        this.yy = dobArr[2];
                        widget.callback(dt);
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: (dt == "") ? "Select a date" : dt,
              rightIcon: Icons.keyboard_arrow_down,
              txtAlign: TextAlign.left,
              rightIconSize: 30,
            ),
          ),
        ],
      ),
    );
  }
}
