import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/model/json/tab_newcase/UserNotesModel.dart';
import 'package:Bright_Star/view/mywidgets/Btn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../Mixin.dart';
import '../Txt.dart';

class CaseNoteDoneDialog extends StatelessWidget with Mixin {
  final Function(bool) callback;
  final UserNotesModel userNotesModel;

  const CaseNoteDoneDialog({
    Key key,
    @required this.callback,
    @required this.userNotesModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: MyTheme.themeData.accentColor,
      child: Container(
        width: getWP(context, 80),
        //height: getHP(context, 55),
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Txt(
                      txt: "Task Details",
                      txtColor: MyTheme.brownColor,
                      txtSize: 2.5,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                )
              ],
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  drawInfoBox("Case No:", userNotesModel.id.toString()),
                  SizedBox(height: 20),
                  drawInfoBox("Title:", userNotesModel.title),
                  SizedBox(height: 20),
                  drawInfoBox("Description:", userNotesModel.comments),
                  SizedBox(height: 20),
                  drawInfoBox("Assigned by:", userNotesModel.initiatorName),
                  //SizedBox(height: 20),
                  /*Txt(
                      txt:
                          "NB: After completing the task, please click the 'YES' Button Otherwise click the 'No' Button.",
                      txtColor: Colors.black,
                      txtSize: 1.8,
                      txtAlign: TextAlign.center,
                      isBold: false),*/
                ],
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Btn(
                    txt: "No",
                    txtColor: Colors.black,
                    bgColor: Colors.grey,
                    width: getWP(context, 30),
                    height: getHP(context, 5),
                    callback: () {
                      Get.back();
                      callback(false);
                    }),
                Btn(
                    txt: "Yes",
                    txtColor: Colors.white,
                    bgColor: MyTheme.brownColor,
                    width: getWP(context, 30),
                    height: getHP(context, 5),
                    callback: () {
                      Get.back();
                      callback(true);
                    }),
              ],
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawInfoBox(String title, String val) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Txt(
            txt: title,
            txtColor: Colors.black,
            txtSize: 1.8,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(width: 10),
        Expanded(
          child: Txt(
              txt: val,
              txtColor: Colors.black,
              txtSize: 1.8,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
      ],
    );
  }
}
