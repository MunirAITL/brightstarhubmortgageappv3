import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class InputBox extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd;
  double leftPad, rightPad;
  InputBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    @required this.isPwd,
    this.leftPad = 20,
    this.rightPad = 20,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: leftPad, right: rightPad),
      child: TextField(
        controller: ctrl,
        keyboardType: kbType,
        obscureText: isPwd,
        maxLength: len,
        autocorrect: false,
        style: TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context),
        ),
        decoration: new InputDecoration(
          labelText: lableTxt,
          labelStyle: new TextStyle(
            color: Colors.black,
            fontSize: getTxtSize(context: context),
          ),
          contentPadding: EdgeInsets.only(left: 20, right: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }
}
