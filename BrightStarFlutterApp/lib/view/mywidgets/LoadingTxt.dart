import 'package:Bright_Star/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class LoadingTxt extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Center(
        child: ScaleAnimatedTextKit(
          text: [
            "Loading...",
            "Please wait...",
          ],
          textStyle: TextStyle(
              fontSize: getTxtSize(context: context), color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
