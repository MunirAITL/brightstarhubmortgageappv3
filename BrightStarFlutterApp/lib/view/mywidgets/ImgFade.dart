import 'package:flutter/material.dart';
import 'package:image_fade/image_fade.dart';

class ImgFade extends StatelessWidget {
  final String url;
  final bool isAssetsImg;
  const ImgFade({
    Key key,
    @required this.url,
    this.isAssetsImg = true,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ImageFade(
      fit: BoxFit.cover,
      loadingBuilder:
          (BuildContext context, Widget child, ImageChunkEvent event) {
        if (event == null) {
          return child;
        }
        return Center(
          child: CircularProgressIndicator(
              value: event.expectedTotalBytes == null
                  ? 0.0
                  : event.cumulativeBytesLoaded / event.expectedTotalBytes),
        );
      },
      image: (isAssetsImg)
          ? AssetImage(url)
          : NetworkImage(url), /* add child content here */
    );
  }
}
