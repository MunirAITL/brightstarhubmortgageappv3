import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:Bright_Star/Mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Function callback;

  const BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        child: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),

        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(10.0),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: 1.0, color: borderColor),
        ),
      ),
    );
  }
}
