import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/config/AppConfig.dart';
import 'package:Bright_Star/config/MyDefine.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/tab_noti/NotiHelper.dart';
import 'package:Bright_Star/controller/network/NetworkMgr.dart';
import 'package:Bright_Star/controller/observer/StateProvider.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiAPIModel.dart';
import 'package:Bright_Star/model/json/tab_noti/NotiModel.dart';
import 'package:Bright_Star/view/dashboard/more/badges/BadgeScreen.dart';
import 'package:Bright_Star/view/dashboard/more/profile/ProfileScreen.dart';
import 'package:Bright_Star/view/dashboard/more/settings/SettingsScreen.dart';
import 'package:Bright_Star/view/dashboard/timeline/TimeLineTab.dart';
import 'package:Bright_Star/view/mywidgets/BoldTxt.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:get/get.dart';
import '../ImgFade.dart';
import '../MyNetworkImage.dart';

class AppDrawer extends StatefulWidget {
  final UserModel userModel;
  final bool isNewCaseTab;
  final bool isMyCasesTab;
  final bool isTimelineTab;
  final bool isNotiTab;
  final int totalMsg;
  final int totalNoti;
  final bool isRightClose;
  AppDrawer({
    Key key,
    @required this.userModel,
    @required this.totalMsg,
    @required this.totalNoti,
    @required this.isRightClose,
    @required this.isNewCaseTab,
    @required this.isMyCasesTab,
    @required this.isTimelineTab,
    @required this.isNotiTab,
  }) : super(key: key);

  @override
  State createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> with Mixin {
  final StateProvider _stateProvider = StateProvider();
  final posKey = GlobalKey();
  bool isNotiViewClicked = false;

  bool isDialogHelpOpenned = false;
  List<String> listImgHelpTut = [
    "assets/images/helptut/newcase.png",
    "assets/images/helptut/mycases.png",
    "assets/images/helptut/timeline.png",
    "assets/images/helptut/noti.png",
    "assets/images/helptut/more.png",
  ];
  int indexHelpTut = 0;

  //  noti tab start here
  UserModel userModel;
  List<NotiModel> listNotiModel = [];

//  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  wsOnPageLoad() async {
    setState(() {
      isLoading = true;
    });
    try {
      final url = NotiHelper().getUrl(
          pageStart: pageStart, pageCount: pageCount, userModel: userModel);
      log(url);

      log(url);
      await NetworkMgr()
          .getData<NotiAPIModel, Null>(
        context: context,
        url: url,
        isLoading: false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> notifications =
                    model.responseData.notifications;

                if (notifications != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (notifications.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (NotiModel noti in notifications) {
                      listNotiModel.add(noti);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listNotiModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showToast(msg: "Notifications not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listImgHelpTut = null;
    listNotiModel = null;
    userModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return (widget.userModel == null)
        ? SizedBox()
        : (isDialogHelpOpenned)
            ? drawHelpTutBG()
            : drawLayout();
  }

  drawHelpTutBG() {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: getW(context),
          height: getH(context),
          child: ImgFade(url: listImgHelpTut[indexHelpTut]),
        ),
      ),
    );
  }

  drawLayout() {
    return SizedBox(
      width: getW(context), //20.0,
      child: Drawer(
          child: Container(
        color: MyTheme.themeData.accentColor,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10),
            drawHeader(),
            SizedBox(height: 20),
            Row(
              children: [
                SizedBox(width: getWP(context, 9)),
                CircleAvatar(
                  radius: 12.0,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(widget.userModel.profileImageURL),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Txt(
                      txt: widget.userModel.name,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 40, right: 40, bottom: 10),
              child: Container(color: Colors.black45, height: 0.5),
            ),
            (!isNotiViewClicked) ? drawItems() : drawNotiView(),
          ],
        ),
      )),
    );
  }

  drawItems() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              key: posKey,
              title: Txt(
                  txt: "New case",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () async {
                Get.back();
                if (!widget.isNewCaseTab) {
                  //  notifier to back for dashboard load
                  _stateProvider
                      .notify(ObserverState.STATE_CHANGED_dashboard_reload);
                }
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "My cases",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () async {
                Get.back();
                if (!widget.isMyCasesTab) {
                  Get.to(
                    () => MyCaseTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Messages",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                    showBadge: (widget.totalMsg > 0) ? true : false,
                    position: BadgePosition.topEnd(top: -10, end: 20),
                    badgeContent: Txt(
                        txt: widget.totalMsg.toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ],
              ),
              onTap: () async {
                Get.back();
                if (!widget.isTimelineTab) {
                  Get.to(
                    () => TimeLineTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Notifications",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                      showBadge: (widget.totalNoti > 0) ? true : false,
                      position: BadgePosition.topEnd(top: -10, end: 20),
                      badgeContent: Txt(
                        widget.totalNoti.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10
                        ),
                      ))
                ],
              ),
              onTap: () async {
                Get.back();
                if (!widget.isNotiTab) {
                  Get.to(
                    () => NotiTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Notifications",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                    showBadge: (widget.totalNoti > 0) ? true : false,
                    position: BadgePosition.topEnd(top: -10, end: 20),
                    badgeContent: Txt(
                        txt: widget.totalNoti.toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ],
              ),
              onTap: () {
                setState(() {
                  isNotiViewClicked = true;
                });
              },
              trailing: IconButton(
                  iconSize: 20,
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black54,
                  ),
                  onPressed: () {
                    setState(() {
                      isNotiViewClicked = true;
                    });
                  }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 20, bottom: 10),
            child: Txt(
                txt: "YOUR ACCOUNT",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - 0.5,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "My profile",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => ProfileScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Summary",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => SummaryScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Settings",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => SettingsScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "All Account",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(() => AllAccScreen(
                      isLoggedIn: true,
                      userModel: userModel,
                    ));
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Badges",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => BadgeScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Help",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => HelpScreen(),
                ).then((isHelpTut) {
                  if (isHelpTut) {
                    if (!isDialogHelpOpenned) {
                      isDialogHelpOpenned = true;
                      Get.dialog(HelpTutDialog(
                        callback: (int index) {
                          //
                          indexHelpTut = index;
                          setState(() {});
                          log(index.toString());
                        },
                      )).then((value) {
                        isDialogHelpOpenned = false;
                        //Get.back();
                      });
                    }
                    setState(() {});
                  }
                });
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Sign out",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.back();
                _stateProvider.notify(ObserverState.STATE_CHANGED_logout);
              },
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  drawHeader() {
    return (widget.isRightClose)
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    width: getWP(context, MyTheme.logoWidth),
                    //height: getHP(context, 12),
                    child: Image.asset(
                      "assets/images/logo/logo.png",
                      //fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10, bottom: 10),
                child: IconButton(
                    iconSize: 35,
                    icon: Icon(
                      Icons.close,
                      color: Colors.black54,
                    ),
                    onPressed: () {
                      Get.back();
                    }),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 10),
                child: IconButton(
                    iconSize: 35,
                    icon: Icon(
                      Icons.close,
                      color: Colors.black45,
                    ),
                    onPressed: () {
                      Get.back();
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Container(
                    width: getWP(context, 40),
                    //height: getHP(context, 12),
                    child: Image.asset(
                      "assets/images/logo/logo.png",
                      //fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
            ],
          );
  }

  drawNotiView() {
    //print('absolute coordinates on screen: ${containerKey.globalPaintBounds}');
    try {
      //RenderBox _box = posKey.currentContext.findRenderObject();
      //Offset position = _box.localToGlobal(Offset.zero);

      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 40),
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
                dense: true,
                minLeadingWidth: 0,
                leading: IconButton(
                  iconSize: 20,
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black54,
                  ),
                  onPressed: () {
                    setState(() {
                      isNotiViewClicked = false;
                    });
                  },
                ),
                title: Transform.translate(
                  offset: Offset(-20, 0),
                  child: Row(
                    children: [
                      Txt(
                          txt: "Notifications",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(width: 10),
                      Badge(
                        showBadge: (widget.totalNoti > 0) ? true : false,
                        position: BadgePosition.topEnd(top: -10, end: 20),
                        badgeContent: Txt(
                            txt: widget.totalNoti.toString(),
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  setState(() {
                    isNotiViewClicked = false;
                  });
                },
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 40, right: 40, top: 10),
                child: Container(
                  height: getHP(context, 65),
                  //color: MyTheme.skyColor,
                  color: Colors.white,
                  child: (listNotiModel.length > 0)
                      ? NotificationListener(
                          onNotification: (scrollNotification) {
                            if (scrollNotification is ScrollStartNotification) {
                              //print('Widget has started scrolling');
                            } else if (scrollNotification
                                is ScrollEndNotification) {
                              if (!isPageDone) {
                                pageStart++;
                                wsOnPageLoad();
                              }
                            }
                            return true;
                          },
                          child: Theme(
                            data: MyTheme.refreshIndicatorTheme,
                            child: RefreshIndicator(
                              onRefresh: _getRefreshData,
                              child: ListView.builder(
                                addAutomaticKeepAlives: true,
                                cacheExtent: AppConfig.page_limit.toDouble(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                primary: false,
                                itemCount: listNotiModel.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return drawNotiItem(index);
                                },
                              ),
                            ),
                          ),
                        )
                      : (!isLoading)
                          ? Padding(
                              padding: const EdgeInsets.all(20),
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: getWP(context, 40),
                                      height: getHP(context, 20),
                                      child: Image.asset(
                                        'assets/images/screens/home/my_cases/case_nf.png',
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    Txt(
                                      txt:
                                          "Looks like you haven't created any notification?",
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.center,
                                      isBold: false,
                                      txtLineSpace: 1.2,
                                    ),
                                    SizedBox(height: 20),
                                    GestureDetector(
                                      onTap: () {
                                        wsOnPageLoad();
                                      },
                                      child: Txt(
                                          txt: "Refresh",
                                          txtColor:
                                              MyTheme.themeData.accentColor,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Center(
                              child: CircularProgressIndicator(
                                  backgroundColor: Colors.grey,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.blue)),
                            ),
                ),
              ),
            ),
          ],
        ),
      );
    } catch (e) {
      log(e.toString());
      return Container();
    }
  }

  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];
      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap =
          NotiHelper().getNotiMap(model: notiModel, userModel: userModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      return GestureDetector(
        onTap: () async {
          if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                userModel: userModel,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () async {
                  //  go to timeline as per condition
                  Get.to(
                    () => TimeLineTab(),
                  ).then((value) {
                    //callback(route);
                  });
                });
          }
        },
        child: Card(
          //height: getHP(context, 25),
          //color: Colors.transparent,
          elevation: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20, top: 20),
                child: CircleAvatar(
                  radius: 15.0,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl((notiModel != null)
                        ? notiModel.initiatorImageUrl
                        : MyDefine.MISSING_IMG),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    BoldTxt(text1: txt ?? '', text2: eventName),
                    SizedBox(height: 10),
                    Center(
                      child: Txt(
                          txt: notiMap['publishDateTime'].toString(),
                          txtColor: Colors.grey,
                          txtSize: MyTheme.txtSize - .8,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                    //SizedBox(height: 20),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
