import 'package:Bright_Star/Mixin.dart';
import 'package:Bright_Star/config/MyTheme.dart';
import 'package:Bright_Star/controller/helper/more/ProfileHelper.dart';
import 'package:Bright_Star/model/db/DBMgr.dart';
import 'package:Bright_Star/model/json/auth/UserModel.dart';
import 'package:Bright_Star/view/dashboard/DashboardScreen.dart';
import 'package:Bright_Star/view/mywidgets/BSBtn.dart';
import 'package:Bright_Star/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AllAccScreen extends StatefulWidget {
  final UserModel userModel;
  final int otpId;
  final String otpMobileNumber;
  final bool isLoggedIn;
  const AllAccScreen(
      {Key key,
      @required this.userModel,
      this.otpId,
      this.otpMobileNumber,
      @required this.isLoggedIn})
      : super(key: key);
  @override
  State createState() => _AllAccScreenState();
}

class _AllAccScreenState extends State<AllAccScreen> with Mixin {
  List<dynamic> listItems = [
    {
      'id': 1,
      'name': 'Ten Right Angle',
      'domain': '',
      'phone': '',
      'btn': BSBtn(
        txt: "Customer Panel",
        bgColor: Colors.grey,
        height: null,
        callback: () {
          //
        },
      ),
    },
    {
      'id': 2,
      'name': 'Ever North Mortgages Ltd',
      'domain': 'https://www.mortgage-magic.co.uk',
      'phone': '0333 888 0238',
      'btn': BSBtn(
        txt: "Customer Panel",
        bgColor: Colors.grey,
        height: null,
        callback: () {
          //
        },
      ),
    },
  ];

  int _groupValue = 0;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(

                    //expandedHeight: 160,
                    automaticallyImplyLeading: widget.isLoggedIn,
                    elevation: 0,
                    //toolbarHeight: getHP(context, 10),
                    backgroundColor: MyTheme.appbarColor,
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: true,
                    title: Txt(
                        txt: "Your Account",
                        txtColor: Colors.black,
                        txtSize: MyTheme.appbarTitleFontSize,
                        txtAlign: TextAlign.start,
                        isBold: false)),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            SizedBox(height: 10),
            Txt(
                txt: "Use these accounts instead of creating a new one.",
                txtColor: MyTheme.brownColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                txtLineSpace: 1.2,
                isBold: true),
            SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                BSBtn btn = item['btn'] as BSBtn;

                return Card(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: ListTile(
                      title: Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: MyTheme.brownColor,
                          disabledColor: MyTheme.brownColor,
                          selectedRowColor: MyTheme.brownColor,
                          indicatorColor: MyTheme.brownColor,
                          toggleableActiveColor: MyTheme.brownColor,
                        ),
                        child: RadioListTile(
                          value: index,
                          groupValue: _groupValue,
                          onChanged: (value) {
                            if (mounted) {
                              setState(() {
                                _groupValue = value;
                              });
                            }
                          },
                          title: Txt(
                              txt: item["name"],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: "Domain: " + item["domain"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: "Phone: " + item["phone"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20),
                            child: btn ?? SizedBox(),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: 20),
            BSBtn(
              txt: "Continue with this account",
              height: getHP(context, 7),
              callback: () async {
                //
                try {
                  if (widget.otpId != null && widget.otpMobileNumber != null) {
                    await DBMgr.shared.setUserProfile(
                      user: widget.userModel,
                      otpID: widget.otpId.toString(),
                      otpMobileNumber: widget.otpMobileNumber,
                    );
                  } else {
                    await DBMgr.shared.setUserProfile(user: widget.userModel);
                  }
                } catch (e) {}
                try {
                  await ProfileHelper().downloadProfileImages(
                      context: context, userModel: widget.userModel);
                } catch (e) {
                  log(e.toString());
                }
                Get.off(() => DashBoardScreen()).then((value) {
                  //callback(route);
                });
              },
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
